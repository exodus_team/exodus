package it.uniroma2.art.exodus.xls_flow;

public class SVOTriple {
	
	private boolean IS_A_triple;
	
	private String subject;
	private String predicate=null;
	private Object object;
	
	public SVOTriple(String subject, String predicate, Object object) {
		this.subject = subject;
		this.predicate = predicate;
		this.object = object;
		IS_A_triple = false;
	} 
	
	public SVOTriple(String subject, Object object) {
		this.subject = subject;
		this.object = object;
		IS_A_triple = true;
	}

	public String getSubject() {
		return subject;
	}

	public String getPredicate() {
		return predicate;
	}

	public Object getObject() {
		return object;
	}

	public boolean isIS_A_triple() {
		return IS_A_triple;
	}	
	
	@Override
	public String toString(){
		String ret = "<" + subject + ",";
		if (IS_A_triple)
			ret = ret + "IS-A";
		else ret = ret + predicate;
		ret = ret + ","+object+">";
		return ret;
	}
	
	
	
}
