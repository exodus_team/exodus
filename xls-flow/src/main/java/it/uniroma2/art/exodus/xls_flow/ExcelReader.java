package it.uniroma2.art.exodus.xls_flow;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;

import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelReader {
	
	
	private List<SVOTriple> svoTripleList;
	
	public ExcelReader(){
		svoTripleList = new ArrayList<SVOTriple>();
	}
	
	public void buildTriples(File excelFile){
		Workbook workbook = null;
		System.out.println(System.getProperty("java.class.path"));
		try {
			//FileInputStream inputStream = new FileInputStream(excelFile);
			//workbook = getRelevantWorkbook(inputStream, excelFile.getAbsolutePath());
			workbook = getRelevantWorkbook(excelFile);
			
			//CON APACHE POI 3.12
			int numberOfSheets = workbook.getNumberOfSheets();
			for(int i = 0 ; i < numberOfSheets ; i++){
				Sheet currSheet = workbook.getSheetAt(i);
				buildTriplesBySheet(currSheet);
			}
			
			/* CON APACHE POI 3.16
			Iterator<Sheet> sheetIterator = workbook.iterator();
			while(sheetIterator.hasNext()){
				Sheet currSheet = sheetIterator.next();
				buildTriplesBySheet(currSheet);
			}*/
			

		} catch (IOException e) {
			e.printStackTrace();
		}
		finally{

			try {
				if(workbook!=null)
					workbook.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	
	
	
	/* CON APACHE POI 3.16
	 * private static Object createObjectValueByCell(Cell cell){
		Object ret = null;
		switch (cell.getCellTypeEnum()) {
	   		case STRING:
		   		ret = cell.getStringCellValue();
		   		break;
		   	case NUMERIC:
		   		if (HSSFDateUtil.isCellDateFormatted(cell))
		   			ret = cell.getDateCellValue();
		   		else ret = cell.getNumericCellValue();
		   		break;
		   	case BOOLEAN:
		   		ret = cell.getBooleanCellValue();
		   		break;
		   	case BLANK:
		   		break;
		   	default:
		   		break;
		}
		return ret;
	}
	 */
	
	//CON APACHE POI 3.12
	private static Object createObjectValueByCell(Cell cell){
		Object ret = null;
		switch (cell.getCellType()) {
	   		case Cell.CELL_TYPE_STRING:
		   		ret = cell.getStringCellValue();
		   		break;
		   	case Cell.CELL_TYPE_NUMERIC:
		   		if (HSSFDateUtil.isCellDateFormatted(cell))
		   			ret = cell.getDateCellValue();
		   		else ret = cell.getNumericCellValue();
		   		break;
		   	case Cell.CELL_TYPE_BOOLEAN:
		   		ret = cell.getBooleanCellValue();
		   		break;
		   	case Cell.CELL_TYPE_BLANK:
		   		break;
		   	default:
		   		break;
		}
		return ret;
	}
	
	
	
	
	private void buildTriplesBySheet(Sheet sheet){
		TreeMap<Integer,String> fieldnamesMap = new TreeMap<Integer,String>();
		
		Iterator<Row> rowIterator = sheet.iterator();
		
		if(rowIterator.hasNext()) { //first row, I catch field names
			 Row nextRow = rowIterator.next();
			 Iterator<Cell> cellIterator = nextRow.cellIterator();
	           
	         while (cellIterator.hasNext()) {
	             Cell cell = cellIterator.next();
	             String fieldname = cell.getStringCellValue();
	             
	             if(fieldname!=null && !fieldname.trim().equals(""))
	            	 fieldnamesMap.put(cell.getColumnIndex(),fieldname);
	             
	             //System.out.println(cell.getColumnIndex()+ ": " +cell.getStringCellValue());
	         }
		}
	     
		Set<Integer> columnIndexes = fieldnamesMap.keySet();
        
		while (rowIterator.hasNext()) {
           Row nextRow = rowIterator.next();
           boolean isFirstCell = true;
           
           String itemName = null;
           
           try{
        	   for(int columnIndex :columnIndexes){
        		   //System.out.println(columnIndex);
            	   Cell cell = nextRow.getCell(columnIndex);
            	   SVOTriple svoTriple;
            	   if(isFirstCell){
            		   //itemName = cell.getStringCellValue();
            		   String itemType = fieldnamesMap.get(columnIndex);
            		   System.out.println(itemType);
            		   Object itemNameObj = createObjectValueByCell(cell);
            		   if(itemNameObj instanceof String)
            			   itemName = (String) itemNameObj;
            		   else
            			   itemName = itemType+ itemNameObj;
            		   svoTriple = new SVOTriple(itemName,itemType);
            		   isFirstCell = false;
            		   svoTripleList.add(svoTriple);
            	   }
            	   else{
            		   if(cell!=null){
            			   String property = fieldnamesMap.get(columnIndex);
                		   Object object = createObjectValueByCell(cell);
                		   svoTriple = new SVOTriple(itemName,property, object);
                		   svoTripleList.add(svoTriple);
            		   }
            		   
            	   }
            	   
               }
           }
           catch(Exception e){
        	   e.printStackTrace();
        	   //TODO message with log
           }
           
           
           
		}
	}
	
	
	
	
	public static void main(String[] args) {
		String excelFilePath = "input/Orders.xls";
		File excelFile = new File(excelFilePath);
		
		ExcelReader reader = new ExcelReader();
		reader.buildTriples(excelFile);
		System.out.println(reader.getSVOtriples());
	}
	
	
	

	/*public static void main(String[] args) {
		try {
			String excelFilePath = "input/Orders.xlsx";   
			FileInputStream inputStream = new FileInputStream(new File(excelFilePath));
			System.out.println(excelFilePath);
			Workbook workbook = getRelevantWorkbook(inputStream, excelFilePath);
	  
			Sheet firstSheet = workbook.getSheetAt(0);
			
			TreeMap<Integer,String> fieldnamesMap = new TreeMap<Integer,String>();
			
			Iterator<Row> rowIterator = firstSheet.iterator();
			
			if(rowIterator.hasNext()) { //first row, I catch field names
				 Row nextRow = rowIterator.next();
				 Iterator<Cell> cellIterator = nextRow.cellIterator();
		           
		         while (cellIterator.hasNext()) {
		             Cell cell = cellIterator.next();
		             
		             fieldnamesMap.put(cell.getColumnIndex(),cell.getStringCellValue());
		             
		             System.out.println(cell.getStringCellValue());
		         }
			}
		     
			Set<Integer> columnIndexes = fieldnamesMap.keySet();

	        
			while (rowIterator.hasNext()) {
	           Row nextRow = rowIterator.next();
	           
	           //Iterator<Cell> cellIterator = nextRow.cellIterator();
	           boolean isFirstCell = true;
	           for(int columnIndex :columnIndexes){
	        	   Cell cell = nextRow.getCell(columnIndex);
	        	   String itemName;
	        	   SVOTriple svoTriple;
	        	   if(isFirstCell){
	        		   itemName = cell.getStringCellValue();
	        		   String itemType = fieldnamesMap.get(columnIndex);
	        		   svoTriple = new SVOTriple(itemName,itemType);
	        		   isFirstCell = false;
	        	   }
	        	   else{
	        		   String property = fieldnamesMap.get(columnIndex);
	        		   Object object = createObjectValueByCell(cell);
	        		   svoTriple = new SVOTriple(itemName,property, object);
	        	   }
	        	  // svoTripleList.add(svoTriple);
	           }
	           
	           while (cellIterator.hasNext()) {
	               Cell cell = cellIterator.next();

	               switch (cell.getCellTypeEnum()) {
	               		case STRING:
	            	   		System.out.print("(string) "+fieldnamesMap.get(cell.getColumnIndex())+": "+cell.getStringCellValue());
	            	   		break;
	            	   	case NUMERIC:
	            	   		if (HSSFDateUtil.isCellDateFormatted(cell))
	            	   			System.out.print("(date) "+fieldnamesMap.get(cell.getColumnIndex())+": "+cell.getDateCellValue());
	            	   		else System.out.print("(numeric) "+fieldnamesMap.get(cell.getColumnIndex())+": "+cell.getNumericCellValue());
	            	   		break;
	            	   	case BOOLEAN:
	            	   		System.out.print("(boolean) "+fieldnamesMap.get(cell.getColumnIndex())+": "+cell.getBooleanCellValue());
	            	   		break;
	            	   	case BLANK:
	            	   		System.out.print("BLANK");
	            	   		break;
	            	   	default:
	            	   		break;
	               }
	               System.out.print(" ");
	           }
	           System.out.println();
	       }
	        
	       workbook.close();
	       inputStream.close();
	  
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	*/
	
	
	public List<SVOTriple> getSVOtriples(){
		return new ArrayList<SVOTriple>(svoTripleList);
	}
	
	/*private static Workbook getRelevantWorkbook(FileInputStream inputStream, String excelFilePath) throws IOException
	{
	   Workbook workbook = null;
	 
	   if (excelFilePath.endsWith("xls")) {
	       workbook = new HSSFWorkbook(inputStream);
	   } else if (excelFilePath.endsWith("xlsx")) {
	       workbook = new XSSFWorkbook(inputStream);
	   } else {
	       throw new IllegalArgumentException("Incorrect file format");
	   }
	 
	   return workbook;
	}*/
	
	private static Workbook getRelevantWorkbook(File excelFile) throws IOException
	{
		
		/*Class klass = com.bea.xml.stream.EventFactory.class;
		System.out.println(klass.getResource('/' + klass.getName().replace('.', '/') + ".class").getFile());*/
		
	   Workbook workbook = null;
	   String excelFilePath = excelFile.getAbsolutePath();
	   if (excelFilePath.endsWith("xls")) {
	       workbook = new HSSFWorkbook(new FileInputStream(excelFile));
	   } else if (excelFilePath.endsWith("xlsx")) {
	       try {
			workbook = new XSSFWorkbook(excelFile);
	    	   /*OPCPackage pkg;
				try {
					//pkg = OPCPackage.open(new FileInputStream(excelFile));
					//workbook = new XSSFWorkbook(pkg);
					workbook = WorkbookFactory.create(excelFile);
				} catch (InvalidFormatException e1) {
					throw new IllegalArgumentException("Incorrect file format", e1);
				}*/
	       } catch (Exception e) {
				//System.out.println("OH NO");
				throw new IllegalArgumentException("Incorrect file format", e);
				/*OPCPackage pkg;
				try {
					pkg = OPCPackage.open(new FileInputStream(excelFile));
					workbook = new XSSFWorkbook(pkg);
				} catch (InvalidFormatException e1) {
					throw new IllegalArgumentException("Incorrect file format", e1);
				}*/
				
	       }
	   } else {
	       throw new IllegalArgumentException("Incorrect file format");
	   }
	 
	   return workbook;
	}
	
	
	
	/*private static Workbook createXSSFWorkbook(File excelFile) throws IOException{
		FileOutputStream streamOut = new FileOutputStream(excelFile, true);
		XSSFWorkbook streamWorkbook = new XSSFWorkbook();
		streamWorkbook.write(streamOut);
		streamOut.close();
		return streamWorkbook;
	}*/

}
