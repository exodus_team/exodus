package it.uniroma2.art.exodus.xls_flow;

import java.io.File;

/**
 * Hello world!
 *
 */
public class App 
{
	public static void main(String[] args) {
		String excelFilePath = "input/Orders.xlsx";
		File excelFile = new File(excelFilePath);
		
		/*ExcelReader reader = new ExcelReader();
		reader.buildTriples(excelFile);
		System.out.println(reader.getSVOtriples());*/
		
		

		//System.out.println(System.getProperty("java.class.path"));
		System.out.println(System.getProperties());


		
		File outputOntologyFile = new File("output/output.n3");
		XlsFlow flow = new XlsFlow();
		flow.produceOntology(excelFile, outputOntologyFile);
	}
}
