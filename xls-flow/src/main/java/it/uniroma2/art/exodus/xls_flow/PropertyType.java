package it.uniroma2.art.exodus.xls_flow;

public enum PropertyType {
	OBJECT_PROPERTY, DATATYPE, ANNOTATION;
	
	public static String getPropertyTypeIRI(PropertyType propertyType){
		String OWL_NS = "http://www.w3.org/2002/07/owl#";
		String ret;
		if(propertyType.equals(OBJECT_PROPERTY))
			ret = OWL_NS + "ObjectProperty";
		else if(propertyType.equals(DATATYPE))
			ret = OWL_NS + "DatatypeProperty";
		else if(propertyType.equals(ANNOTATION))
			ret = OWL_NS + "AnnotationProperty";
		else throw new IllegalArgumentException("PropertyType "+ propertyType + " not handled");
		return ret;
	}
	
}
