package it.uniroma2.art.exodus.xls_flow;

import java.io.File;
import java.io.PrintStream;
import java.util.List;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.sail.Sail;
import org.eclipse.rdf4j.sail.memory.MemoryStore;


import it.uniroma2.art.exodus.exodus_flow.Flow;

public class XlsFlow implements Flow {

	public XlsFlow(){};
	
	public void produceOntology(File inputFile, File outputOntologyFile) {
		String DEFAULT_NS = "http://art.uniroma2.it/exodus#";
		String OWL_NS = "http://www.w3.org/2002/07/owl#";
		
		ExcelReader reader = new ExcelReader();
		reader.buildTriples(inputFile);
		List<SVOTriple> svoTriples = reader.getSVOtriples();
		
		
		Sail sail = new MemoryStore();
		Repository rep = new SailRepository(sail);
		ModelBuilder modelBuilder = new ModelBuilder();
		
		try {

			rep.initialize();
			RepositoryConnection conn = rep.getConnection();
			conn.setNamespace("", DEFAULT_NS);
			ValueFactory valueFactory = conn.getValueFactory();
			
			OntologyHandler.setValueFactory(valueFactory);
			
			for (SVOTriple triple : svoTriples) {
				OntologyHandler ontologyHandler = new OntologyHandler(triple, DEFAULT_NS);
				Resource subject = ontologyHandler.getSubjectAsResource();
				IRI predicate = ontologyHandler.getPredicateAsIRI();
				Value object = ontologyHandler.getObjectAsValue();
				modelBuilder.add(subject, predicate, object);
				
				PropertyType propertyType = ontologyHandler.getPropertyType();
				String propertyTypeIRI = PropertyType.getPropertyTypeIRI(propertyType);
				modelBuilder.add(predicate, valueFactory.createIRI("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), valueFactory.createIRI(propertyTypeIRI));
			}
			
			Model model = modelBuilder.build();

			outputOntologyFile.createNewFile();

			Rio.write(model, new PrintStream(outputOntologyFile), RDFFormat.N3);
			
		} catch (Exception e){
			e.printStackTrace();
		}

		
	}
	
	
	
	

}
