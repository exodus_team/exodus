package it.uniroma2.art.exodus.xls_flow;

import java.util.Date;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;

public final class OntologyHandler {

	private PropertyType propertyType;
	private SVOTriple svoTriple;
	private String prefix;
	
	private static ValueFactory valueFactory;
	
	static{
		valueFactory = SimpleValueFactory.getInstance();
	}
	
	public static void setValueFactory(ValueFactory valueFactory){
		OntologyHandler.valueFactory = valueFactory; 
	}
	
	private PropertyType getPropertyTypeBySVOTriple(SVOTriple svoTriple){
		PropertyType ret;
		if(svoTriple.isIS_A_triple())
			ret = PropertyType.OBJECT_PROPERTY;
		else ret = PropertyType.DATATYPE;
		return ret;
	}
	
	public OntologyHandler(SVOTriple svoTriple, String prefix){
		this.svoTriple = svoTriple;
		if(prefix.endsWith("/") || prefix.endsWith("#"))
			this.prefix = prefix;
		else this.prefix = "#" + prefix;
		this.propertyType = getPropertyTypeBySVOTriple(svoTriple);
	}
	
	public Resource getSubjectAsResource(){
		String subjectStr = svoTriple.getSubject();
		subjectStr = refineForOntology(subjectStr, true);
		subjectStr = prefix + subjectStr;
		IRI subjectIRI = valueFactory.createIRI(subjectStr);
		return subjectIRI;
	}
	
	public IRI getPredicateAsIRI(){
		String predicateStr;
		//per ora il fatto dell'is-a coincide con il caso in cui la properietà è una object property, ma in generale è solo un sottocaso e così dovrà essere implementato
		if(svoTriple.isIS_A_triple())
			predicateStr = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type";
		else {
			predicateStr = svoTriple.getPredicate();
			predicateStr = refineForOntology(predicateStr, true);
			predicateStr = predicateStr.substring(0, 1).toLowerCase() + predicateStr.substring(1);
			predicateStr = prefix + predicateStr;
		}

		IRI predicateIRI = valueFactory.createIRI(predicateStr);
		return predicateIRI;
	}
	
	public Value getObjectAsValue(){
		Value ret = null;
		Object object = svoTriple.getObject();
		
		//per ora il fatto dell'is-a coincide con il caso in cui l'object è una IRI, ma in generale è solo un sottocaso e così dovrà essere implementato
		if(svoTriple.isIS_A_triple()){
			String objectStr = object.toString();
			objectStr = refineForOntology(objectStr, true);
			objectStr = prefix + objectStr;
			ret = valueFactory.createIRI(objectStr);
		}
		
		else{
			if(object instanceof Double){
				double obj = (Double) object;
				ret = valueFactory.createLiteral(obj);
			}
			else if(object instanceof String){
				String obj = (String) object;
				ret = valueFactory.createLiteral(obj);
			}
			else if(object instanceof Date){
				Date obj = (Date) object;
				ret = valueFactory.createLiteral(obj);
			}
			
			else throw new IllegalArgumentException("Type of "+ object + " not handled");
		}
		return ret;
	}
	
	//metodi che a partire dagli elementi della SVOTriple ritornano oggetti rdf4j
	
	
	
	private static String refineDoubleDatatype(double datatype){
		String ret = '\"'+datatype+'\"'+"^^xsd:double";
		return ret;
	}
	
	private static String refineForOntology(String toRefine, boolean recursivelyDeleteBlanks) {
		String ret;
		if(toRefine != null){
			ret = toRefine.trim();
			if(ret.length() > 1){
				if(!ret.contains(" "))
					ret = ret.substring(0, 1).toUpperCase() + ret.substring(1).toLowerCase();
				else{
					String[] words = ret.split(" "); 
					ret = ""; 
					for(int i = 0 ; i < words.length ; i++){
						if (! recursivelyDeleteBlanks) 
							ret = ret + " ";
						ret = ret + refineForOntology(words[i], recursivelyDeleteBlanks);
					}
					if (! recursivelyDeleteBlanks) 
						ret = ret.substring(1);	
				}
			}
				
			else if (ret.length() == 1)
				ret = ret.toUpperCase();
		}
		else ret = "";
		
		return ret;
	}

	public PropertyType getPropertyType() {
		return propertyType;
	}

	
}
