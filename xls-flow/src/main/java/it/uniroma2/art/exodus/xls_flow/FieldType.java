package it.uniroma2.art.exodus.xls_flow;

public enum FieldType {
	
	NUMBER, TEXT, BOOLEAN, DATE

}
