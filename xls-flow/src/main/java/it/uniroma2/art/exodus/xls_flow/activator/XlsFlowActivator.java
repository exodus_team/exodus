package it.uniroma2.art.exodus.xls_flow.activator;

import java.util.Hashtable;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;


import it.uniroma2.art.exodus.exodus_flow.Flow;
import it.uniroma2.art.exodus.xls_flow.XlsFlow;

public class XlsFlowActivator implements BundleActivator
{
   
    public void start(BundleContext context)
    {
        Hashtable<String, String> props = new Hashtable<String, String>();
        props.put("Language", "English");
        context.registerService(
            Flow.class.getName(), new XlsFlow(), props);
    	
    }
 
 
    public void stop(BundleContext context)
    {
        // NOTE: The service is automatically unregistered.
    }


}
