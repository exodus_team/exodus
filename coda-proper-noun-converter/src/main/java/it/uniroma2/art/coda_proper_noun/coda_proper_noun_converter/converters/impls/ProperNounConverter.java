package it.uniroma2.art.coda_proper_noun.coda_proper_noun_converter.converters.impls;



import it.uniroma2.art.coda.exception.ConverterException;
import it.uniroma2.art.coda.interfaces.CODAContext;

import it.uniroma2.art.coda.interfaces.annotations.converters.FeaturePathArgument;

import it.uniroma2.art.coda.interfaces.annotations.converters.RequirementLevels;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;

import it.uniroma2.art.coda_proper_noun.coda_proper_noun_converter.converters.contracts.ProperNounContract;

/**
 * An implementation of the contract {@code ProperNounContract}
 */
public class ProperNounConverter implements ProperNounContract {

	public static final String CONVERTER_URI = "http://art.uniroma2.it/converters/properNoun#";

	private ValueFactory fact = SimpleValueFactory.getInstance();
	
	@FeaturePathArgument(requirementLevel = RequirementLevels.REQUIRED)
	public IRI produceURI(CODAContext ctx, String value) throws ConverterException {
		return fact.createIRI(ctx.getDefaultNamespace() + value.replaceAll("\\s+", "%"));
	}

	@FeaturePathArgument(requirementLevel = RequirementLevels.REQUIRED)
	public Literal produceLiteral(CODAContext ctx, String datatype, String lang, String value) throws ConverterException {
		if (datatype != null) {
			return fact.createLiteral(value, fact.createIRI(datatype));
		} else if (lang != null) {
			return fact.createLiteral(value, lang);
		} else {
			return fact.createLiteral(value);
		}
	}
}
