package it.uniroma2.art.coda_proper_noun.coda_proper_noun_converter.converters.impls;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.io.File;
import java.io.InputStream;
import java.util.List;

import org.apache.uima.UIMAFramework;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.Type;
import org.apache.uima.resource.metadata.TypeSystemDescription;
import org.apache.uima.util.CasCreationUtils;
import org.apache.uima.util.XMLInputSource;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.XMLSchema;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.repository.util.Repositories;
import org.eclipse.rdf4j.sail.memory.MemoryStore;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import it.uniroma2.art.coda.core.CODACore;
import it.uniroma2.art.coda.standalone.CODAStandaloneFactory;
import it.uniroma2.art.coda.structures.ARTTriple;
import it.uniroma2.art.coda.structures.SuggOntologyCoda;

public class ProperNounConverterIT {

	public static final String BUNDLES_DIRECTORY_PATH = "target/it-bundles";
	public static final String OSGI_CACHE_DIRECTORY_PATH = "target/it-cache";
	
	public static final String BASE_URI = "http://example.org/";
	public static final String DEFAULT_NS;
	
	static {
		if (!BASE_URI.endsWith("/") && !BASE_URI.endsWith("#")) {
			DEFAULT_NS = BASE_URI + "#";
		} else {
			DEFAULT_NS = BASE_URI;
		}
	}

	private static CODACore codaCore;
	private static Repository repo;
	
	// Initializes CODA
	@BeforeClass
	public static void setup() throws Exception {
		File bundlesDirectory = new File(BUNDLES_DIRECTORY_PATH);
		File cacheDirectory = new File(OSGI_CACHE_DIRECTORY_PATH);

		repo = new SailRepository(new MemoryStore());
		repo.initialize();
		
		Repositories.consume(repo, repoConn->repoConn.setNamespace("", DEFAULT_NS));

		codaCore = CODAStandaloneFactory.getInstance(bundlesDirectory, cacheDirectory);
		codaCore.initialize(repo.getConnection());
	}
	
	// Tear down CODA
	@AfterClass
	public static void teardown() {
		if (codaCore != null) {
			codaCore.stopAndClose();
		} else if (repo != null) {
			repo.shutDown();
		}
	}
	
	@Test
	public void testProduceURI() throws Exception {
		
		/// Initialize the input CAS
		
		// Load the type system descriptor
		// (located at src/test/it.uniroma2.art.coda_proper_noun.coda_proper_noun_converter/converters/impls/typeSystemDescriptor.xml)
		TypeSystemDescription tsDes = UIMAFramework.getXMLParser().parseTypeSystemDescription(
				new XMLInputSource(ProperNounConverterIT.class.getResource("typeSystemDescriptor.xml")));

		// Create a CAS object with the type system above
		CAS aCas = CasCreationUtils.createCas(tsDes, null, null);

		// Get a reference to the type of test annotations
		Type testAnnotationType = aCas.getTypeSystem().getType("it.uniroma2.art.coda_proper_noun.coda_proper_noun_converter.TestAnnotation");
		Feature testFeature = testAnnotationType.getFeatureByBaseName("testFeature");

		// Create an annotation of the given type (with a dummy range 0-0)
		FeatureStructure testAnnotationFS = aCas.createAnnotation(testAnnotationType,0,0);
		
		// Set the value of the testFeature 
		testAnnotationFS.setFeatureValueFromString(testFeature, "hello");

		// Add the annotation to the index
		aCas.addFsToIndexes(testAnnotationFS);

		//// Load the test PEARL rule
		try (InputStream is = ProperNounConverterIT.class.getResourceAsStream("projectionRules.pr")) {
			codaCore.setProjectionRulesModelAndParseIt(is);
		}
		
		//// Execute CODA
		codaCore.setJCas(aCas.getJCas());
		
		//// Assertions
		SuggOntologyCoda suggOntCoda = codaCore.processNextAnnotation(true);

		assertThat(suggOntCoda, is(notNullValue()));

		List<Value> objects = suggOntCoda.getAllInsertARTTriple().stream()
				.filter(triple -> triple.getPredicate().equals(RDF.VALUE)).map(ARTTriple::getObject)
				.collect(toList());

		ValueFactory valueFactory = SimpleValueFactory.getInstance();

		assertThat(objects,
				containsInAnyOrder(valueFactory.createIRI("http://example.org/hello"),
						valueFactory.createLiteral("hello", "en"),
						valueFactory.createLiteral("hello", XMLSchema.STRING)));
	}

}
