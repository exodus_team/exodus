
/* First created by JCasGen Tue Feb 28 15:33:22 CET 2017 */
package it.uniroma2.art.exodus.person.person;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.tcas.Annotation_Type;

/** Type defined in it.uniroma2.art.exodus.person.person
 * Updated by JCasGen Thu Mar 02 15:55:40 CET 2017
 * @generated */
public class Person_Type extends Annotation_Type {
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = Person.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("it.uniroma2.art.exodus.person.person.Person");
 
  /** @generated */
  final Feature casFeat_properNoun;
  /** @generated */
  final int     casFeatCode_properNoun;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public String getProperNoun(int addr) {
        if (featOkTst && casFeat_properNoun == null)
      jcas.throwFeatMissing("properNoun", "it.uniroma2.art.exodus.person.person.Person");
    return ll_cas.ll_getStringValue(addr, casFeatCode_properNoun);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setProperNoun(int addr, String v) {
        if (featOkTst && casFeat_properNoun == null)
      jcas.throwFeatMissing("properNoun", "it.uniroma2.art.exodus.person.person.Person");
    ll_cas.ll_setStringValue(addr, casFeatCode_properNoun, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	 * @generated
	 * @param jcas JCas
	 * @param casType Type 
	 */
  public Person_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_properNoun = jcas.getRequiredFeatureDE(casType, "properNoun", "uima.cas.String", featOkTst);
    casFeatCode_properNoun  = (null == casFeat_properNoun) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_properNoun).getCode();

  }
}



    