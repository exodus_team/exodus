package it.uniroma2.art.exodus.person.activator;

import java.util.Hashtable;
import java.util.Objects;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

import it.uniroma2.art.coda.osgi.bundle.CODAOSGiFactory;
import it.uniroma2.art.exodus.exodus_flow.Flow;
import it.uniroma2.art.exodus.person.ProperNamesFlow;

/**
 * This class implements a simple bundle that uses the bundle
 * context to register an English language dictionary service
 * with the OSGi framework. The dictionary service interface is
 * defined in a separate class file and is implemented by an
 * inner class.
**/
/**
 * dovrò prevedere una classe astratta FlowActivator da mettere sotto exodus-flow oppure in un progetto a
 * parte, che deve essere estesa dall'activator concreto di ciascun flusso
 *
 */
public class ProperNamesFlowActivator implements BundleActivator {
	private ServiceTracker<CODAOSGiFactory, CODAOSGiFactory> codaOSGiFactoryTracker;

	/**
	 * Implements BundleActivator.start(). Registers an instance of a dictionary service using the bundle
	 * context; attaches properties to the service that can be queried when performing a service look-up.
	 * 
	 * @param context
	 *            the framework context for the bundle.
	 **/
	public void start(BundleContext context) {
		Hashtable<String, String> props = new Hashtable<String, String>();
		props.put("Language", "English");

		codaOSGiFactoryTracker = new ServiceTracker<>(context, CODAOSGiFactory.class, null);
		codaOSGiFactoryTracker.open();
		
		context.registerService(Flow.class.getName(), new ProperNamesFlow(() -> {
			return Objects.requireNonNull(codaOSGiFactoryTracker.getService().getInstance(context),
					"No CODA OSGi Factory Found");
		}), props);

	}

	/**
	 * Implements BundleActivator.stop(). Does nothing since the framework will automatically unregister any
	 * registered services.
	 * 
	 * @param context
	 *            the framework context for the bundle.
	 **/
	public void stop(BundleContext context) {
		// NOTE: The service is automatically unregistered.
		
		codaOSGiFactoryTracker.close();
	}

}