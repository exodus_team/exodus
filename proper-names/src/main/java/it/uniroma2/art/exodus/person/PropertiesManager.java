package it.uniroma2.art.exodus.person;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;
import java.util.Properties;

import org.ini4j.Config;
import org.ini4j.Ini;

public class PropertiesManager {
	
	
	
	public static String PROPERTIES_FILE = "proper-names.properties";
	
	private static PropertiesManager instance = null;
	
	//private static String ontologyFilepath;
	
	private static String codaFelixCacheDir;
	
	private static String specifierFile;
	private static String pearlFile;
	private static String targetBundlesDir;
	
	

	private PropertiesManager(){
		init();
	}
	
	
	public static PropertiesManager getInstance(){
		if (instance == null)
			instance = new PropertiesManager();
		return instance;
	}
	
	
	
	private static void init(){
		
		
		Properties prop = new Properties();
		InputStream input = null;

		try {

			Class<PropertiesManager> thisClass = PropertiesManager.class;
			URL propertiesURL = thisClass.getClassLoader().getResource(PROPERTIES_FILE);
			input = propertiesURL.openStream();
			
			//input = new FileInputStream(PROPERTIES_FILE);

			// load a properties file
			prop.load(input);

			// get the property value and print it out
			//ontologyFilepath = prop.getProperty("OntologyFilepath");
			codaFelixCacheDir = prop.getProperty("CodaFelixCacheDir");
			targetBundlesDir = prop.getProperty("TargetBundlesDir");
			
			specifierFile = prop.getProperty("SpecifierFile");
			pearlFile = prop.getProperty("PearlFile");

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	  
		
	}


	/*public String getOntologyFilepath() {
		return ontologyFilepath;
	}*/
	
	
	public String getCodaFelixCacheDir(){
		return codaFelixCacheDir;
	}
	
	
	public String getSpecifierFile(){
		return specifierFile;
	}
	
	public String getPearlFile(){
		return pearlFile;
	}
	
	public String getTargetBundlesDir(){
		return targetBundlesDir;
	}



}
