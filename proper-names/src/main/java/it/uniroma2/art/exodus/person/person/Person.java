

/* First created by JCasGen Tue Feb 28 15:33:22 CET 2017 */
package it.uniroma2.art.exodus.person.person;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.tcas.Annotation;


/** Type defined in it.uniroma2.art.exodus.person.person
 * Updated by JCasGen Thu Mar 02 15:55:40 CET 2017
 * XML source: C:/Users/Andrea/workspaceTesi/person/desc/it/uniroma2/art/exodus/person/personEngine.xml
 * @generated */
public class Person extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Person.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated
   * @return index of the type  
   */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Person() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated
   * @param addr low level Feature Structure reference
   * @param type the type of this Feature Structure 
   */
  public Person(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated
   * @param jcas JCas to which this Feature Structure belongs 
   */
  public Person(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated
   * @param jcas JCas to which this Feature Structure belongs
   * @param begin offset to the begin spot in the SofA
   * @param end offset to the end spot in the SofA 
  */  
  public Person(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** 
   * <!-- begin-user-doc -->
   * Write your own initialization here
   * <!-- end-user-doc -->
   *
   * @generated modifiable 
   */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: properNoun

  /** getter for properNoun - gets properNoun
   * @generated
   * @return value of the feature 
   */
  public String getProperNoun() {
    if (Person_Type.featOkTst && ((Person_Type)jcasType).casFeat_properNoun == null)
      jcasType.jcas.throwFeatMissing("properNoun", "it.uniroma2.art.exodus.person.person.Person");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Person_Type)jcasType).casFeatCode_properNoun);}
    
  /** setter for properNoun - sets properNoun 
   * @generated
   * @param v value to set into the feature 
   */
  public void setProperNoun(String v) {
    if (Person_Type.featOkTst && ((Person_Type)jcasType).casFeat_properNoun == null)
      jcasType.jcas.throwFeatMissing("properNoun", "it.uniroma2.art.exodus.person.person.Person");
    jcasType.ll_cas.ll_setStringValue(addr, ((Person_Type)jcasType).casFeatCode_properNoun, v);}    
  }

    