package it.uniroma2.art.exodus.person.annotator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.fit.component.JCasAnnotator_ImplBase;
import org.apache.uima.fit.descriptor.ConfigurationParameter;
import org.apache.uima.jcas.JCas;

import it.uniroma2.art.exodus.person.person.Person;


public class PersonAnnotator extends JCasAnnotator_ImplBase {

	public static final String PERSON_ARRAY = "personArray";
	@ConfigurationParameter(name = PERSON_ARRAY)
	private String [] personArray;
	
	@Override
	public void process(JCas aJCas) throws AnalysisEngineProcessException {
		
		Pattern patter = Pattern.compile("\\b([A-Z]?[a-z]+)\\b");
		
		//Pattern patter = Pattern.compile("\\b[A-Z][a-z]*(\\.)?(\\s)*\\b");
		
		//Pattern patter = Pattern.compile("\\b[A-Z][a-z]*[\\.]?[\\s]*\\b");
		
		//System.out.println("sto dentro PersonAnnotator");
		
		String text = aJCas.getDocumentText();
		
		Matcher matcher = patter.matcher(text);
		
		if(personArray == null || personArray.length==0){
			// the array is null or has no element, so there is no reason to execute the matcher
			return;
		}
		
		int count = 0;
		while(matcher.find(count)){
			
			String textFound = matcher.group();
			for(int i=0; i<personArray.length; ++i){
				String currentPerson = personArray[i].toLowerCase();
				if(currentPerson.compareToIgnoreCase(textFound) == 0){
					Person personAnnotation = new Person(aJCas);
					personAnnotation.setBegin(matcher.start());
					personAnnotation.setEnd(matcher.end());
					
					if (!("anonymous".equalsIgnoreCase(textFound))) {
						personAnnotation.setProperNoun(textFound);
					}

					//personAnnotation.setWord(textFound);
					personAnnotation.addToIndexes();
					break; // the person has been found, so skip to the next word
				}
			}
			
			count = matcher.end();
		}

	}
	
	
	
}