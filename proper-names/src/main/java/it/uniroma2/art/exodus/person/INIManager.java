package it.uniroma2.art.exodus.person;


import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.ini4j.Config;
import org.ini4j.Ini;


public class INIManager {
	
	public static String INI_FILE = "person.INI";
	
	private static INIManager instance = null;
	
	private static String ontologyFilepath;
	
	private static String codaFelixCacheDir;
	
	private static String specifierFile;
	private static String pearlFile;
	private static String targetBundlesDir;
	
	

	private INIManager(){
		init();
	}
	
	
	public static INIManager getInstance(){
		if (instance == null)
			instance = new INIManager();
		return instance;
	}
	
	
	
	private static void init(){
		
		Config.getGlobal().setEscape(false);
		
		
        Ini ini = new Ini();

        try {
			ini.load(new InputStreamReader(new FileInputStream(INI_FILE), "ISO-8859-1"));
			
			Ini.Section ontologySection = ini.get("Ontology");
			
			ontologyFilepath = ontologySection.get("Filepath").trim();
			
			
			Ini.Section codaSection = ini.get("Coda");
			
			codaFelixCacheDir = codaSection.get("FelixCacheDir").trim();
			
			specifierFile = codaSection.get("SpecifierFile").trim();
			pearlFile = codaSection.get("PearlFile").trim();
			targetBundlesDir = codaSection.get("TargetBundlesDir").trim();
			
			
			
		} catch (IOException e) {
			e.printStackTrace();
		} 
		
	}


	public String getOntologyFilepath() {
		return ontologyFilepath;
	}
	
	
	public String getCodaFelixCacheDir(){
		return codaFelixCacheDir;
	}
	
	
	public String getSpecifierFile(){
		return specifierFile;
	}
	
	public String getPearlFile(){
		return pearlFile;
	}
	
	public String getTargetBundlesDir(){
		return targetBundlesDir;
	}


	

	
	
	
}
