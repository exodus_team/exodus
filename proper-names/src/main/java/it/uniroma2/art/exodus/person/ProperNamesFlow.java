package it.uniroma2.art.exodus.person;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import org.apache.uima.UIMAFramework;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceManager;
import org.apache.uima.resource.ResourceSpecifier;
import org.apache.uima.ruta.engine.RutaEngine;
import org.apache.uima.util.XMLInputSource;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.sail.Sail;
import org.eclipse.rdf4j.sail.memory.MemoryStore;
import org.springframework.core.io.DefaultResourceLoader;

import it.uniroma2.art.coda.core.CODACore;
import it.uniroma2.art.coda.structures.ARTTriple;
import it.uniroma2.art.coda.structures.PreviousDecisionSingleElement;
import it.uniroma2.art.coda.structures.PreviousDecisions;
import it.uniroma2.art.coda.structures.SuggOntologyCoda;
import it.uniroma2.art.exodus.exodus_flow.Flow;

public class ProperNamesFlow implements Flow {

	private Supplier<CODACore> codaCoreSupplier;

	public ProperNamesFlow(Supplier<CODACore> codaCoreSupplier) {
		this.codaCoreSupplier = codaCoreSupplier;
	}

	/*
	 * private CODACore createCODACore(File felixCache, File bundles) throws Exception{ CODAOSGiManger manager
	 * = CODAOSGiManger.initialize(felixCache, new Properties()); manager.loadComponents(bundles);
	 * 
	 * return new CODACore(new StandaloneComponentProvider(manager)); }
	 */

	@Override
	public void produceOntology(File inputFile, File filledOntologyFile) {
		String DEFAULT_NS = "http://art.uniroma2.it/exodus#";

		DefaultResourceLoader resourceLoader = new DefaultResourceLoader(this.getClass().getClassLoader());
		// File targetBundlesFolder = new File("resources/bundle");
		// File pearlDocument = new File ("resources/rules/pearlBase.pr");

		org.springframework.core.io.Resource pearlDocument = resourceLoader
				.getResource(PropertiesManager.getInstance().getPearlFile());

		Sail sail = new MemoryStore();
		Repository rep = new SailRepository(sail);

		try {

			rep.initialize();
			try (RepositoryConnection conn = rep.getConnection()) {
				conn.setNamespace("", DEFAULT_NS);
			}

			// System.out.println("targetBundlesFolder: "+targetBundlesFolder.getAbsolutePath());
			// System.out.println("targetFelixCache: "+targetFelixCache.getAbsolutePath());

			// CODACore codaCore = CODAStandaloneFactory.getInstance(targetBundlesFolder, targetFelixCache);

			CODACore codaCore = codaCoreSupplier.get();

			try {

				// File specFile = new File("resources/desc/it/uniroma2/art/exodus/person/personEngine.xml");
				org.springframework.core.io.Resource specFile = resourceLoader
						.getResource(PropertiesManager.getInstance().getSpecifierFile());

				XMLInputSource in = new XMLInputSource(specFile.getInputStream(), null);
				ResourceSpecifier specifier = UIMAFramework.getXMLParser().parseResourceSpecifier(in);

				ResourceManager uimaResourceManager = UIMAFramework.newDefaultResourceManager();
//				uimaResourceManager.setExtensionClassPath(
//						context.getBundle().adapt(BundleWiring.class).getClassLoader(), "", true);

				RutaEngine rutaEngine = null;

				AnalysisEngine ae = UIMAFramework.produceAnalysisEngine(specifier, uimaResourceManager, null);

				codaCore.initialize(rep.getConnection());

				codaCore.setProjectionRulesModelAndParseIt(pearlDocument.getInputStream());

				JCas cas = ae.newJCas();

				String text = readFile(inputFile);

				cas.setDocumentText(text);
				ae.process(cas);

				codaCore.setJCas(cas);

				List<SuggOntologyCoda> annotations = codaCore.processAllAnnotation(false);

				ModelBuilder modelBuilder = new ModelBuilder();
				PreviousDecisions prevDec = new PreviousDecisions();

				for (SuggOntologyCoda annotation : annotations) {
					List<ARTTriple> triplesList = annotation.getAllInsertARTTriple();
					for (ARTTriple triple : triplesList) {
						Resource subject = triple.getSubject();
						IRI predicate = triple.getPredicate();
						Value object = triple.getObject();
						modelBuilder.add(subject, predicate, object);
					}
 
					PreviousDecisionSingleElement prevDecSingleElem = new PreviousDecisionSingleElement(
							new ArrayList<ARTTriple>(triplesList), annotation);
					prevDec.addPrevDecSingleElem(prevDecSingleElem);

				}

				Model model = modelBuilder.build();

				// String filepath = PropertiesManager.getInstance().getOntologyFilepath();
				// System.out.println(filepath);
				// File filledOntologyFile = new File(filepath);

				filledOntologyFile.createNewFile();

				Rio.write(model, new PrintStream(filledOntologyFile), RDFFormat.N3);
			} finally {

				codaCore.stopAndClose();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private static String readFile(File file) throws IOException {
		byte[] encoded = Files.readAllBytes(Paths.get(file.getAbsolutePath()));
		return new String(encoded, StandardCharsets.UTF_8);
	}

	// public static void main(String[] args) {
	// /*
	// * String text = "Mario volava alto mentre Gianni era più concreto.\n"; text = text +
	// * "Ciao caro Goffredo, come stai?\n"; text = text + "Andiamo incontro per strada a Lorenzo Carlo.\n";
	// * text = text + "Erano anni che non vedevo Lorenzo bere.\n"; text = text +
	// * "Salutate con ardore Tito Valerio Tauro, tribuno romano.\n"; text = text +
	// * "Bill and Billa 03.2014\n";
	// */
	//
	// /*
	// * String text = "Matteo Renzi fu capo del governo.\n"; text = text +
	// * "a v Paolo ha superato l'esame.\n"; text = text +
	// * "Batman non ha poteri, al contrario di Superman.\n";
	// */
	//
	// Flow flow = new ProperNamesFlow();
	// flow.produceOntology(new File("resources\\dummy.txt"), new File("output\\filledOntology.n3"));
	// }

}
