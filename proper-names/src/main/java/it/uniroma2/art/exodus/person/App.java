package it.uniroma2.art.exodus.person;

/**
 * Hello world!
 *
 */
public class App {
	
	public static void main(String[] args) {
//		String text = "Mario volava alto mentre Gianni era più concreto.\n";
//		text = text + "Ciao caro Goffredo, come stai?\n";
//		text = text + "Andiamo incontro per strada a Lorenzo Carlo.\n";
//		text = text + "Erano anni che non vedevo Lorenzo bere.\n";
//		text = text + "Salutate con ardore Tito Valerio Tauro, tribuno romano.\n";
//		text = text + "Bill and Billa 03.2014\n";
//		
//		String DEFAULT_NS = "http://art.uniroma2.it/exodus#";
//		
//		File targetBundlesFolder = new File("resources/bundle");
//		File targetFelixCache = new File (INIManager.getInstance().getCodaFelixCacheDir());
//		File pearlDocument = new File ("resources/rules/pearlBase.pr");
//		
//		Sail sail = new MemoryStore();
//		Repository rep = new SailRepository(sail);
//		
//		try{
//			
//			rep.initialize();
//			try(RepositoryConnection conn= rep.getConnection()){
//				conn.setNamespace("", DEFAULT_NS);
//			}
//			CODACore codaCore = CODAStandaloneFactory.getInstance(targetBundlesFolder, targetFelixCache);
//			
//			//File specFile = new File("pathToMyWorkspace/MyProject/descriptor/"+ "my/package/MyScriptEngine.xml");
//			File specFile = new File("resources/desc/it/uniroma2/art/exodus/person/personEngine.xml");
//			XMLInputSource in = new XMLInputSource(specFile);
//			ResourceSpecifier specifier = UIMAFramework.getXMLParser().parseResourceSpecifier(in);
//			// for import by name... set the datapath in the ResourceManager
//			AnalysisEngine ae = UIMAFramework.produceAnalysisEngine(specifier);
//			
//			codaCore.initialize(rep.getConnection());
//			
//			codaCore.setProjectionRulesModelAndParseIt(pearlDocument);
//			
//			JCas cas = ae.newJCas();
//			//CAS cas = ae.newCAS();
//			//cas.setDocumentText("This is my document.");
//			
//			
//			
//			cas.setDocumentText(text);
//			ae.process(cas);
//			
//			Type personType = cas.getTypeSystem().getType("it.uniroma2.art.exodus.person.person.Person");
//			
//			System.out.println("Annotations for Person:");
//			
//			Feature properNameFeature = personType.getFeatureByBaseName("properNoun");
//			
//			AnnotationIndex<Annotation> annotationIndexDate = cas.getAnnotationIndex(personType);
//			for (Annotation each : annotationIndexDate) {
//				System.out.println(each.getFeatureValueAsString(properNameFeature));
//				System.out.println(each.getCoveredText());
//			}
//			
//			
//			codaCore.setJCas(cas);
//			
//			List<SuggOntologyCoda> annotations = codaCore.processAllAnnotation(false);
//			System.out.println("#annotations: "+annotations.size());
//			
//			/*for(SuggOntologyCoda annotation : annotations){
//				System.out.println(annotation.getAnnotation().toString());
//				List<ARTTriple> triplesList = annotation.getAllARTTriple();
//				System.out.println("Current tripleList size: " + triplesList.size());
//			}*/
//			
//			//da gestire alla vecchia maniera
//			annotations.forEach(sugg -> {
//				sugg.getAllInsertARTTriple().forEach(t -> {
//					System.out.println(t.getSubject()+ "  " + t.getPredicate() + "  "+ t.getObject());
//				});
//			});
//			
//			
//			
//			
//			
//			ModelBuilder modelBuilder = new ModelBuilder();
//			PreviousDecisions prevDec = new PreviousDecisions();
//			
//			//List<ARTTriple> addedTripleList = new ArrayList<ARTTriple>();
//			
//			for (SuggOntologyCoda annotation : annotations) { 
//				List<ARTTriple> triplesList = annotation.getAllInsertARTTriple();
//				for(ARTTriple triple : triplesList){
//					Resource subject = triple.getSubject(); 
//			        IRI predicate = triple.getPredicate(); 
//			        Value object = triple.getObject(); 
//			        modelBuilder.add(subject, predicate, object);
//			        System.out.println("subject: " + subject.toString());
//			        System.out.println("predicate: " + predicate.toString());
//			        System.out.println("object: " + object.toString());
//			        System.out.println();
//				}
//				//addedTripleList.addAll(triplesList);
//				
//				PreviousDecisionSingleElement prevDecSingleElem = new PreviousDecisionSingleElement(new ArrayList<ARTTriple>(triplesList), annotation); 
//				    prevDec.addPrevDecSingleElem(prevDecSingleElem); 
//				
//		    } 
//		    
//		  
//			
//			Model model = modelBuilder.build();
//			
//			File filledOntologyFile = new File("output/filledOntology.rdf");
//			filledOntologyFile.createNewFile();
//			
//
//
//			//Rio.write(model, System.out, RDFFormat.RDFXML);
//			
//			Rio.write(model, new PrintStream(filledOntologyFile), RDFFormat.RDFXML);
//
//
//			/*String filename = "example-data-artists.ttl";
//
//			// read the file 'example-data-artists.ttl' as an InputStream.
//			InputStream input = Example06ReadTurtle.class.getResourceAsStream("/" + filename);
//
//			// Rio also accepts a java.io.Reader as input for the parser.
//			Model model = Rio.parse(input, "", RDFFormat.TURTLE);*/
//			
//			
//			
//			
//			
//			
//			
//			
//			
//			
//		}
//		catch (Exception e){
//			e.printStackTrace();
//		}
//		
//
//	}
//	
//	
//	
//	
//	
//	
//	/*
//	public static void main(String[] args){
//		executeCoda();
//	}
//	
//	*/
//	
//	
//	private static void executeCoda(){
//		String DEFAULT_NS = "http://example.org#";
//		
//		File targetBundlesFolder = new File("bundle");
//		File targetFelixCache = new File ("felix-cache");
//		File pearlDocument = new File ("rules/pearlBase.pr");
//		
//		Sail sail = new MemoryStore();
//		Repository rep = new SailRepository(sail);
//		
//		try{
//			rep.initialize();
//			try(RepositoryConnection conn= rep.getConnection()){
//				conn.setNamespace("", DEFAULT_NS);
//			}
//			CODACore codaCore = CODAStandaloneFactory.getInstance(targetBundlesFolder, targetFelixCache);
//			try{
//				codaCore.initialize(rep.getConnection());
//				
//				codaCore.setProjectionRulesModelAndParseIt(pearlDocument);
//				
//				
//				
//				/*File specFile = new File("src/main/resources/descriptor/typeSystemDescriptorPerson.xml");
//				XMLInputSource in = new XMLInputSource(specFile);
//				ResourceSpecifier specifier = UIMAFramework.getXMLParser().parseResourceSpecifier(in);
//				// for import by name... set the datapath in the ResourceManager
//				AnalysisEngine ae = UIMAFramework.produceAnalysisEngine(specifier);
//				JCas jcas = ae.newJCas();*/
//				
//				
//				
//				
//				JCas jcas = JCasFactory.createJCas();
//				jcas.setDocumentText("Pippo Andrea   Tiziano Greg    Lorenzo ");
//				//AnalysisEngine ae = AnalysisEngineFactory.createEngine(PersonAnnotator.class, PersonAnnotator.PERSON_ARRAY, new String[]{"Andrea", "Tiziano", "Marco", "Lillo", "Greg" });
//				AnalysisEngine ae = AnalysisEngineFactory.createEngine(PersonAnnotator.class, PersonAnnotator.PERSON_ARRAY, new String[]{"Andrea", "Tiziano", "Marco", "Lillo", "Greg", "Fabrizio", "Leonardo", "Matteo", "Serena" });
//				
//				
//				
//				
//				ae.process(jcas);
//				codaCore.setJCas(jcas);
//				
//				List<SuggOntologyCoda> annotations = codaCore.processAllAnnotation(false);
//				System.out.println("#annotations: "+annotations.size());
//				//da gestire alla vecchia maniera
//				annotations.forEach(sugg -> {
//					sugg.getAllInsertARTTriple().forEach(t -> {
//						System.out.println(t.getSubject()+ "  " + t.getPredicate() + "  "+ t.getObject());
//					});
//				});
//				
//				
//				
//				
//				
//				
//				
//				
//				
//				
//			
//				
//				
//				
//				
//				
//				
//				
//			}
//			finally{
//				codaCore.stopAndClose();
//			}
//		}
//		catch (Exception e){
//			e.printStackTrace();
//		}
//		finally{
//			rep.shutDown();
//		}
//		
//		
//		
//
	}
	
	
}
