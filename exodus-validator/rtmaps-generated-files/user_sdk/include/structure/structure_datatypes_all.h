#ifndef __RTMAPS_structure_DATATYPES_COMMON_H__
#define __RTMAPS_structure_DATATYPES_COMMON_H__

#include "robotml/maps_robotml_datatypes_all.h"


/*******************************************/
/* DATA TYPES FORWARD DECLARATIONS*/
/*******************************************/

enum Type;
class Date;
class HashSet;
class Set;
class ArrayList;
class List;
class LinkedList;
class Comparable;
class Resource;

/*******************************************/
/* COMPOSED DATA TYPES INCLUDE FILES*/
/*******************************************/
#include <structure/Type.h>
#include <structure/Date.h>
#include <structure/HashSet.h>
#include <structure/Set.h>
#include <structure/ArrayList.h>
#include <structure/List.h>
#include <structure/LinkedList.h>
#include <structure/Comparable.h>
#include <structure/Resource.h>

#endif //__RTMAPS_structure__DATATYPES_H__
