#ifndef __MAPS_structure_DATATYPE_FILTERS_H__
#define __MAPS_structure_DATATYPE_FILTERS_H__


#include "structure/Type.h"
#include "structure/Date.h"
#include "structure/HashSet.h"
#include "structure/Set.h"
#include "structure/ArrayList.h"
#include "structure/List.h"
#include "structure/LinkedList.h"
#include "structure/Comparable.h"
#include "structure/Resource.h"
#include "maps.hpp"

// The RTMaps input filter for the structure Type
const MAPSTypeFilterBase MAPSFilterType = MAPS_FILTER_USER_STRUCTURE(Type);
// The RTMaps input filter for the structure Date
const MAPSTypeFilterBase MAPSFilterDate = MAPS_FILTER_USER_STRUCTURE(Date);
// The RTMaps input filter for the structure HashSet
const MAPSTypeFilterBase MAPSFilterHashSet = MAPS_FILTER_USER_STRUCTURE(HashSet);
// The RTMaps input filter for the structure Set
const MAPSTypeFilterBase MAPSFilterSet = MAPS_FILTER_USER_STRUCTURE(Set);
// The RTMaps input filter for the structure ArrayList
const MAPSTypeFilterBase MAPSFilterArrayList = MAPS_FILTER_USER_STRUCTURE(ArrayList);
// The RTMaps input filter for the structure List
const MAPSTypeFilterBase MAPSFilterList = MAPS_FILTER_USER_STRUCTURE(List);
// The RTMaps input filter for the structure LinkedList
const MAPSTypeFilterBase MAPSFilterLinkedList = MAPS_FILTER_USER_STRUCTURE(LinkedList);
// The RTMaps input filter for the structure Comparable
const MAPSTypeFilterBase MAPSFilterComparable = MAPS_FILTER_USER_STRUCTURE(Comparable);
// The RTMaps input filter for the structure Resource
const MAPSTypeFilterBase MAPSFilterResource = MAPS_FILTER_USER_STRUCTURE(Resource);

#endif //__MAPS_structure_DATATYPE_FILTERS_H__
