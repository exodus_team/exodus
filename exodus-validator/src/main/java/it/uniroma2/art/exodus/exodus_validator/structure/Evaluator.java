package it.uniroma2.art.exodus.exodus_validator.structure;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;

public class Evaluator<T> {
	private Predicate<T> predicate;
	private List<T> forceEvaluationToTRUE;
	private List<T> forceEvaluationToFALSE;
	
	public Evaluator(Predicate<T> predicate, Collection<T> forceEvaluationToTRUE, Collection<T> forceEvaluationToFALSE){
		this.predicate = predicate;
		this.forceEvaluationToTRUE = new ArrayList<> (forceEvaluationToTRUE);
		this.forceEvaluationToFALSE = new ArrayList<> (forceEvaluationToFALSE);
	}
	
	public Evaluator(Predicate<T> predicate){
		this(predicate, Collections.emptyList(), Collections.emptyList());
	}
	
	public Predicate<T> getPredicate(){
		return predicate;
	}
	
	
	public boolean evaluate(T toEvaluate){
		boolean ret;
		if(forceEvaluationToTRUE.contains(toEvaluate))
			ret = true;
		else if(forceEvaluationToFALSE.contains(toEvaluate))
			ret = false;
		else {
			ret = predicate.test(toEvaluate);
		}
		return ret;	
	}
	
	
	public static void main(String[] args){
		Evaluator<String> eval = new Evaluator<>(s-> s.equals(s.toUpperCase()));
		System.out.println(eval.evaluate("LOL"));
		System.out.println(eval.evaluate("LOl"));
		System.out.println(eval.evaluate("kkH"));
		System.out.println();
		
		Evaluator<String> eval2 = new Evaluator<>(s-> s.equals(s.toUpperCase()), Collections.singletonList("kkH"), Collections.emptyList());
		System.out.println(eval2.evaluate("LOL"));
		System.out.println(eval2.evaluate("LOl"));
		System.out.println(eval2.evaluate("kkH"));
		
	}
	
}
