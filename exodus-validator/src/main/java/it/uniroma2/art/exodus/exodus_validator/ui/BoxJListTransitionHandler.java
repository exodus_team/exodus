package it.uniroma2.art.exodus.exodus_validator.ui;

import java.util.Map;

import javax.swing.DefaultListModel;
import javax.swing.JList;

import it.uniroma2.art.exodus.exodus_validator.BoxTransitionHandler;
import it.uniroma2.art.exodus.exodus_validator.structure.BoxLocation;
import it.uniroma2.art.exodus.exodus_validator.structure.BoxType;

public class BoxJListTransitionHandler extends BoxTransitionHandler{
	
	private Map<BoxType,JList<BoxLocation>> jListMap;
	
	public BoxJListTransitionHandler(Map<BoxType,JList<BoxLocation>> jListMap){
		this.jListMap = jListMap;
	}

	@Override
	public void move(BoxLocation boxLocation, BoxType boxType) {
		remove(boxLocation);
		boxLocation.setBoxType(boxType);
		((BoxLocationListModel) jListMap.get(boxType).getModel()).addElement(boxLocation);
	}

	@Override
	public void remove(BoxLocation boxLocation) {
		( (BoxLocationListModel) jListMap.get(boxLocation.getBoxType()).getModel()).removeElement(boxLocation);
	}

}
