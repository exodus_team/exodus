package it.uniroma2.art.exodus.exodus_validator.engine;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Value;

public class BindingTripleValueFactory {
	
	private static BindingTripleValueFactory instance = null;
	
	private BindingTripleValueFactory(){
		
	}
	
	public static BindingTripleValueFactory getInstance(){
		if(instance==null)
			instance = new BindingTripleValueFactory();
		return instance;
	}
	
	public BindingTripleValue<? extends Value> createBindingTripleValue(Value value){
		if(value instanceof IRI)
			return new BindingTripleValue<IRI>((IRI) value);
		else if(value instanceof Resource)
			return new BindingTripleValue<Resource>((Resource) value);
		else return new BindingTripleValue<Value>(value);
	}
	

}
