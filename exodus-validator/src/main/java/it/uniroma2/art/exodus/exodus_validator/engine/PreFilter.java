package it.uniroma2.art.exodus.exodus_validator.engine;

public class PreFilter {
	
	public enum BindingTripleElementRole{
		SUBJECT, PREDICATE, OBJECT
	}
	
	public enum ValueType {
		IS_CLASS, IS_OBJECT_PROPERTY, IS_DATATYPE_PROPERTY, IS_INDIVIDUAL
	}
	
	private BindingTripleElementRole role;
	private ValueType valueType;
	
	public PreFilter(BindingTripleElementRole role, ValueType valueType) {
		this.role = role;
		this.valueType = valueType;
	}

	public BindingTripleElementRole getRole() {
		return role;
	}

	public ValueType getValueType() {
		return valueType;
	}
	
	
	
	 

}
