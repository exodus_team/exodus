package it.uniroma2.art.exodus.exodus_validator.engine;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Value;

public class BindingTripleValue<T extends Value> extends BindingTripleElement{
	private T rdf4jValue;
	
	public BindingTripleValue(T value){
		this.rdf4jValue = value;
	}
	

	@Override
	public boolean equals(BindingTripleElement other) {
		boolean ret = false;
		if(other instanceof BindingTripleValue){
			BindingTripleValue<?> otherBindingTripleValue = (BindingTripleValue<?>) other;
			ret = valueEquals(otherBindingTripleValue.rdf4jValue);
		}
		return ret;
	}
	
	private <V extends Value> boolean valueEquals(V otherRDF4jValue){
		boolean ret = false;
		if( this.rdf4jValue instanceof IRI) {
			IRI thisRDF4jValueIRI = (IRI) this.rdf4jValue;
			ret = thisRDF4jValueIRI.equals(otherRDF4jValue);
		}
		else ret = this.rdf4jValue.stringValue().equals(otherRDF4jValue.stringValue());
		return ret;
	}


	@Override
	public boolean bind(Value rdf4jValue) {
		return valueEquals(rdf4jValue);
	}


	@Override
	public String toString() {
		String ret;
		if( this.rdf4jValue instanceof IRI) {
			IRI thisRDF4jValueIRI = (IRI) this.rdf4jValue;
			ret =  "<" + thisRDF4jValueIRI.toString() + ">";
		}
		else if(this.rdf4jValue instanceof Resource)
			ret = "<" + this.rdf4jValue.stringValue() + ">";
		else if (this.rdf4jValue instanceof Literal){
			Literal literal = (Literal) this.rdf4jValue;
			ret = this.rdf4jValue.stringValue();
			if(literal.getLanguage().isPresent()){
				ret = "\"" + ret + "\"@" + literal.getLanguage().get();
			}
			else ret = "\"" + ret + "\"";
		}
		else ret = this.rdf4jValue.stringValue();
		return ret;
	}
	
	public T getRDF4jValue(){
		return rdf4jValue;
	}
	
	@Override
	public int hashCode(){
		return 32* rdf4jValue.hashCode();
	}
	
	
}
