package it.uniroma2.art.exodus.exodus_validator.engine;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class InfRule {
	
	private BindingTriple preconditionBindingTriple;
	private Set<BindingTriple> triggingConditions;
	
	public InfRule(BindingTriple preconditionBindingTriple, Collection<BindingTriple> triggingConditions){
		this.preconditionBindingTriple = preconditionBindingTriple;
		this.triggingConditions = new TreeSet<>(triggingConditions);
	}
	
	public void addTriggingCondition(BindingTriple bindingTriple){
		this.triggingConditions.add(bindingTriple);	
	}

	public BindingTriple getPreconditionBindingTriple() {
		return preconditionBindingTriple;
	}

	public Set<BindingTriple> getTriggingConditions() {
		return new TreeSet<>(triggingConditions);
	}
	
	@Override
	public String toString(){
		return preconditionBindingTriple + " -- "+ triggingConditions;
	}
	
	@Override
	public boolean equals(Object other){
		boolean ret = false;
		if(other instanceof InfRule){
			InfRule otherInfRule = (InfRule) other;
			if(this.preconditionBindingTriple.equals(otherInfRule.preconditionBindingTriple))
				if(this.triggingConditions.containsAll(otherInfRule.triggingConditions) && otherInfRule.triggingConditions.containsAll(this.triggingConditions))
					ret = true;
		}
		return ret;
	}
	
	@Override
	public int hashCode(){
		return this.toString().hashCode();
	}
	

}
