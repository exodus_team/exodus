package it.uniroma2.art.exodus.exodus_validator;


import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.LinkedBlockingQueue;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;

public class BFSOntologyClassExplorer extends OntologyExplorer<Resource, Queue<Set<LabeledResource<Resource>>>>{
	
	private Set<Resource> visitedClasses;
	
	private Map<String, Collection<Resource>> equivalentClasses;
	private Map<String, Collection<Resource>> superClasses;
	private Map<String, Collection<Resource>> subClasses;
	
	private boolean useAuxiliaryMaps = false;
	
	
	/*	Query to retrieve all direct Thing's subclasses 
	 * 
	 *  prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
		prefix owl: <http://www.w3.org/2002/07/owl#>
		SELECT ?class
		WHERE {
			?class rdfs:subClassOf owl:Thing .
			OPTIONAL {
				?class rdfs:subClassOf ?subclass.
				?subclass rdfs:subClassOf owl:Thing .
				filter(?class != ?subclass && ?subclass != owl:Thing)   
			}
		filter (!bound(?subclass) && isURI(?class) && ?class != owl:Thing && ?class != owl:Nothing)
		}
		
		
		query to retrieve all equivalent classes of a given class:
		
		prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
		prefix owl: <http://www.w3.org/2002/07/owl#>
		SELECT ?eqClass
		WHERE {

			{?class owl:equivalentClass ?eqClass. }
                        UNION
                        {?eqClass owl:equivalentClass ?class. }
		        filter ( ?class IN  (<http://dbpedia.org/ontology/Person>,<http://schema.org/Person>)   )
		}
		
		
		
		
		query to retrieve all disjoint classes (considering equivalent classes but not superclasses):
		
		prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
		prefix owl: <http://www.w3.org/2002/07/owl#>
		SELECT DISTINCT ?disjClass
		WHERE {

			{?class owl:disjointWith ?disjClass. }
                        UNION
                        {?disjClass owl:disjointWith ?class. }
		        filter ( ?class IN  (<http://dbpedia.org/ontology/Person>,<http://schema.org/Person>)   )
		}
		
		
		
		query to retrieve all directed subclasses of given (equivalent) classes:
		
		prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
		prefix owl: <http://www.w3.org/2002/07/owl#>
		SELECT ?subClass
		WHERE {

			{?subClass rdfs:subClassOf ?class. }
                       
		        filter ( ?class IN  (<http://dbpedia.org/ontology/Person>,<http://schema.org/Person>, <http://xmlns.com/foaf/0.1/Person>)   )
		}
		
		
	 */
	

	
	private static final String queryForTopClasses = 
			"prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> "+
		"prefix owl: <http://www.w3.org/2002/07/owl#> "+
		"SELECT ?class "+
		"WHERE { "+
			"?class rdfs:subClassOf owl:Thing . "+
			"OPTIONAL { "+
				"?class rdfs:subClassOf ?subclass. "+
				"?subclass rdfs:subClassOf owl:Thing . "+
				"filter(?class != ?subclass && ?subclass != owl:Thing) "+   
			"} "+
		"filter (!bound(?subclass) && isURI(?class) && ?class != owl:Thing && ?class != owl:Nothing) "+
		"}";
	
	
	public Collection<Resource> getEquivalentClasses(Resource ontClass){
		return getEquivalentClasses(ontClass, false);
	}
	
	
	private Collection<Resource> getEquivalentClasses(Resource ontClass, boolean traceVisited){
		Collection<Resource> ret = new HashSet<Resource>();
		
		Collection<Resource> ontClasses = new HashSet<Resource>();
		
		if(useAuxiliaryMaps){
			String currKey = ontClass.stringValue();

			if(equivalentClasses.containsKey(currKey))
				ret.addAll(equivalentClasses.get(currKey));
			
			if(ret.isEmpty()){
				ontClasses.add(ontClass);
				ret = getEquivalentClasses(ontClasses, traceVisited);
				
				for(Resource clazz : ret)
					equivalentClasses.put(clazz.stringValue(), ret);
			}
		}
		
		else{
			ontClasses.add(ontClass);
			ret = getEquivalentClasses(ontClasses, traceVisited);
		}
		
		ret.add(ontClass);
		
		return ret;
	}
	

	
	
	public Collection<Resource> getDisjointClassesWithoutConsideringSubclasses(Set<Resource> ontClasses){
		Set<Resource> ret = new HashSet<Resource>();
		String inClause;
		
		String queryString =
				"prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> "+
				"prefix owl: <http://www.w3.org/2002/07/owl#> "+
				"SELECT DISTINCT ?disjClass "+
				"WHERE { "+
					"{?class owl:disjointWith  ?disjClass. } ";
		if(!containsAlsoInferredTriples){
			queryString = queryString +
		            "UNION "+
		            "{?disjClass owl:disjointWith ?class. } ";
			Set<Resource> equivalentClasses = new HashSet<Resource>();
			ontClasses.forEach(ontClass -> equivalentClasses.addAll(getEquivalentClasses(ontClass, false)));
					
			inClause = buildInClause(equivalentClasses);
		}
		else{
			inClause = buildInClause(ontClasses);
		}
		queryString = queryString +
				    "FILTER ( ?class IN ("+inClause+  ") ) "+
				"} ";
		TupleQuery query = conn.prepareTupleQuery(queryString);
		
		try (TupleQueryResult result = query.evaluate()) {
			
			while (result.hasNext()) {
			    BindingSet solution = result.next();
			    Value currValue = solution.getValue("disjClass");
			    if(currValue instanceof Resource){
			    	Resource currOntClazz = (Resource) currValue;
			    	ret.add(currOntClazz);
			    }
			    	
			}
	    }
		
		return ret;
	}
	
	
	
	
	
	
	
	
	
	
	public Collection<Resource> getDisjointClassesWithoutConsideringSubclasses(Resource ontClass){
		List<Resource> ret = new ArrayList<Resource>();
		String inClause;
		
		String queryString =
				"prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> "+
				"prefix owl: <http://www.w3.org/2002/07/owl#> "+
				"SELECT DISTINCT ?disjClass "+
				"WHERE { "+
					"{?class owl:disjointWith  ?disjClass. } ";
		if(!containsAlsoInferredTriples){
			queryString = queryString +
		            "UNION "+
		            "{?disjClass owl:disjointWith ?class. } ";
			Collection<Resource> equivalentClasses = getEquivalentClasses(ontClass, false);
			inClause = buildInClause(equivalentClasses);
		}
		else{
			Set<Resource> thisOntClass = new HashSet<Resource>();
			thisOntClass.add(ontClass);
			inClause = buildInClause(thisOntClass);
		}
		queryString = queryString +
				    "FILTER ( ?class IN ("+inClause+  ") ) "+
				"} ";
		
		TupleQuery query = conn.prepareTupleQuery(queryString);
		
		try (TupleQueryResult result = query.evaluate()) {
			
			while (result.hasNext()) {
			    BindingSet solution = result.next();
			    Value currValue = solution.getValue("disjClass");
			    if(currValue instanceof Resource){
			    	Resource currOntClazz = (Resource) currValue;
			    	ret.add(currOntClazz);
			    }
			    	
			}
	    }
		
		return ret;
	}
	
	
	
	
	private Collection<Resource> getEquivalentClasses(Collection<Resource> ontClasses, boolean traceVisited){
		if(ontClasses.size() == 0) return ontClasses;
		Collection<Resource> ret = new HashSet<Resource>();
		
		boolean newOntClasses = false;
		
		String inClause = buildInClause(ontClasses);
		
		String queryString =
				"prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> "+
				"prefix owl: <http://www.w3.org/2002/07/owl#> "+
				"SELECT DISTINCT ?eqClass "+
				"WHERE { "+
					"{?class owl:equivalentClass ?eqClass. } ";
		if(!containsAlsoInferredTriples)
			queryString = queryString +
		            "UNION "+
		            "{?eqClass owl:equivalentClass ?class. } ";
		
		queryString = queryString +
				    "FILTER ( ?class IN ("+inClause+  ") ) "+
				"} ";
		TupleQuery query = conn.prepareTupleQuery(queryString);
		
	    try (TupleQueryResult result = query.evaluate()) {
		
			while (result.hasNext()) {
			    BindingSet solution = result.next();
			    Value currValue = solution.getValue("eqClass");
			    
			    if(currValue instanceof Resource){
			    	Resource currOntClazz = (Resource) currValue;
			    	if(!containsAlsoInferredTriples && !ontClasses.contains(currOntClazz)){
			    		newOntClasses = true;
			    		ret.add(currOntClazz);
				    	if(traceVisited)
				    		visitedClasses.add(currOntClazz);
			    	}
			    	
			    }
			    	
			}
	    }
	    
	    if(!containsAlsoInferredTriples && newOntClasses){
	    	ret.addAll(ontClasses);
	    	ret = getEquivalentClasses(ret,traceVisited);
	    }
	    
	    return ret;
	    
	}
	
	
	
	
	
	public Set<Resource> getAllSubClasses(Resource ontClass){
		Set<Resource> ret = new HashSet<Resource>();
		String currKey = ontClass.stringValue();
		if(useAuxiliaryMaps && subClasses.containsKey(currKey))
			ret.addAll(subClasses.get(currKey));
		
		if(ret.isEmpty()){
			Set<Resource> ontClasses = new HashSet<Resource>();
			ontClasses.add(ontClass);
			ret = getAllSubClasses(ontClasses);
			
			if(useAuxiliaryMaps)
				for(Resource clazz : ret)
					subClasses.put(clazz.stringValue(), ret);
			
		}
		
		return ret;
	} 
	
	
	
	
	public Collection<Resource> getAllSuperClasses(Resource ontClass){
		Collection<Resource> ret = new ArrayList<Resource>();
		String currKey = ontClass.stringValue();
		if(useAuxiliaryMaps && superClasses.containsKey(currKey))
			ret.addAll(superClasses.get(currKey));
		
		if(ret.isEmpty()){
			Set<Resource> ontClasses = new HashSet<Resource>();
			ontClasses.add(ontClass);
			ret = getAllSuperClasses(ontClasses);
			
			if(useAuxiliaryMaps)
				for(Resource clazz : ret)
					superClasses.put(clazz.stringValue(), ret);
		}
		
		return ret;
	} 
	
	
	
	
	
	private Set<Resource> getAllSubClasses(Set<Resource> ontClasses){
		if(ontClasses.size() == 0) return ontClasses;
		Set<Resource> ret = new HashSet<Resource>();
		
		boolean newOntClasses = false;
		
		String inClause = buildInClause(ontClasses);
		
		String queryString =
				"prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> "+
				"prefix owl: <http://www.w3.org/2002/07/owl#> "+
				"SELECT DISTINCT ?subClass "+
				"WHERE { "+
				"{?subClass rdfs:subClassOf ?class. } "+
				    "FILTER ( ?class IN ("+inClause+  ") ) "+
				"} ";
		TupleQuery query = conn.prepareTupleQuery(queryString);
		
		//System.out.println(queryString);
		
	    try (TupleQueryResult result = query.evaluate()) {
		
			while (result.hasNext()) {
			    BindingSet solution = result.next();
			    Value currValue = solution.getValue("subClass");
			    
			    if(currValue instanceof Resource){
			    	Resource currOntClazz = (Resource) currValue;
			    	
			    	if(containsAlsoInferredTriples)
			    		ret.add(currOntClazz);
			    	
			    	else if(!ontClasses.contains(currOntClazz)){
			    		String currOntClassUri = currOntClazz.stringValue();
			    		if(useAuxiliaryMaps && subClasses.containsKey(currOntClassUri))
			    			ret.addAll(subClasses.get(currOntClassUri));
			    		else newOntClasses = true;
			    		ret.add(currOntClazz);
			    	}
			    	
			    }
			    	
			}
	    }
	    
	    if(!containsAlsoInferredTriples && newOntClasses){
	    	ret.addAll(ontClasses);
	    	ret = getAllSubClasses(ret);
	    }
	    
	    return ret;
	}
	
	
	
	
	private Collection<Resource> getAllSuperClasses(Collection<Resource> ontClasses){
		if(ontClasses.size() == 0) return ontClasses;
		Collection<Resource> ret = new ArrayList<Resource>();
		
		boolean newOntClasses = false;
		
		String inClause = buildInClause(ontClasses);
		
		String queryString =
				"prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> "+
				"prefix owl: <http://www.w3.org/2002/07/owl#> "+
				"SELECT DISTINCT ?superClass "+
				"WHERE { "+
				"{?class rdfs:subClassOf ?superClass. } "+
				    "FILTER ( ?class IN ("+inClause+  ") ) "+
				"} ";
		TupleQuery query = conn.prepareTupleQuery(queryString);
		
	    try (TupleQueryResult result = query.evaluate()) {
		
			while (result.hasNext()) {
			    BindingSet solution = result.next();
			    Value currValue = solution.getValue("superClass");
			    
			    if(currValue instanceof Resource){
			    	Resource currOntClazz = (Resource) currValue;
			    	
			    	if(containsAlsoInferredTriples)
			    		ret.add(currOntClazz);
			    	
			    	else if(!ontClasses.contains(currOntClazz)){
			    		String currOntClassUri = currOntClazz.stringValue();
			    		if(useAuxiliaryMaps && superClasses.containsKey(currOntClassUri))
			    			ret.addAll(superClasses.get(currOntClassUri));
			    		else newOntClasses = true;
			    		ret.add(currOntClazz);
			    	}
			    	
			    }
			    	
			}
	    }
	    
	    if(!containsAlsoInferredTriples && newOntClasses){
	    	ret.addAll(ontClasses);
	    	ret = getAllSuperClasses(ret);
	    }
	    
	    return ret;
	    
	}
	
	
	
	
	public BFSOntologyClassExplorer(Model ontologyModel, boolean containsAlsoInferredTriples){
		super(ontologyModel, containsAlsoInferredTriples);
	}
	
	

	public BFSOntologyClassExplorer(Model ontologyModel) {
		super(ontologyModel);
	}
	
	@Override
	protected void initializeResourcesToExplore(){
		visitedClasses = new HashSet<Resource>();
		equivalentClasses = new TreeMap<String,Collection<Resource>>();
		superClasses = new TreeMap<String,Collection<Resource>>();
		subClasses = new TreeMap<String,Collection<Resource>>();
		resorcesToExplore = new LinkedBlockingQueue<Set<LabeledResource<Resource>>>();
		
		TupleQuery query = conn.prepareTupleQuery(queryForTopClasses);
		
	    try (TupleQueryResult result = query.evaluate()) {
		
			while (result.hasNext()) {
			    BindingSet solution = result.next();
			    Value currValue = solution.getValue("class");
			    if(currValue instanceof IRI){
			    	IRI iri = (IRI)currValue;
			    	
			    	if(!visitedClasses.contains(iri)){
			    		visitedClasses.add(iri);
			    		Set<LabeledResource<Resource>> labeledEquivalentClasses = new HashSet<LabeledResource<Resource>>();
			    		List<String> labels = getLabels(iri);
				    	LabeledResource<Resource> labeledRes = new LabeledResource<Resource>(iri);
				    	labeledRes.addLabels(labels);
				    	labeledEquivalentClasses.add(labeledRes);
				    	
				    	//retrieve all equivalents classes, create their labeled versions and put them in the set
				    	Collection<Resource> eqClasses = getEquivalentClasses(iri, true);
				    	for(Resource eqClass : eqClasses){
				    		List<String> eqClassLabels = getLabels(eqClass);
				    		LabeledResource<Resource> labeledEqClass = new LabeledResource<Resource>(eqClass);
				    		labeledEqClass.addLabels(eqClassLabels);
				    		labeledEquivalentClasses.add(labeledEqClass);
				    	}
				    	
				    	resorcesToExplore.add(labeledEquivalentClasses);
			    	}
			    	
			    }
			    	
			}
	    }
		
	}
	
	
	
	
	private Set<Set<LabeledResource<Resource>>> getDirectedSubClasses(Set<LabeledResource<Resource>> equivalentLabeledClasses){
		Set<Set<LabeledResource<Resource>>> ret = new HashSet<Set<LabeledResource<Resource>>>();
		
		String inClause = "";
		for(LabeledResource<Resource> labeledClass : equivalentLabeledClasses){
			String resourceId;
			Resource ontClass = labeledClass.getResource();
			if(ontClass instanceof IRI)
				resourceId = "<"+((IRI) ontClass).toString()+">";
			else resourceId = "<"+ontClass.stringValue()+">";
			inClause = inClause + resourceId + ",";
		}
		inClause = inClause.substring(0, inClause.length()-1);
		
		String queryString = "prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> "+
			"prefix owl: <http://www.w3.org/2002/07/owl#> "+
				"SELECT DISTINCT ?subClass "+
				"WHERE { "+
					"{?subClass rdfs:subClassOf ?class. } "+     
				    "filter ( ?class IN  (" + inClause + ")   ) "+
				"} ";
		
		TupleQuery query = conn.prepareTupleQuery(queryString);
		
	    try (TupleQueryResult result = query.evaluate()) {
		
			while (result.hasNext()) {
			    BindingSet solution = result.next();
			    Value currValue = solution.getValue("subClass");
			    if(currValue instanceof Resource){
			    	Resource currOntClazz = (Resource) currValue;
			    	if(!visitedClasses.contains(currOntClazz)){
				    	visitedClasses.add(currOntClazz);
				    	List<String> labels = getLabels(currOntClazz);
				    	LabeledResource<Resource> currLabeledClass = new LabeledResource<Resource>(currOntClazz);
				    	currLabeledClass.addLabels(labels);
				    	
				    	Set<LabeledResource<Resource>> currEqClasses = new HashSet<LabeledResource<Resource>>();
				    	currEqClasses.add(currLabeledClass);
				    	Collection<Resource> eqResources = getEquivalentClasses(currOntClazz, true);
				    	for(Resource eqRes : eqResources){
				    		LabeledResource<Resource> labeledRes = new LabeledResource<Resource>(eqRes);
				    		labeledRes.addLabels(getLabels(eqRes));
				    		currEqClasses.add(labeledRes);
				    	}
				    	
				    	ret.add(currEqClasses);
			    	}
			    	
			    }
			    	
			}
	    }
	    
	    return ret;
	}


	@Override
	public Set<LabeledResource<Resource>> visit() {
		Set<LabeledResource<Resource>> currResources = resorcesToExplore.poll();
		resorcesToExplore.addAll(getDirectedSubClasses(currResources));
		return currResources;
	}



	@Override
	public boolean hasOtherItems() {
		return !resorcesToExplore.isEmpty();
	}

}
