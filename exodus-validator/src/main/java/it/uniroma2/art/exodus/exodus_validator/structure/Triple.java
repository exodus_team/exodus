package it.uniroma2.art.exodus.exodus_validator.structure;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.Value;

public class Triple implements Comparable<Triple> {
	
	private Resource subject;
	private IRI predicate;
	private Value object;
	
	public Triple(Statement statement){
		this(statement.getSubject(), statement.getPredicate(), statement.getObject());
	}
	
	public Triple(Resource subject, IRI predicate, Value object) {
		super();
		this.subject = subject;
		this.predicate = predicate;
		this.object = object;
	}

	public Resource getSubject() {
		return subject;
	}

	public IRI getPredicate() {
		return predicate;
	}

	public Value getObject() {
		return object;
	}
	
	@Override
	public String toString(){
		return "<"+subject.stringValue()+","+predicate.stringValue()+","+objectStringValue()+">";
	}

	@Override
	public int compareTo(Triple o) {
		return this.toString().compareTo(o.toString());
	}
	
	@Override
	public int hashCode(){
		return this.toString().hashCode();
	}
	
	
	private String objectStringValue(){
		String ret = object.stringValue();
		if(object instanceof Literal){
			Literal objectLiteral = (Literal) object;
			if(objectLiteral.getLanguage().isPresent()){
				ret = "\"" + ret + "\"@" + objectLiteral.getLanguage().get();
			}
			else ret = "\"" + ret + "\"";
		}
		return ret;
	}
	
	
	
	
}
