package it.uniroma2.art.exodus.exodus_validator.engine;

import java.util.List;

import it.uniroma2.art.exodus.exodus_validator.BFSOntologyClassExplorer;

public class ConsistenceInfEngine extends InfEngine<ConsistenceInfRule, ConsistenceInfEngineBinder>{
	
	public ConsistenceInfEngine(BFSOntologyClassExplorer kbExplorer, List<PreFilter> preFilters, ConsistenceInfRule...infRules) {
		super(ConsistenceInfEngineBinder.class, kbExplorer, preFilters, infRules);
	}
	
	public ConsistenceInfEngine(BFSOntologyClassExplorer kbExplorer, ConsistenceInfRule...infRules) {
		super(ConsistenceInfEngineBinder.class, kbExplorer, infRules);
	}
}
