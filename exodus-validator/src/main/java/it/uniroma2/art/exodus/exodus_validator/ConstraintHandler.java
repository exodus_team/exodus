package it.uniroma2.art.exodus.exodus_validator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import it.uniroma2.art.exodus.exodus_validator.structure.BoxLocation;
import it.uniroma2.art.exodus.exodus_validator.structure.BoxLocationEvaluatorFactory;
import it.uniroma2.art.exodus.exodus_validator.structure.BoxType;
import it.uniroma2.art.exodus.exodus_validator.structure.Constraint;
import it.uniroma2.art.exodus.exodus_validator.structure.Evaluator;
import it.uniroma2.art.exodus.exodus_validator.structure.Expression;
import it.uniroma2.art.exodus.exodus_validator.structure.MultipleExpression.Type;
import it.uniroma2.art.exodus.exodus_validator.structure.MultipleExpression;
import it.uniroma2.art.exodus.exodus_validator.structure.SingleExpression;
import it.uniroma2.art.exodus.exodus_validator.structure.ValidationHandler;


public class ConstraintHandler {
	
	private static ConstraintHandler instance=null;
	
	private ConstraintHandler(){
		
	}
	
	public static synchronized ConstraintHandler getInstance(){
		if(instance==null)
			instance = new ConstraintHandler();
		return instance;
	}
	
	private void setCCforBoxLocationInAND(Collection<BoxLocation> boxLocationsInAnd, Expression<BoxLocation> normalizedRSExpression){
		if(boxLocationsInAnd==null || boxLocationsInAnd.isEmpty())
			throw new IllegalArgumentException("Box Location collection is null or empty");
		
		BoxLocation candidateLSboxLoc;
		candidateLSboxLoc = boxLocationsInAnd.stream().filter(boxLoc -> boxLoc.getBoxType().equals(BoxType.INTERROGATION)).max(Comparator.comparing(BoxLocation::getTime)).orElse(null);
		
		if(candidateLSboxLoc==null){
			candidateLSboxLoc = boxLocationsInAnd.stream().max(Comparator.comparing(BoxLocation::getTime)).orElse(null);
			//MOVE TO ?BOX (UTILIZZARE LO STESSO METODO DELLA VALIDAZIONE MANUALE)
			Transition<ValidationHandler> transition = new Transition<>(ValidationHandler.getInstance(), candidateLSboxLoc, BoxType.INTERROGATION);
			transition.execute();
		}
			
		
		MultipleExpression<BoxLocation> andExpression = new MultipleExpression<>(Type.AND);
		for(BoxLocation boxLoc : boxLocationsInAnd)
			if(!boxLoc.equals(candidateLSboxLoc))
				andExpression.addSubExpression(new SingleExpression<>(boxLoc));
		
		if(boxLocationsInAnd.size()==1 && normalizedRSExpression==null){
			//MOVE TO -BOX (UTILIZZARE LO STESSO METODO DELLA VALIDAZIONE MANUALE)
			Transition<ValidationHandler> transition = new Transition<>(ValidationHandler.getInstance(), candidateLSboxLoc, BoxType.MINUS);
			transition.execute();
		}
		
		else {
			if(normalizedRSExpression!=null)
				andExpression.addSubExpression(normalizedRSExpression);
			setConflictConstraint(candidateLSboxLoc, andExpression);
		}
		
	}
	
	
	
	
	
	public void setConflictConstraint(BoxLocation boxLocation, Expression<BoxLocation> normalizedRSexpression){
		Constraint conflictConstraint = boxLocation.getConflictConstraint();
		if(conflictConstraint==null)
			conflictConstraint = new Constraint(boxLocation, normalizedRSexpression);
		else {
			Expression<BoxLocation> oldExpression = conflictConstraint.getTarget();
			List<Expression<BoxLocation>> listExpr = Arrays.asList(oldExpression, normalizedRSexpression);
			Expression<BoxLocation> newExpression = new MultipleExpression<>(listExpr,Type.OR);
			conflictConstraint.setTarget(newExpression);
		}
		boxLocation.setConflictConstraint(conflictConstraint);
		Set<BoxLocation> allItems = normalizedRSexpression.getAllItems();
		addPointingConflictConstraint(allItems, conflictConstraint);
	}
	
	
	
	public void setDependencyConstraint(BoxLocation boxLocation, Expression<BoxLocation> rsExpression){
		Expression<BoxLocation> normalizedRSexpression = normalizeExpression(rsExpression);
		
		Constraint dependencyConstraint = boxLocation.getDependencyConstraint();
		Constraint indirectDependencyConstraint = boxLocation.getIndirectDependencyConstraint();
		
		if(dependencyConstraint==null){ // il che implica anche che indirectDependencyConstraint è uguale a null
			dependencyConstraint = new Constraint(boxLocation, rsExpression);
			indirectDependencyConstraint = new Constraint(boxLocation, normalizedRSexpression);
		}
		else {
			Expression<BoxLocation> oldExpression = dependencyConstraint.getTarget();
			Expression<BoxLocation> oldIndirectExpression = indirectDependencyConstraint.getTarget();
			
			List<Expression<BoxLocation>> listExpr = Arrays.asList(oldExpression, rsExpression);
			List<Expression<BoxLocation>> listIndirectExpr = Arrays.asList(oldIndirectExpression, normalizedRSexpression);
			
			Expression<BoxLocation> newExpression = new MultipleExpression<>(listExpr,Type.OR);
			Expression<BoxLocation> newIndirectExpression = new MultipleExpression<>(listIndirectExpr,Type.OR);
			
			dependencyConstraint.setTarget(newExpression);
			indirectDependencyConstraint.setTarget(newIndirectExpression);
		}
		
		boxLocation.setDependencyConstraint(dependencyConstraint);
		boxLocation.setIndirectDependencyConstraint(indirectDependencyConstraint);
		
		Set<BoxLocation> allItems = rsExpression.getAllItems();
		Set<BoxLocation> allNormalizedItems = normalizedRSexpression.getAllItems();
		
		addPointingDependencyConstraint(allItems, dependencyConstraint);
		addPointingIndirectDependencyConstraint(allNormalizedItems, indirectDependencyConstraint);
		
		boxLocation.setTripleInferred(true);
	} 
	
	
	
	
	private void setConflictConstraintsForInferredLS(Expression<BoxLocation> lsExpressionNormalizedBySingleItem, Expression<BoxLocation> normalizedRSexpression){
		if(lsExpressionNormalizedBySingleItem instanceof SingleExpression){
			List<BoxLocation> boxLocations = new ArrayList<>();
			SingleExpression<BoxLocation> normalizedDnfLsSingleExpr = (SingleExpression<BoxLocation>) lsExpressionNormalizedBySingleItem;
			boxLocations.add(normalizedDnfLsSingleExpr.getItem());
			
			setCCforBoxLocationInAND(boxLocations, normalizedRSexpression);
		}
		
		else if(lsExpressionNormalizedBySingleItem instanceof MultipleExpression){
			MultipleExpression<BoxLocation> normalizedLsMultipleExpr = (MultipleExpression<BoxLocation>) lsExpressionNormalizedBySingleItem;
			if(normalizedLsMultipleExpr.getType().equals(Type.OR)){
				normalizedLsMultipleExpr.getSubExpressions().forEach(subExpr -> setConflictConstraintsForInferredLS(subExpr, normalizedRSexpression));
			}
			else if(normalizedLsMultipleExpr.getType().equals(Type.AND)){
				Set<Expression<BoxLocation>> subExpressions = normalizedLsMultipleExpr.getSubExpressions();
				if(subExpressions.stream().allMatch(subExpr -> subExpr instanceof SingleExpression)){
					List<BoxLocation> boxLocations = subExpressions.stream().map(subExpr -> {
						SingleExpression<BoxLocation> singleSubExpr = (SingleExpression<BoxLocation>) subExpr;
						return singleSubExpr.getItem();
					}).collect(Collectors.toList());
					setCCforBoxLocationInAND(boxLocations, normalizedRSexpression);
				}
				
				else if(subExpressions.size() == 1){
					setConflictConstraintsForInferredLS(subExpressions.iterator().next(), normalizedRSexpression);
				}
				
				else {
					Expression<BoxLocation> expressionToSetAsLeftSide = getExpressionToSetAsLeftSide(subExpressions);
					
					Set<Expression<BoxLocation>> rsSubExpression = subExpressions.stream().filter(subExpr -> !subExpr.equals(expressionToSetAsLeftSide)).collect(Collectors.toSet());
					if(normalizedRSexpression!=null)
						rsSubExpression.add(normalizedRSexpression);
					Expression<BoxLocation> expressionToSetAsRightSide = new MultipleExpression<>(rsSubExpression, MultipleExpression.Type.AND);
					setConflictConstraintsForInferredLS(expressionToSetAsLeftSide, expressionToSetAsRightSide);
				}
					
			}
			else throw new IllegalStateException("MultipleExpression type "+ normalizedLsMultipleExpr.getType() + " not handled");
		}
		
		else throw new IllegalStateException("Expression subclass "+ lsExpressionNormalizedBySingleItem.getClass() + " not handled");
	}
	
	
	
	private Expression<BoxLocation> getExpressionToSetAsLeftSide(Set<Expression<BoxLocation>> expressionsInAND){
		Set<Expression<BoxLocation>> falseSubExpressions = expressionsInAND.stream().filter(subExpr -> subExpr.evaluate(new Evaluator<BoxLocation>(boxLoc -> boxLoc.getBoxType().compareTo(BoxType.PLUS) < 0
				))).collect(Collectors.toSet());
		if(falseSubExpressions.isEmpty()){
			return expressionsInAND.stream().min((expr1, expr2) -> computeGlobalExpressionTime(expr1).compareTo(computeGlobalExpressionTime(expr2))).get();
		}
		else return falseSubExpressions.stream().min((expr1, expr2) -> computeGlobalExpressionTime(expr1).compareTo(computeGlobalExpressionTime(expr2))).get();
	}
	
	private Date computeGlobalExpressionTime(Expression<BoxLocation> expression){
		if(expression instanceof SingleExpression){
			SingleExpression<BoxLocation> singleExpression = (SingleExpression<BoxLocation>) expression;
			return singleExpression.getItem().getTime();
		}
		else if(expression instanceof MultipleExpression){
			MultipleExpression<BoxLocation> multipleExpression = (MultipleExpression<BoxLocation>) expression;
			if(multipleExpression.getType().equals(Type.OR))
				return multipleExpression.getSubExpressions().stream().map(this::computeGlobalExpressionTime).min( (date1, date2) -> date1.compareTo(date2)).orElse(null);
			else if(multipleExpression.getType().equals(Type.AND))
				return multipleExpression.getSubExpressions().stream().map(this::computeGlobalExpressionTime).max( (date1, date2) -> date1.compareTo(date2)).orElse(null);
			else throw new IllegalStateException("MultipleExpression type "+ multipleExpression.getType() + " not handled");
		}
		else throw new IllegalStateException("Expression subclass "+ expression.getClass() + " not handled");
	}
	
	
	
	
	//version without transformation to DNF 
	public void setConflictConstraintsForInferredLS(BoxLocation lsBoxLocation, Expression<BoxLocation> normalizedRSexpression){
		Expression<BoxLocation> normalizedLSexpr = normalizeSingleItem(lsBoxLocation);
		normalizedLSexpr = ExpressionUtils.getRefinedExpression(normalizedLSexpr);
		setConflictConstraintsForInferredLS(normalizedLSexpr, normalizedRSexpression);
	}
		
	
	
	//version with transformation to DNF 
	/*public void setConflictConstraintsForInferredLS(BoxLocation lsBoxLocation, Expression<BoxLocation> normalizedRSexpression){
		Expression<BoxLocation> normalizedLSexpr = normalizeSingleItem(lsBoxLocation);
		Expression<BoxLocation> normalizedDnfLSexpr = ExpressionUtils.toDNF(normalizedLSexpr);
		
		List<BoxLocation> boxLocations = new ArrayList<>();
		
		if(normalizedDnfLSexpr instanceof SingleExpression){
			SingleExpression<BoxLocation> normalizedDnfLsSingleExpr = (SingleExpression<BoxLocation>) normalizedDnfLSexpr;
			boxLocations.add(normalizedDnfLsSingleExpr.getItem());
			
			setCCforBoxLocationInAND(boxLocations, normalizedRSexpression);
		}
			
		else if(normalizedDnfLSexpr instanceof MultipleExpression){
			MultipleExpression<BoxLocation> normalizedDnfLsMultipleExpr = (MultipleExpression<BoxLocation>) normalizedDnfLSexpr;
			Collection<Expression<BoxLocation>> expressionsInOR = new ArrayList<>();
			if(normalizedDnfLsMultipleExpr.getType().equals(Type.OR))
				expressionsInOR.addAll(normalizedDnfLsMultipleExpr.getSubExpressions());
			else expressionsInOR.add(normalizedDnfLsMultipleExpr);
			
			expressionsInOR.forEach(expr -> {
				if(expr instanceof SingleExpression){
					SingleExpression<BoxLocation> singleExpr = (SingleExpression<BoxLocation>) expr;
					setCCforBoxLocationInAND(Collections.singletonList(singleExpr.getItem()), normalizedRSexpression);
				}
				else if (expr instanceof MultipleExpression){
					MultipleExpression<BoxLocation> multipleExpr = (MultipleExpression<BoxLocation>) expr;
					if(multipleExpr.getType().equals(Type.AND)){
						Stream<Expression<BoxLocation>> subExprStream = multipleExpr.getSubExpressions().stream();
						if(subExprStream.allMatch(subExpr -> (subExpr instanceof SingleExpression))){
							//since its subexpressions are single, it sufficies getting all items
							Set<BoxLocation> boxLocInAND = multipleExpr.getAllItems();
							setCCforBoxLocationInAND(boxLocInAND, normalizedRSexpression);
						}
						else throw new IllegalStateException("Expression " + normalizedDnfLSexpr + " not in DNF");
					}
					else throw new IllegalStateException("Expression " + normalizedDnfLSexpr + " not in DNF");
				}
					
			});
			
			
		
		}
	}
	*/
	
	
	
	
	/*public void setConflictConstraintsForInferredLS(BoxLocation lsBoxLocation, Expression<BoxLocation> normalizedExpression){
		Expression<BoxLocation> normalizedLSexpr = normalizeSingleItem(lsBoxLocation);
		Expression<BoxLocation> normalizedDnfLSexpr = ExpressionUtils.toDNF(normalizedLSexpr);
		if(normalizedDnfLSexpr instanceof SingleExpression){
			SingleExpression<BoxLocation> normalizedDnfLsSingleExpr = (SingleExpression<BoxLocation>) normalizedDnfLSexpr;
			BoxLocation boxLocBySingleExpr = normalizedDnfLsSingleExpr.getItem();
			if(normalizedExpression!=null)
				setConflictConstraint(boxLocBySingleExpr, normalizedExpression);
			else {
				//PUT boxLocBySingleExpr (and its inferred triples) into "- BOX" 
			}

		}
		else if (normalizedDnfLSexpr instanceof MultipleExpression){
			MultipleExpression<BoxLocation> normalizedDnfLsMultipleExpr = (MultipleExpression<BoxLocation>) normalizedDnfLSexpr;
			Collection<Expression<BoxLocation>> expressionsInOR = new ArrayList<>();
			if(normalizedDnfLsMultipleExpr.getType().equals(Type.OR))
				expressionsInOR.addAll(normalizedDnfLsMultipleExpr.getSubExpressions());
			else expressionsInOR.add(normalizedDnfLsMultipleExpr);
			
			
		}
			
		
	}
	*/
	
	
	
	
	private void addPointingConflictConstraint(Set<BoxLocation> pointedBoxLocations, Constraint conflictConstraint){
		pointedBoxLocations.forEach(pointedBoxLocation -> pointedBoxLocation.addPointingConflictConstraint(conflictConstraint));
	}
	
	
	private void addPointingDependencyConstraint(Set<BoxLocation> pointedBoxLocations, Constraint dependencyConstraint){
		pointedBoxLocations.forEach(pointedBoxLocation -> pointedBoxLocation.addPointingDependencyConstraint(dependencyConstraint));
	}
	
	
	private void addPointingIndirectDependencyConstraint(Set<BoxLocation> pointedBoxLocations, Constraint indirectDependencyConstraint){
		pointedBoxLocations.forEach(pointedBoxLocation -> pointedBoxLocation.addPointingIndirectDependencyConstraint(indirectDependencyConstraint));
	}
	
	
	
	
	
	
	
	
	public Expression<BoxLocation> normalizeExpression(Expression<BoxLocation> expression){
		Expression<BoxLocation> normalizedExpr = expression;
		Set<BoxLocation> allItems = expression.getAllItems();
		for(BoxLocation item : allItems){
			Expression<BoxLocation> expressionByNormalization = normalizeSingleItem(item);
			normalizedExpr = ExpressionUtils.replaceAll(normalizedExpr, item, expressionByNormalization);
		}
		return normalizedExpr;
	}
	
	
	
	
	//da ottimizzare: infatti lo stesso box location item potrebe essere presente in due punti diversi della computazione, nel qual caso 
	//l'elaborazione su di esso sarebbe compita più volte inutilmente (idem sopra)
	private Expression<BoxLocation> normalizeSingleItem(BoxLocation boxLoc) {
		if(!boxLoc.isTripleInferred())
			return new SingleExpression<>(boxLoc);
		else{
			Constraint indirecDependencyConstr = boxLoc.getIndirectDependencyConstraint();
			Expression<BoxLocation> indirectDependencyConstrExpr = indirecDependencyConstr.getTarget();
			return indirectDependencyConstrExpr;
		}
	}

	
	public BoxLocation mergeBoxLocation(BoxLocation boxLocation1, BoxLocation boxLocation2){
		return null;
	}
	
	
	
	
}
