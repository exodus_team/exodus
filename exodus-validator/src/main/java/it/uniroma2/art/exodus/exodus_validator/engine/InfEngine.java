package it.uniroma2.art.exodus.exodus_validator.engine;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.vocabulary.OWL;
import org.eclipse.rdf4j.model.vocabulary.RDF;

import it.uniroma2.art.exodus.exodus_validator.BFSOntologyClassExplorer;
import it.uniroma2.art.exodus.exodus_validator.engine.PreFilter.BindingTripleElementRole;
import it.uniroma2.art.exodus.exodus_validator.engine.PreFilter.ValueType;
import it.uniroma2.art.exodus.exodus_validator.structure.Triple;

//itera su tutte le regole per vedere quali hanno la precondition che matcha con la tripla in input
//per le regole che matchano, controllare che il binding della precondition verifichi le trigging restrictions della regola
//in caso affermativo, costruire i conflict iterando sulle variabili dei triggingConditionInOR rimaste libere
//La logica di risoluzione dei binding deve stare dentro Binder 
public class InfEngine<T extends InfRule, B extends Binder<T>> {
	
	private Map<BindingTriple, List<T>> infRulesByPrecondition;
	private BFSOntologyClassExplorer kbExplorer;
	private List<PreFilter> preFilters;
	
	private Class<B> binderClazz;
	

	@SafeVarargs
	public InfEngine(Class<B> binderClazz, BFSOntologyClassExplorer kbExplorer, T...infRules){
		this.binderClazz = binderClazz;
		
		if(infRules==null || infRules.length==0)
			throw new IllegalArgumentException("infRules cannot be null or empty");
		
		this.kbExplorer = kbExplorer;
		this.infRulesByPrecondition = new HashMap<>();
		
		for (T infRule : infRules){
			BindingTriple precondition = infRule.getPreconditionBindingTriple();
			if(this.infRulesByPrecondition.containsKey(precondition))
				this.infRulesByPrecondition.get(precondition).add(infRule);
			else{ 
				List<T> singletonRuleList = new ArrayList<>();
				singletonRuleList.add(infRule);
				this.infRulesByPrecondition.put(precondition, singletonRuleList);
			}
		}
		preFilters = new ArrayList<>();
	}
	
	
	@SafeVarargs
	public InfEngine(Class<B> binderClazz,  BFSOntologyClassExplorer kbExplorer, List<PreFilter> preFilters, T...infRules){
		this(binderClazz, kbExplorer, infRules);
		this.preFilters.addAll(preFilters);
	}
	
	


	//chiamato dal Validator per farmi ritornare tutto assieme
	public List<InfEngineOutput> fireAllInfRules(Triple triple){
		Map<B,List<T>> infRulesByBinder = firePartially(triple);
		return infRulesByBinder.entrySet().stream().map(entry -> {
			B binder = entry.getKey();
			List<T> infRules = entry.getValue();
			
			return infRules.stream().map(infRule -> 
				getListOfOutputs(infRule, binder)
			).reduce( (list1, list2) -> {
				List<InfEngineOutput> merged = new ArrayList<>(list1);
				merged.addAll(list2);
				return merged;
			}).orElse(new ArrayList<InfEngineOutput>());
			
		}).reduce( (list1, list2) -> {
			List<InfEngineOutput> merged = new ArrayList<>(list1);
			merged.addAll(list2);
			return merged;
		}).orElse(new ArrayList<InfEngineOutput>());
	}
	
	
	//for each binder it returns the consistence rules with the same precondition of the binder
	public Map<B,List<T>> firePartially(Triple triple){
		Map<B,List<T>> ret = new HashMap<>();
		if (! preFilters.stream().anyMatch(preFilter -> verifyTriggingRestriction(triple, preFilter)))
			infRulesByPrecondition.keySet().stream().filter(precondition -> precondition.bind(triple)).forEach(precondition -> {
				BinderFactory<B> factory = new BinderFactory<>(binderClazz);
				B binder = factory.createBinder(triple, precondition);
				List<T> infRules = infRulesByPrecondition.get(precondition);
				ret.put(binder, infRules);
			});
		
		return ret;			
	}
	
	public List<InfEngineOutput> getListOfOutputs(T infRule, B binder){
		binder.bind(infRule);
		List<BindingTriple> boundTriplesInAND = binder.getBoundTriples();
		List<InfEngineOutput> ret = new ArrayList<>();
		if(!boundTriplesInAND.isEmpty() && binder.hasFreeVariables()){
			List<Map<Variable, Value>> mappings = kbExplorer.getMappingsByBoundTriples(boundTriplesInAND, infRule.getClass());
			ret = mappings.stream().map(binder::getOutputByBindings).filter(output -> !output.getTriplesInAND().isEmpty()).collect(Collectors.toList());
		}
		else if(!boundTriplesInAND.isEmpty() && !binder.hasFreeVariables()){
			//invoke ask and ret 
			InfEngineOutput output = binder.getOutputByBindings(Collections.emptyMap());
			if (output.getTriplesInAND().stream().allMatch(kbExplorer::checkTriple))
				ret.add(output);
		}
		else { //per gestire il caso che una regola non abbia trigger conditions (è il caso delle triple generation rules che si basano solo sulla precondition, del tipo <A, disjointWith, B> => <B,disjointWith, A>
			InfEngineOutput infEngineOutput = binder.getOutputByBindings(Collections.emptyMap());
			ret = Collections.singletonList(infEngineOutput);
		}
		return ret;
	}
	
	//il validator dovrà chiamare prima il fire facendosi ritornare la Map<Binder,List<ConsistenceRule>>, per poi iterare su ciascun 
	//consistence rule di ciascun binder invocando getTriplesInANDbyBoundTriples con ogni regola ed il relativo binder
	
	
	
	
	
	public InfEngineOutputIterator<T,B> fire(Triple triple){
		Map<B,List<T>> map = firePartially(triple);
		return new InfEngineOutputIterator<>(map, this);
	}
	 
	
	
	private boolean verifyTriggingRestriction(Triple triple, PreFilter triggingRestriction){
		boolean ret = true;
		
		/*BindingTripleElementRole role = triggingRestriction.getRole();
		ValueType valueType = triggingRestriction.getValueType();
		
		if(ValueType.IS_CLASS.equals(valueType)){
			
			if(BindingTripleElementRole.SUBJECT.equals(role)) 
				ret = kbExplorer.checkTriple(triple.getSubject(), RDF.TYPE, OWL.CLASS);
			else if(BindingTripleElementRole.PREDICATE.equals(role))
				ret = kbExplorer.checkTriple(triple.getPredicate(), RDF.TYPE, OWL.CLASS);
			else if(BindingTripleElementRole.OBJECT.equals(role)){
				Value object = triple.getObject();
				ret = (object instanceof Resource) && kbExplorer.checkTriple((Resource) triple.getObject(), RDF.TYPE, OWL.CLASS);
			}
			else throw new IllegalArgumentException("Role " + role + " not handled");
			
		}
		*/
		//GESTIRE GLI ALTRI CASI SE SI DECIDE DI USARE LE TRIGGING RESTRICITIONS
		
		return ret;
		
		
	}

	


}
