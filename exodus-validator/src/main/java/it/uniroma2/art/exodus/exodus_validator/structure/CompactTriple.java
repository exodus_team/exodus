package it.uniroma2.art.exodus.exodus_validator.structure;

public class CompactTriple {
	private String subject;
	private String predicate;
	private String object;
	
	public CompactTriple(String subject, String predicate, String object) {
		this.subject = subject;
		this.predicate = predicate;
		this.object = object;
	}

	public String getSubject() {
		return subject;
	}

	public String getPredicate() {
		return predicate;
	}

	public String getObject() {
		return object;
	}
	
	
	@Override
	public String toString(){
		return "< "+subject+" , "+predicate + " , " + object + " >";
	}
	
	
}
