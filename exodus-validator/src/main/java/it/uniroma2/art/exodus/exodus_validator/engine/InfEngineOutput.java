package it.uniroma2.art.exodus.exodus_validator.engine;

import java.util.ArrayList;
import java.util.List;

import it.uniroma2.art.exodus.exodus_validator.structure.Triple;

public class InfEngineOutput {
	
	private List<Triple> triplesInAND;
	
	

	public InfEngineOutput(List<Triple> triplesInAND) {
		this.triplesInAND = new ArrayList<>(triplesInAND);
	}



	public List<Triple> getTriplesInAND() {
		return new ArrayList<>(triplesInAND);
	}
	
	public String toString(){
		return triplesInAND.toString();
	}
	
	

}
