package it.uniroma2.art.exodus.exodus_validator.ui;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.AbstractMap.SimpleEntry;
import java.util.Collections;
import java.util.Enumeration;

import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.bidimap.TreeBidiMap;
import org.apache.commons.lang.StringUtils;

import it.uniroma2.art.exodus.exodus_validator.BoxTransitionHandler;
import it.uniroma2.art.exodus.exodus_validator.ExpressionUtils;
import it.uniroma2.art.exodus.exodus_validator.Transition;
import it.uniroma2.art.exodus.exodus_validator.structure.Box;
import it.uniroma2.art.exodus.exodus_validator.structure.BoxLocation;
import it.uniroma2.art.exodus.exodus_validator.structure.BoxType;
import it.uniroma2.art.exodus.exodus_validator.structure.Constraint;
import it.uniroma2.art.exodus.exodus_validator.structure.ValidationHandler;

public class ValidatorJListManager{
	
	private ValidationHandler validationHandler;
	
	private Map<BoxType,JList<BoxLocation>> jListMap;
	private ManualValidationGUI manualValidationGUI;
	
	private BoxType currBoxType;
	private BoxLocation currSelected;
	
	private BoxJListTransitionHandler transitionHandler;

	
	private int modelSize;
	
	
	public ValidatorJListManager(ManualValidationGUI manualValidationGUI){
		jListMap = new HashMap<>();
		this.manualValidationGUI = manualValidationGUI;
		
		validationHandler = ValidationHandler.getInstance();
		this.modelSize = 0;
		
		//non posso raggrupparle perché nel settare le JList mi serve da sapere il size totale delle Box
		for(int i = 0 ; i < BoxType.values().length ; i++){
			BoxType boxType = BoxType.values()[i];
			Box box = validationHandler.getBoxByType(boxType);
			this.modelSize = this.modelSize + box.size();
		}
		
		for(int i = 0 ; i < BoxType.values().length ; i++){
			BoxType boxType = BoxType.values()[i];
			Box box = validationHandler.getBoxByType(boxType);
			JList<BoxLocation> jList = createJList(box.getBoxLocationsList());
			jListMap.put(boxType, jList);
		}
		
	
		transitionHandler = new BoxJListTransitionHandler(jListMap);
		
		currBoxType = BoxType.PLUS;
		
		currSelected = null;
	}
	
	
	
	
	
	public JList<BoxLocation> selectJlist(BoxType boxType, Predicate<BoxLocation> filterPred){
		if(!boxType.equals(currBoxType) || filterPred!=null){
			
			JList<BoxLocation> selectedJlist = jListMap.get(boxType);
			
			if(filterPred!=null){
				BoxLocationListModel completeModel = (BoxLocationListModel) selectedJlist.getModel();	
				List<BoxLocation> onlyOriginalTriples = completeModel.getBoxLocationList().stream().filter(filterPred).collect(Collectors.toList());
				selectedJlist = createJList(onlyOriginalTriples);
			}
		
			selectedJlist.setVisible(true);
			selectedJlist.setSelectedIndex(-1);
			currSelected = null;
			manualValidationGUI.setSelectedItemInfo("");
			
			currBoxType = boxType;
			
			return selectedJlist;
			
		}
		
		else return jListMap.get(currBoxType);
	}
	
	
	
	
	
	private JList<BoxLocation> createJList(List<BoxLocation> items){
		JList<BoxLocation> jList = new JList<>();
		BoxLocationListModel model = new BoxLocationListModel();
		
		items.forEach(model::addElement);
		jList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		jList.setModel(model);
		jList.setVisible(true);
		
		jList.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent le) {
		        int index = jList.getSelectedIndex();
		        if (index != -1){
		        	currSelected = model.get(index);
		        	Set<BoxType> boxTypesAllowed = Transition.transitionTypesAllowed(currSelected);
		        	manualValidationGUI.enableRadioButtons(boxTypesAllowed);
		        	setTextToLabel();
		        }
		        else{
		        	currSelected = null;
		        	manualValidationGUI.enableRadioButtons(Collections.emptySet());
		        	manualValidationGUI.setSelectedItemInfo("");
		        }
		    }
		});
		
		return jList;
		
	}
	
	
	
	
	private void setTextToLabel(){
		String text = "";
		
		if(currSelected!=null){
			
			text = "Id: " + currSelected.getId();
			
			text = text + System.lineSeparator() + currSelected.toString();
			
			text = text + System.lineSeparator() + "Triple "+ (currSelected.isTripleInferred() ? "Generated" : "Original");
			
			if(currSelected.getConflictConstraint()!= null)
				text = text + System.lineSeparator() + "Conflict Constraint: " + ExpressionUtils.getRefinedExpression(currSelected.getConflictConstraint().getTarget()).toString();
			
			if(currSelected.getDependencyConstraint()!= null)
				text = text + System.lineSeparator() + "Generation Constraint: " + ExpressionUtils.getRefinedExpression(currSelected.getDependencyConstraint().getTarget()).toString();
			
			Iterator<Constraint> pointingConflictConstraintsIt = currSelected.getPointingConflictConstraints();
			if(pointingConflictConstraintsIt.hasNext()){
				text = text + System.lineSeparator() + "The following triples have a conflict constraints with this one:";
				text = text + System.lineSeparator() + "\t"+ pointingConflictConstraintsIt.next().getSource();
				while(pointingConflictConstraintsIt.hasNext())
					text = text + System.lineSeparator() + "\t"+ pointingConflictConstraintsIt.next().getSource();
			}
			
			Iterator<Constraint> pointingDependencyConstraintsIt = currSelected.getPointingDependencyConstraints();
			if(pointingDependencyConstraintsIt.hasNext()){
				text = text + System.lineSeparator() + "The following triples have a generation constraints with this one:";
				text = text + System.lineSeparator() + "\t"+ pointingDependencyConstraintsIt.next().getSource();
				while(pointingDependencyConstraintsIt.hasNext())
					text = text + System.lineSeparator() + "\t"+ pointingDependencyConstraintsIt.next().getSource();
			}
			
		}
		
		
		manualValidationGUI.setSelectedItemInfo(text);
	}
	
	
	
	public void executeTransition(BoxType targetBoxType){
		Transition<BoxTransitionHandler> transition = new Transition<>(transitionHandler, currSelected, targetBoxType);
		transition.execute();
		currSelected = null;
    	manualValidationGUI.enableRadioButtons(Collections.emptySet());
    	manualValidationGUI.setSelectedItemInfo("");
	}
	
	
	
	public List<BoxLocation> getBoxLocationListByBoxType(BoxType boxType){
		return ((BoxLocationListModel) jListMap.get(boxType).getModel()).getBoxLocationList();
	}
	
	
	public void putIntoValidatedBoxAllPlusBoxItems(){
		List<BoxLocation> plusBoxLocations = ((BoxLocationListModel) jListMap.get(BoxType.PLUS).getModel()).getBoxLocationList();
		plusBoxLocations.stream().filter(plusBoxLocation -> !plusBoxLocation.isTripleInferred()).forEach(plusBoxLocation -> {
			Transition<BoxTransitionHandler> transition = new Transition<>(transitionHandler, plusBoxLocation, BoxType.VALIDATED);
			transition.execute();
		});
		
		currSelected = null;
    	manualValidationGUI.enableRadioButtons(Collections.emptySet());
    	manualValidationGUI.setSelectedItemInfo("");
	}

	
	public JList<BoxLocation> getJListByBoxType(BoxType boxType){
		return jListMap.get(boxType);
	}
	
	
	public BoxLocation getCurrSelected(){
		return currSelected;
	}
	
	public BoxType getCurrBoxType(){
		return currBoxType;
	}

}
