package it.uniroma2.art.exodus.exodus_validator.structure;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

//import java.util.Map;
//import java.util.TreeMap;

public class Box {
	private Map<Integer, BoxLocation> boxLocations;
	private BoxType type;
	
	public Box(BoxType type){
		this.boxLocations = new LinkedHashMap<>();
		this.type = type;
	}
	
	public void addBoxLocation(BoxLocation boxLocation){
		boxLocations.put(boxLocation.getId(), boxLocation);
	}
	
	public BoxType getBoxType(){
		return this.type;
	}
	
	public BoxLocation getBoxLocationAt(int index){
		return boxLocations.get(index);
	}
	
	public void removeBoxLocationAt(int index){
		boxLocations.remove(index);
	}
	
	public void removeBoxLocation(BoxLocation boxLocation){
		boxLocations.remove(boxLocation.getId());
	}
	
	public List<BoxLocation> getBoxLocationsList(){
		return new ArrayList<>(boxLocations.values());
	}
	
	public Iterator<BoxLocation> getBoxLocationIterator(){
		return boxLocations.values().iterator();
	}
	
	@Override
	public String toString(){
		return boxLocations.toString();
	}
	
	
	public int size(){
		return boxLocations.size();
	}
	
	public long inferredSize(){
		return boxLocations.values().stream().filter(boxLocation -> boxLocation.isTripleInferred()).count();
	}
	
	
}
