package it.uniroma2.art.exodus.exodus_validator.engine;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.eclipse.rdf4j.model.Value;

import it.uniroma2.art.exodus.exodus_validator.StatementGenerator;
import it.uniroma2.art.exodus.exodus_validator.structure.Triple;

public class TripleGenerationInfEngineBinder extends Binder<TripleGenerationInfRule>{
	
	private static final Logger logger = LogManager.getLogger(TripleGenerationInfEngineBinder.class);
	
	public TripleGenerationInfEngineBinder(Triple triple, BindingTriple precondition) {
		super(triple, precondition);
	}

	private BindingTriple inferredBoundTriple = null;
	
	@Override
	public void bind(TripleGenerationInfRule infRule){
		super.bind(infRule);
		this.inferredBoundTriple = getBoundTriple(infRule.getInferred(), this.bindings);
	}
	
	

	@Override
	public TripleGenerationInfEngineOutput getOutputByBindings(Map<Variable,Value> mapper){
		List<Triple> boundTriples = this.boundTriples.stream().map(boundTriple -> getTriple(boundTriple, mapper)).collect(Collectors.toList());
		
		Triple inferredTriple = getTriple(this.inferredBoundTriple, mapper);
		TripleGenerationInfEngineOutput output = new TripleGenerationInfEngineOutput(boundTriples, inferredTriple);
		logger.debug("TripleGenerationInfEngineOutput: " + output);
		return output;
		
	}
}
