package it.uniroma2.art.exodus.exodus_validator;

import it.uniroma2.art.exodus.exodus_validator.structure.BoxLocation;
import it.uniroma2.art.exodus.exodus_validator.structure.BoxType;

public abstract class BoxTransitionHandler {

	public static ValidationPhase validationPhase = ValidationPhase.AUTOMATIC_VALIDATION;

	public abstract void move(BoxLocation boxLocation, BoxType boxType);
	
	public abstract void remove(BoxLocation boxLocation);

}
