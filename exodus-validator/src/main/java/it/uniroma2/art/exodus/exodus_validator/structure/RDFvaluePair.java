package it.uniroma2.art.exodus.exodus_validator.structure;

import org.eclipse.rdf4j.model.Value;

public class RDFvaluePair<T extends Value> {
	
	private T ontologyRDFvalue;
	private T graphRDFvalue;
	
	public RDFvaluePair(T ontologyRDFvalue, T graphRDFvalue){
		this.ontologyRDFvalue = ontologyRDFvalue;
		this.graphRDFvalue = graphRDFvalue; 
	}

	public T getOntologyRDFvalue() {
		return ontologyRDFvalue;
	}

	public void setOntologyRDFvalue(T ontologyRDFvalue) {
		this.ontologyRDFvalue = ontologyRDFvalue;
	}

	public T getGraphRDFvalue() {
		return graphRDFvalue;
	}

	public void setGraphRDFvalue(T graphRDFvalue) {
		this.graphRDFvalue = graphRDFvalue;
	}
	
	
	
	
	

}
