package it.uniroma2.art.exodus.exodus_validator.engine;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public class TriggingCondition {
	
	//for example: equivalentClass(D,A) AND disjointWith(D,B)
	
	private List<BindingTriple> bindingTriplesInAND;

	public TriggingCondition(Collection<BindingTriple> bindingTriplesInAND) {
		this.bindingTriplesInAND = new ArrayList<>(bindingTriplesInAND);
	}
	
	public TriggingCondition(){
		this.bindingTriplesInAND = new ArrayList<>();
	}
	
	public Collection<BindingTriple> getBindingTriplesInAND(){
		return new ArrayList<>(this.bindingTriplesInAND);
	}
	
	public void addBindingTriple(BindingTriple bindingTriple){
		this.bindingTriplesInAND.add(bindingTriple);
	}
	
}
