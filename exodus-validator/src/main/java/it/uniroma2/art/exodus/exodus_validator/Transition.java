package it.uniroma2.art.exodus.exodus_validator;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import it.uniroma2.art.exodus.exodus_validator.structure.BoxLocation;
import it.uniroma2.art.exodus.exodus_validator.structure.BoxLocationEvaluatorFactory;
import it.uniroma2.art.exodus.exodus_validator.structure.BoxType;
import it.uniroma2.art.exodus.exodus_validator.structure.Constraint;
import it.uniroma2.art.exodus.exodus_validator.structure.Evaluator;
import it.uniroma2.art.exodus.exodus_validator.structure.Expression;
import it.uniroma2.art.exodus.exodus_validator.structure.RuleType;
import it.uniroma2.art.exodus.exodus_validator.structure.TransitionType;

public class Transition<T extends BoxTransitionHandler> {

	private BoxLocation boxLocation;
	private TransitionType transitionType;
	
	private T boxTransitionHandler;
	
	public Transition(T boxTransitionHandler, BoxLocation boxLocation, TransitionType transitionType) {
		if(!boxLocation.getBoxType().equals(transitionType.getSourceType()))
			throw new IllegalArgumentException("box type has to be equals to transition source type");
		this.boxLocation = boxLocation;
		this.transitionType = transitionType;
		
		this.boxTransitionHandler = boxTransitionHandler;
	}
	
	public Transition(T boxTransitionHandler, BoxLocation boxLocation, BoxType targetBoxType){
		this.transitionType = TransitionType.get(boxLocation.getBoxType(), targetBoxType);
		this.boxLocation = boxLocation;
		this.boxTransitionHandler = boxTransitionHandler;
	}
	
	public void execute(){
		if (transitionType.equals(TransitionType.FROM_INTERROGATION_TO_PLUS))
			fromINTERROGATIONtoPLUS(boxLocation);
		else if(transitionType.equals(TransitionType.FROM_INTERROGATION_TO_REJECTED))
			fromPLUS_INTERROGATIONtoREJECTED(boxLocation);
		else if(transitionType.equals(TransitionType.FROM_PLUS_TO_INTERROGATION))
			fromPLUStoINTERROGATION(boxLocation);
		else if(transitionType.equals(TransitionType.FROM_PLUS_TO_REJECTED))
			fromPLUS_INTERROGATIONtoREJECTED(boxLocation);
		else if(transitionType.equals(TransitionType.FROM_PLUS_TO_VALIDATED))
			fromPLUStoACCEPTED(boxLocation);
		
		else if(transitionType.equals(TransitionType.FROM_INTERROGATION_TO_MINUS))
			fromPLUS_INTERROGATIONtoMINUS(boxLocation);
		else if(transitionType.equals(TransitionType.FROM_PLUS_TO_MINUS))
			fromPLUS_INTERROGATIONtoMINUS(boxLocation);
		
	}
	
	public static Set<BoxType> transitionTypesAllowed(BoxLocation boxLocation){
		BoxType sourceBoxLocationType = boxLocation.getBoxType();
		Set<BoxType> allowedTransitions = new HashSet<>();
		
		if(boxLocation.isTripleInferred())
			return Collections.emptySet();
		
		if(sourceBoxLocationType.equals(BoxType.PLUS)){
			allowedTransitions.add(BoxType.INTERROGATION);
			allowedTransitions.add(BoxType.REJECTED);
			allowedTransitions.add(BoxType.VALIDATED);
		}
		else if(sourceBoxLocationType.equals(BoxType.INTERROGATION)){
			allowedTransitions.add(BoxType.REJECTED);
			if(checkTransitionFromInterrogationToPlus(boxLocation))
				allowedTransitions.add(BoxType.PLUS);
		}
		return allowedTransitions;
	}
	
	
	
	
	
	private void fromPLUStoINTERROGATION(BoxLocation boxLoc){
		boxTransitionHandler.move(boxLoc, BoxType.INTERROGATION);
		
		Iterator<Constraint> pointingDepConstraintsIt = boxLoc.getPointingDependencyConstraints();
		pointingDepConstraintsIt.forEachRemaining(pointingDepConstr -> {
			BoxLocation inferredBoxLoc = pointingDepConstr.getSource();
			if(inferredBoxLoc.getBoxType()== null || inferredBoxLoc.getBoxType().compareTo(BoxType.INTERROGATION) > 0){
				Expression<BoxLocation> pointingDepConstrExpr = pointingDepConstr.getTarget();
				boolean exprValue = pointingDepConstrExpr.evaluate(BoxLocationEvaluatorFactory.getDefaultEvaluator());
				if(!exprValue)
					fromPLUStoINTERROGATION(inferredBoxLoc);
			}
		});
	}
	
	
	
	
	
	private void fromPLUS_INTERROGATIONtoREJECTED(BoxLocation boxLoc){
		boxTransitionHandler.move(boxLoc, BoxType.REJECTED);
		
		Iterator<Constraint> pointingDepConstraintsIt = boxLoc.getPointingDependencyConstraints();
		pointingDepConstraintsIt.forEachRemaining(pointingDepConstr -> {
			BoxLocation inferredBoxLoc = pointingDepConstr.getSource();
			if(!inferredBoxLoc.getBoxType().equals(BoxType.REJECTED) && !inferredBoxLoc.getBoxType().equals(BoxType.MINUS)){
				Expression<BoxLocation> pointingDepConstrExpr = pointingDepConstr.getTarget();
				boolean exprValue = pointingDepConstrExpr.evaluate(BoxLocationEvaluatorFactory.getEvaluator(transitionType, RuleType.INFERRED_TRIPLES_GENERATION));
				if(!exprValue)
					fromPLUS_INTERROGATIONtoREJECTED(inferredBoxLoc);
					
			}
		});
	}
	
	
	
	
	
	private void fromPLUStoACCEPTED(BoxLocation boxLoc){
		boxTransitionHandler.move(boxLoc, BoxType.VALIDATED);
		
		Iterator<Constraint> pointingDepConstraintsIt = boxLoc.getPointingDependencyConstraints();
		pointingDepConstraintsIt.forEachRemaining(pointingDepConstr -> {
			BoxLocation inferredBoxLoc = pointingDepConstr.getSource();
			if(!inferredBoxLoc.getBoxType().equals(BoxType.VALIDATED) && !isMinusNotMoveable(inferredBoxLoc)){
				Expression<BoxLocation> pointingDepConstrExpr = pointingDepConstr.getTarget();
				boolean exprValue = pointingDepConstrExpr.evaluate(BoxLocationEvaluatorFactory.getEvaluator(transitionType, RuleType.INFERRED_TRIPLES_GENERATION));
				if(exprValue)
					fromPLUStoACCEPTED(inferredBoxLoc);
					
			}
		});
	}
	
	
	
	
	private void fromINTERROGATIONtoPLUS(BoxLocation boxLoc){
		if(checkTransitionFromInterrogationToPlus(boxLoc)){
			boxTransitionHandler.move(boxLoc, BoxType.PLUS);
			transitionFromInterrogationToPlus_inferr(boxLoc);
			
			Iterator<Constraint> pointingConflictConstraintsIt = boxLoc.getPointingConflictConstraints();
			pointingConflictConstraintsIt.forEachRemaining(pointingConflictConstr -> {
				BoxLocation conflictBoxLoc = pointingConflictConstr.getSource();
				if(conflictBoxLoc.getBoxType().equals(BoxType.PLUS)){
					Expression<BoxLocation> pointingConflictConstrExpr = pointingConflictConstr.getTarget();
					boolean exprValue = pointingConflictConstrExpr.evaluate(BoxLocationEvaluatorFactory.getDefaultEvaluator());
					if(exprValue)
						fromPLUStoINTERROGATION(conflictBoxLoc);
						
				}
			});
		}
		else throw new TransitionNotAllowedException("Transition from Interrogation to Plus Box is not allowed for box location " + boxLoc);
	}

	
	
	
	
	
	public static boolean checkTransitionFromInterrogationToPlus(BoxLocation boxLoc){
		Constraint conflictConstraint = boxLoc.getConflictConstraint();
		if(conflictConstraint!=null){
			Expression<BoxLocation> conflictConstrExpr = conflictConstraint.getTarget();
			boolean exprValue = conflictConstrExpr.evaluate(BoxLocationEvaluatorFactory.getEvaluator(TransitionType.FROM_INTERROGATION_TO_PLUS, RuleType.CONSISTENCY_CHECKING));
			if(exprValue)
				return false;
		}
		Iterator<Constraint> pointingConflictConstraintsIt = boxLoc.getPointingConflictConstraints();
		while(pointingConflictConstraintsIt.hasNext()){
			Constraint pointingConflictConstr = pointingConflictConstraintsIt.next();
			BoxLocation conflictBoxLoc = pointingConflictConstr.getSource();
			if(conflictBoxLoc.getBoxType().equals(BoxType.VALIDATED)){
				Expression<BoxLocation> pointingConflictConstrExpr = pointingConflictConstr.getTarget();
				Evaluator<BoxLocation> baseEvaluator = BoxLocationEvaluatorFactory.getDefaultEvaluator();
				Evaluator<BoxLocation> complexEvaluator = BoxLocationEvaluatorFactory.getEvaluator(baseEvaluator, Collections.singleton(boxLoc), Collections.emptySet());
				boolean exprValue = pointingConflictConstrExpr.evaluate(complexEvaluator);
				if(exprValue)
					return false;		
			}
		}
		return true;
	}
	
	

	
	private void transitionFromInterrogationToPlus_inferr(BoxLocation boxLoc) {
		boxTransitionHandler.move(boxLoc, BoxType.PLUS);
		
		Iterator<Constraint> pointingDepConstraintsIt = boxLoc.getPointingDependencyConstraints();
		pointingDepConstraintsIt.forEachRemaining(pointingDepConstr -> {
			BoxLocation inferredBoxLoc = pointingDepConstr.getSource();
			if(!inferredBoxLoc.getBoxType().equals(BoxType.PLUS) && !isMinusNotMoveable(inferredBoxLoc) ){
				Expression<BoxLocation> pointingDepConstrExpr = pointingDepConstr.getTarget();
				boolean exprValue = pointingDepConstrExpr.evaluate(BoxLocationEvaluatorFactory.getDefaultEvaluator());
				if(exprValue)
					transitionFromInterrogationToPlus_inferr(inferredBoxLoc);	
			}
		});
		
	}
	
	

	
	
	
	private void fromPLUS_INTERROGATIONtoMINUS(BoxLocation boxLoc){
		if(!BoxTransitionHandler.validationPhase.equals(ValidationPhase.AUTOMATIC_VALIDATION))
			throw new TransitionNotAllowedException("Transition to '-Box' is allowed only in automatic validation phase");
		
		boxTransitionHandler.move(boxLoc, BoxType.MINUS);
		
		Iterator<Constraint> pointingDepConstraintsIt = boxLoc.getPointingDependencyConstraints();
		pointingDepConstraintsIt.forEachRemaining(pointingDepConstr -> {
			BoxLocation inferredBoxLoc = pointingDepConstr.getSource();
			if(!inferredBoxLoc.getBoxType().equals(BoxType.MINUS)){
				Expression<BoxLocation> pointingDepConstrExpr = pointingDepConstr.getTarget();
				boolean exprValue = pointingDepConstrExpr.evaluate(BoxLocationEvaluatorFactory.getEvaluator(transitionType, RuleType.INFERRED_TRIPLES_GENERATION));
				if(!exprValue)
					fromPLUS_INTERROGATIONtoMINUS(inferredBoxLoc);
					
			}
		});
	}
	
	
	
	private boolean isMinusNotMoveable(BoxLocation boxLocation){
		boolean ret = BoxType.MINUS.equals(boxLocation.getBoxType()) 
				&& ValidationPhase.MANUAL_VALIDATION.equals(BoxTransitionHandler.validationPhase);
		if(ret && !boxLocation.isTripleInferred())
			throw new IllegalStateException("Triple not inferred in -Box has dependency constraints");
		return ret;
	}
	
	
	
	
}
