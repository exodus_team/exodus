package it.uniroma2.art.exodus.exodus_validator.engine;

import java.util.Collection;
import java.util.List;

public class ConsistenceInfRule extends InfRule{
	// for example: <A, subClassOf, B> -> (∃D <D, owl:equivalentClass, A> AND <D, owl:disjointWith, B> ) => inconsistence (1.1)
	//				<A, subClassOf, B> -> (∃D <D, rdfs:subClassOf, A> AND <D, owl:disjointWith, B> ) => inconsistence (1.1)
	
	public ConsistenceInfRule(BindingTriple preconditionBindingTriple, Collection<BindingTriple> triggingConditions){
		super(preconditionBindingTriple, triggingConditions);
	}
	
	
	@Override
	public boolean equals(Object other){
		boolean ret = false;
		if(other instanceof ConsistenceInfRule){
			ConsistenceInfRule otherConsistenceInfRule = (ConsistenceInfRule) other;
			if(super.equals(otherConsistenceInfRule))
				ret = true;
		}
		return ret;
	}
	

}
