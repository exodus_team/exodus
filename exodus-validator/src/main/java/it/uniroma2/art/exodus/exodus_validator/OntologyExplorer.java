package it.uniroma2.art.exodus.exodus_validator;



import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.vocabulary.OWL;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.BooleanQuery;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.sail.memory.MemoryStore;

import it.uniroma2.art.exodus.exodus_validator.engine.BindingTriple;
import it.uniroma2.art.exodus.exodus_validator.engine.BindingTripleElement;
import it.uniroma2.art.exodus.exodus_validator.engine.InfRule;
import it.uniroma2.art.exodus.exodus_validator.engine.Variable;
import it.uniroma2.art.exodus.exodus_validator.structure.Triple;

public abstract class OntologyExplorer<T extends Resource, C extends Collection<Set<LabeledResource<T>>>> {
	
	private static final Logger logger = LogManager.getLogger(OntologyExplorer.class);
	
	private static Map<Class<? extends InfRule>, Integer> queryCounterByRuleType = new HashMap<>();
	
	protected boolean containsAlsoInferredTriples;
	
	private String createLabelsQueryString(String resourceId){
		String labelsQueryString = 
			"prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> "+
			"prefix owl: <http://www.w3.org/2002/07/owl#> "+
			"SELECT (str(?label) as ?classLabel ) "+
			"WHERE { "+
				"?res rdfs:label ?label. "+
			    "filter ( ?res = " + resourceId + " && (  LANGMATCHES(LANG(?label),\"en\") || LANG(?label) = \"\"   )   ) "+
			"} ";
		return labelsQueryString;
	}
	
	private static final String allClassesQueryString = "prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> "+
													"prefix owl: <http://www.w3.org/2002/07/owl#> "+
													"SELECT ?type "+
													"WHERE { "+
													"?type a owl:Class. "+
													"}";
	
	private static final String allUriClassesQueryString = "prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> "+
			"prefix owl: <http://www.w3.org/2002/07/owl#> "+
			"SELECT ?type "+
			"WHERE { "+
			"?type a owl:Class. "+
			"FILTER (isURI(?type)) "+
			"}";
	
	
	/*
	 * get all english and no-language labels for a given uri resource
	 * 
	 * prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
		prefix owl: <http://www.w3.org/2002/07/owl#>

		SELECT str(?label) as ?classLabel
		WHERE {

			?res rdfs:label ?label.
                     
		        filter ( isURI(?res) && ?res = <http://dbpedia.org/ontology/Database> && (  LANGMATCHES(LANG(?label),"en") || LANG(?label) = ""   )   )
		}
		
		
		get all uri classes:
		
		prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
		prefix owl: <http://www.w3.org/2002/07/owl#>
		SELECT ?type
		WHERE {
		     ?type a owl:Class.
		     FILTER (isURI(?type))
		}
		
		
		get all classes:
		
		prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
		prefix owl: <http://www.w3.org/2002/07/owl#>
		SELECT ?type
		WHERE {
		     ?type a owl:Class.
		}
	 */
	
	
	//protected Model ontologyModel;
	protected C resorcesToExplore;
	//private Repository db;
	protected RepositoryConnection conn;
	
	
	public OntologyExplorer(Model ontologyModel){
		this(ontologyModel, false);
	}
	
	
	public OntologyExplorer(Model ontologyModel, boolean containsAlsoInferredTriples){
		//resorcesToExplore = new LinkedBlockingQueue<Set<LabeledResource<T>>>();
		//this.ontologyModel = ontologyModel;
		
		this.containsAlsoInferredTriples = containsAlsoInferredTriples;
		
		Repository db = new SailRepository(new MemoryStore());
		db.initialize();
		
		conn = db.getConnection();
		conn.add(ontologyModel);
		
		initializeResourcesToExplore();
		
	}
	
	
	protected abstract void initializeResourcesToExplore();
	
	public abstract Set<LabeledResource<T>> visit();
	public abstract boolean hasOtherItems();
	
	
	
	
	protected String buildInClause(Collection<Resource> ontClasses){
		String inClause = "";
		
		if(ontClasses.size() > 0){
			for(Resource ontClass : ontClasses){
				String resourceId;
				if(ontClass instanceof IRI)
					resourceId = "<"+((IRI) ontClass).toString()+">";
				else resourceId = "<"+ontClass.stringValue()+">";
				inClause = inClause + resourceId + ",";
			}
			inClause = inClause.substring(0, inClause.length()-1);
		}
	
		return inClause;
	}
	
	
	
	
	public List<String> getLabels(T resource){
		List<String> ret= new ArrayList<String>();
		String resourceId;
		if(resource instanceof IRI)
			resourceId = "<" + ((IRI) resource).toString() + ">";
		else if (resource instanceof Resource)
			resourceId = "<" + resource.stringValue() + ">";
		else resourceId = resource.stringValue();
		
		String queryString = createLabelsQueryString(resourceId);
		
		
		TupleQuery query = conn.prepareTupleQuery(queryString);
	    
	    try (TupleQueryResult result = query.evaluate()) {
		
			while (result.hasNext()) {
			    BindingSet solution = result.next();
			    Value currValue = solution.getValue("classLabel");
			    ret.add(currValue.stringValue());
			}
	    }
	    return ret;
	}
	
	
	
	
	
	public Set<Value> getAllClasses(){
		Set<Value> ret= new HashSet<Value>();
		
		TupleQuery query = conn.prepareTupleQuery(allClassesQueryString);
	    
	    try (TupleQueryResult result = query.evaluate()) {
		
			while (result.hasNext()) {
			    BindingSet solution = result.next();
			    Value currValue = solution.getValue("type");
			    ret.add(currValue);
			}
	    }
	    return ret;
	}
	
	
	
	public static <R extends InfRule> int getQueryCounterByRuleClass(Class<R> infRuleClass){
		return queryCounterByRuleType.getOrDefault(infRuleClass, 0);
	}
	
	
	private <R extends InfRule>  void incrementQueryCounter (Class<R> infRuleClass){
		int countAsIs = queryCounterByRuleType.getOrDefault(infRuleClass, 0);
		queryCounterByRuleType.put(infRuleClass, countAsIs +1);
	}
	
	
	public <R extends InfRule> List<Map<Variable,Value>> getMappingsByBoundTriples(List<BindingTriple> boundTriplesInAND, Class<R> infRuleClass){
		incrementQueryCounter(infRuleClass);
		return getMappingsByBoundTriples(boundTriplesInAND);
	}
	
	
	//it returns a list of collections of triples, where each collection represents a list of triples that TOGETHER 
	// generates a conflict
	private List<Map<Variable,Value>> getMappingsByBoundTriples(List<BindingTriple> boundTriplesInAND){
		List<Map<Variable,Value>> ret = new ArrayList<>();
		
		Set<Variable> freeVariables = new HashSet<>();
		
		String prefix = "prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> "+
				"prefix owl: <http://www.w3.org/2002/07/owl#> ";
		
		String whereClause = "WHERE {" + boundTriplesInAND.stream().map(boundTriple -> {
			BindingTripleElement boundSubject = boundTriple.getBindingSubject();
			BindingTripleElement boundPredicate = boundTriple.getBindingPredicate();
			BindingTripleElement boundObject = boundTriple.getBindingObject();
			
			if(boundSubject instanceof Variable)
				freeVariables.add((Variable) boundSubject);
			if(boundPredicate instanceof Variable)
				freeVariables.add((Variable) boundPredicate);
			if(boundObject instanceof Variable)
				freeVariables.add((Variable) boundObject);
			
			return " "+ boundSubject.toString() + " "+ boundPredicate.toString() + " " + boundObject.toString() + " . ";
		}).reduce((s1, s2) -> s1 + s2).orElse("") + "}";
		

		String selectClause = "SELECT "+ freeVariables.stream().map(variable -> variable.toString() + " ").reduce((s1,s2)->s1+s2).orElse("");
		
		String queryString = prefix + selectClause + whereClause;
		
		logger.debug(queryString);
		
		TupleQuery query;
		try{
			query = conn.prepareTupleQuery(queryString);
		}
		catch(Exception e){
			logger.warn("An error occours during query preparation", e);
			return ret;
		}
		
		
		try (TupleQueryResult result = query.evaluate()) {
			
			while (result.hasNext()) {
				Map<Variable,Value> currMapping = new HashMap<>();
			    
				BindingSet solution = result.next();
			    freeVariables.forEach(variable -> {
			    	Value currValue = solution.getValue(variable.getName());
			    	currMapping.put(variable, currValue);
			    });
			    
			    ret.add(currMapping);  	
			}
		}

		return ret;	
	}
	
	
	
	public Set<Resource> getAllUriClasses(){
		Set<Resource> ret= new HashSet<>();
		
		TupleQuery query = conn.prepareTupleQuery(allUriClassesQueryString);
	    
	    try (TupleQueryResult result = query.evaluate()) {
		
			while (result.hasNext()) {
			    BindingSet solution = result.next();
			    Value currValue = solution.getValue("type");
			    if(currValue instanceof Resource){
			    	Resource currResource = (Resource) currValue;
			    	ret.add(currResource);
			    }
			    
			}
	    }
	    return ret;
	}
	
	
	
	public boolean checkTriple(Triple triple){
		return checkTriple(triple.getSubject(), triple.getPredicate(), triple.getObject());
	}
	
	
	public boolean checkTriple(Statement triple){
		return checkTriple(triple.getSubject(), triple.getPredicate(), triple.getObject());
	}
	
	public boolean checkTriple(Resource subject, IRI predicate, Value object){
		return conn.hasStatement(subject, predicate, object, false);
	}
	
	
	public List<Statement> getAllTriples(){
		return conn.getStatements(null, null, null, false).asList();
	}
	
	
	public boolean checkResource(Resource resource){
		String resourceStringValue;
		
		if(resource instanceof IRI)
			resourceStringValue = "<" + ((IRI) resource).toString() + ">";
		else if (resource instanceof Resource)
			resourceStringValue = "<" + resource.stringValue() + ">";
		else resourceStringValue = resource.stringValue();
		
		String queryString =
				"prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> "+
				"prefix owl: <http://www.w3.org/2002/07/owl#> "+
				"ASK "+
					"{ ?s ?p ?o FILTER(?s=" + resourceStringValue + " || ?p=" + resourceStringValue + " || ?o="+ resourceStringValue +  ") } ";
		
		
		BooleanQuery query = conn.prepareBooleanQuery(queryString);
		
		return query.evaluate();
	}
	
	
	public boolean checkTriple(String subjectStringValue, String predicateIRIstring, String objectStringValue){
		String queryString =
				"prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> "+
				"prefix owl: <http://www.w3.org/2002/07/owl#> "+
				"ASK "+
					"{ " + subjectStringValue + " " + predicateIRIstring + " "+ objectStringValue +  ". } ";
		BooleanQuery query = conn.prepareBooleanQuery(queryString);
		
		return query.evaluate();
	}
	
	
	public boolean checkTriple(Resource subject, String predicateIRIstring, Value object){
		String subjectStringValue;
		String objectStringValue;
		
		if(subject instanceof IRI)
			subjectStringValue = "<" + ((IRI) subject).toString() + ">";
		else subjectStringValue = "<" + subject.stringValue() + ">";
		
		if(object instanceof IRI)
			objectStringValue = "<" + ((IRI) object).toString() + ">";
		else if (object instanceof Resource)
			objectStringValue = "<" + object.stringValue() + ">";
		else objectStringValue = object.stringValue();
		
		return checkTriple(subjectStringValue, predicateIRIstring, objectStringValue);
	}
	
	
	
	public boolean containsClass(Resource ontClass){
		String subjectStringValue;
		
		if(ontClass instanceof IRI)
			subjectStringValue = "<" + ((IRI) ontClass).toString() + ">";
		else subjectStringValue = "<" + ontClass.stringValue() + ">";
		
		String predicateIRIstring = "<" + RDF.TYPE.toString() + ">";
		String objectStringValue = "<" + OWL.CLASS.toString() + ">";
		
		return checkTriple(subjectStringValue, predicateIRIstring, objectStringValue);
	}
	
	
	
	@SuppressWarnings("deprecation")
	public List<Statement> matchStatement(Resource subject, IRI predicate, Value object){
		return conn.getStatements(subject, predicate, object, false).asList();
	}
	
	
	
	
	public List<Resource> getIndividualsByClass(Resource ontClass){
		String ontClassStringValue;
		if(ontClass instanceof IRI)
			ontClassStringValue = "<" + ((IRI) ontClass).toString() + ">";
		else ontClassStringValue = "<" + ontClass.stringValue() + ">";
		
		List<Resource> ret = new ArrayList<Resource>();
		String queryString =
				"prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> "+
				"prefix owl: <http://www.w3.org/2002/07/owl#> "+
				"SELECT DISTINCT ?ind "+
				"WHERE { "+
					"?ind a " + ontClassStringValue + " . " + ontClassStringValue + " a owl:Class . } ";
		TupleQuery query = conn.prepareTupleQuery(queryString);
		
		try (TupleQueryResult result = query.evaluate()) {
			
			while (result.hasNext()) {
			    BindingSet solution = result.next();
			    Value currValue = solution.getValue("ind");
			    if(currValue instanceof Resource){
			    	Resource currResource = (Resource) currValue;
			    	ret.add(currResource);
				}
				    	
			}
		}
		
		return ret;
		
	}
	
	
	
	public List<Resource> getClassesByIndividual(Resource individual){
		String individualStringValue;
		if(individual instanceof IRI)
			individualStringValue = "<" + ((IRI) individual).toString() + ">";
		else individualStringValue = "<" + individual.stringValue() + ">";
		
		List<Resource> ret = new ArrayList<Resource>();
		String queryString =
				"prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> "+
				"prefix owl: <http://www.w3.org/2002/07/owl#> "+
				"SELECT DISTINCT ?class "+
				"WHERE { "+
					"{ "+ individualStringValue + " a ?class . ?class a owl:Class . } ";
		TupleQuery query = conn.prepareTupleQuery(queryString);
		
		try (TupleQueryResult result = query.evaluate()) {
			
			while (result.hasNext()) {
			    BindingSet solution = result.next();
			    Value currValue = solution.getValue("class");
			    if(currValue instanceof Resource){
			    	Resource currResource = (Resource) currValue;
			    	ret.add(currResource);
				}
				    	
			}
		}
		
		return ret;
		
	}
	
	
	
	
	
	public List<Resource> getClassesByIndividuals(Collection<Resource> individuals){
		String inClause = buildInClause(individuals);
		
		List<Resource> ret = new ArrayList<Resource>();
		String queryString =
				"prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> "+
				"prefix owl: <http://www.w3.org/2002/07/owl#> "+
				"SELECT DISTINCT ?class "+
				"WHERE { "+
					" ?ind a ?class . ?class a owl:Class . " +
				"FILTER (?ind IN (" + inClause + " ) )    } ";
		
		TupleQuery query = conn.prepareTupleQuery(queryString);
		
		try (TupleQueryResult result = query.evaluate()) {
			
			while (result.hasNext()) {
			    BindingSet solution = result.next();
			    Value currValue = solution.getValue("class");
			    if(currValue instanceof Resource){
			    	Resource currResource = (Resource) currValue;
			    	ret.add(currResource);
				}
				    	
			}
		}
		
		return ret;
		
	}
	
	
	
	public List<Resource> getSubjectByPredicateAndObject(String predicateIRIstring, Value object){
		String objectStringValue;
		if(object instanceof IRI)
			objectStringValue = "<" + ((IRI) object).toString() + ">";
		else if(object instanceof Resource)
			objectStringValue =  "<" + object.stringValue() + ">";
		else objectStringValue = object.stringValue();
		
		List<Resource> ret = new ArrayList<Resource>();
		String queryString =
				"prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> "+
				"prefix owl: <http://www.w3.org/2002/07/owl#> "+
				"SELECT DISTINCT ?s "+
				"WHERE { "+
					"{?s " + predicateIRIstring + " "+ objectStringValue +  ". } ";
		
		TupleQuery query = conn.prepareTupleQuery(queryString);
		
		try (TupleQueryResult result = query.evaluate()) {
			
			while (result.hasNext()) {
			    BindingSet solution = result.next();
			    Value currValue = solution.getValue("s");
			    if(currValue instanceof Resource){
			    	Resource currResource = (Resource) currValue;
			    	ret.add(currResource);
				}
				    	
			}
		}
		
		return ret;
		
	}
	
	
	
	public List<Value> getObjectBySubjectAndPredicate(Resource subject, String predicateIRIstring){
		String subjectStringValue;
		if(subject instanceof IRI)
			subjectStringValue = "<" + ((IRI) subject).toString() + ">";
		else subjectStringValue = "<" + subject.stringValue() + ">";
		
		List<Value> ret = new ArrayList<Value>();
		String queryString =
				"prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> "+
				"prefix owl: <http://www.w3.org/2002/07/owl#> "+
				"SELECT DISTINCT ?o "+
				"WHERE { "+
					"{ "+ subjectStringValue + " " + predicateIRIstring + " ?o . } ";
		
		TupleQuery query = conn.prepareTupleQuery(queryString);
		
		try (TupleQueryResult result = query.evaluate()) {
			
			while (result.hasNext()) {
			    BindingSet solution = result.next();
			    Value currValue = solution.getValue("o");
			    ret.add(currValue);
			     	
			}
		}
		
		return ret;
		
	}
	
	
	
	
	
	public List<Resource> getSubjectByPredicateAndObject(IRI predicateIRI, Value object){
		return getSubjectByPredicateAndObject(predicateIRI.toString(), object);
	}
	
	public List<Value> getObjectBySubjectAndPredicate(Resource subject, IRI predicateIRI){
		return getObjectBySubjectAndPredicate(subject, predicateIRI.toString());
	}
	
	
	public void addTriple(Triple triple){
		addTriple(triple.getSubject(), triple.getPredicate(), triple.getObject());
	}
	
	public void addTriple(Resource subject, IRI predicate, Value object){
		ValueFactory vf = SimpleValueFactory.getInstance();
		Statement statement = vf.createStatement(subject, predicate, object);
		addTriple(statement);
	}
	
	public void addTriple(Statement statement){
		conn.add(statement);
	}
	
	
}
