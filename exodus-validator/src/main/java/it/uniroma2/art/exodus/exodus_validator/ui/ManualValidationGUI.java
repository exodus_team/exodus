package it.uniroma2.art.exodus.exodus_validator.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JViewport;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;

import org.apache.commons.lang.StringUtils;

import it.uniroma2.art.exodus.exodus_validator.structure.BoxLocation;
import it.uniroma2.art.exodus.exodus_validator.structure.BoxType;
import it.uniroma2.art.exodus.exodus_validator.structure.ValidationHandler;
import javax.swing.JRadioButton;
import javax.swing.JRadioButtonMenuItem;
import java.awt.GridLayout;
import javax.swing.JCheckBox;
import javax.swing.JTextField;

public class ManualValidationGUI  extends JFrame implements ActionListener{

	private static final long serialVersionUID = 8155760192841335006L;

	
	private JLabel running_label;
	
	private JPanel p;
    private JButton esegui;
    
    private JScrollPane boxJScrollPane;
    
    private JTextArea selectedItemInfoTextArea;
    
    private JScrollPane selectedItemInfoScrollPane;
    
    private JList<BoxLocation> currBoxJList;
    
    private Map<BoxType, JScrollPane> boxJScrollPanesMap;
    
    private ValidatorJListManager validatorJListManager;
    
    private ValidationHandler validationHandler;
    
    private Predicate<BoxLocation> filterPred = null;
    
    private GridBagConstraints gbc_boxJScrollPane;
    private JButton btnPersist;
    private JPanel panel;
    private JRadioButtonMenuItem radioButtonToPlus;
    private JRadioButtonMenuItem radioButtonToInterrogation;
    private JRadioButtonMenuItem radioButtonToAcceptation;
    private JRadioButtonMenuItem radioButtonToRejection;
    private JRadioButtonMenuItem radioButtonAcceptAllPlus;
    private ButtonGroup bottonGroup;
    private JCheckBox chckbxShowGeneratedTriples;
    private JButton btnFilter;
    private JTextField textField;
    private String filterText;
    
    
    private void init(){
    	validationHandler = ValidationHandler.getInstance();
    	this.validatorJListManager = new ValidatorJListManager(this);
    	initGBCboxJScrollPane();
    	boxJScrollPanesMap = new HashMap<>();
    	for(int i = 0 ; i < BoxType.values().length ; i++){
    		BoxType currBoxType = BoxType.values()[i];
    		JScrollPane boxJScrollPane = new JScrollPane(validatorJListManager.getJListByBoxType(currBoxType));
    		boxJScrollPanesMap.put(currBoxType, boxJScrollPane);
    	}
    }
    
    
   private void initGBCboxJScrollPane(){
	   gbc_boxJScrollPane = new GridBagConstraints();
	   gbc_boxJScrollPane.insets = new Insets(0, 0, 5, 5);
	   gbc_boxJScrollPane.fill = GridBagConstraints.BOTH;
	   gbc_boxJScrollPane.gridx = 1;
	   gbc_boxJScrollPane.gridy = 0;
	   gbc_boxJScrollPane.gridheight = 4;
   }
    
    
    public ManualValidationGUI(){
		super("Manual Validator");
		init();
		GuiManager.setUi(this);
		createAndShowGUI();
    }
	


    public Container createPane(){
    	GridBagConstraints gbc;
    	p = new JPanel();
    	gbc=new GridBagConstraints();
		gbc.anchor=GridBagConstraints.SOUTH;
		gbc.insets=new Insets(3,3,3,3);
		gbc.gridx = 0;
		gbc.gridy = 4;
		GridBagLayout gbl_p = new GridBagLayout();
		gbl_p.columnWidths = new int[]{117, 953, 312, 0};
		gbl_p.rowHeights = new int[]{23, 43, 53, 256, 59, 43, 59, 0, 0};
		gbl_p.columnWeights = new double[]{0.0, 1.0, 1.0, Double.MIN_VALUE};
		gbl_p.rowWeights = new double[]{1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		p.setLayout(gbl_p);
		
	
		String[] boxMenuItems = {"Plus Box", "Minus Box", "Interrogation Box", "Acceptation Box", "Rejection Box"};
		JComboBox<String> boxMenu = new JComboBox<>(boxMenuItems);
		boxMenu.setVisible(true);
		boxMenu.addActionListener(actionListener -> {
			if(boxMenu.getSelectedIndex()==0)
				selectJList(BoxType.PLUS, filterPred);
			else if(boxMenu.getSelectedIndex()==1)
				selectJList(BoxType.MINUS, filterPred);
			else if(boxMenu.getSelectedIndex()==2)
				selectJList(BoxType.INTERROGATION, filterPred);
			else if(boxMenu.getSelectedIndex()==3)
				selectJList(BoxType.VALIDATED, filterPred);
			else if(boxMenu.getSelectedIndex()==4)
				selectJList(BoxType.REJECTED, filterPred);

			refresh(this);
		});
		
		GridBagConstraints gbc_boxMenu = new GridBagConstraints();
		gbc_boxMenu.gridheight = 4;
		gbc_boxMenu.insets = new Insets(0, 0, 5, 5);
		gbc_boxMenu.gridx = 0;
		gbc_boxMenu.gridy = 0;
		p.add(boxMenu, gbc_boxMenu);
		
		bottonGroup = new ButtonGroup ();
		
		chckbxShowGeneratedTriples = new JCheckBox("Show Generated Triples ");
		chckbxShowGeneratedTriples.setSelected(true);
		chckbxShowGeneratedTriples.addActionListener(actionListener -> {
			computeFilterPred();
			selectJList(validatorJListManager.getCurrBoxType(), filterPred);
			refresh(this);
		});
		GridBagConstraints gbc_chckbxShowGeneratedTriples = new GridBagConstraints();
		gbc_chckbxShowGeneratedTriples.insets = new Insets(0, 0, 5, 0);
		gbc_chckbxShowGeneratedTriples.gridx = 2;
		gbc_chckbxShowGeneratedTriples.gridy = 0;
		p.add(chckbxShowGeneratedTriples, gbc_chckbxShowGeneratedTriples);
		
		textField = new JTextField();
		
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.insets = new Insets(0, 0, 5, 0);
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 2;
		gbc_textField.gridy = 1;
		p.add(textField, gbc_textField);
		textField.setColumns(10);
		
		btnFilter = new JButton("Filter");
		btnFilter.addActionListener(actionListener -> {
			filterText = textField.getText();
			computeFilterPred();
			selectJList(validatorJListManager.getCurrBoxType(), filterPred);
			refresh(this);
		});
		GridBagConstraints gbc_btnSearch = new GridBagConstraints();
		gbc_btnSearch.anchor = GridBagConstraints.NORTH;
		gbc_btnSearch.insets = new Insets(0, 0, 5, 0);
		gbc_btnSearch.gridx = 2;
		gbc_btnSearch.gridy = 2;
		p.add(btnFilter, gbc_btnSearch);
		
		panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.insets = new Insets(0, 0, 5, 0);
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 2;
		gbc_panel.gridy = 3;
		p.add(panel, gbc_panel);
		panel.setLayout(new GridLayout(0, 1, 0, 0));
		
		radioButtonToPlus = new JRadioButtonMenuItem("Move to Plus Box");
		panel.add(radioButtonToPlus);
		bottonGroup.add(radioButtonToPlus);
		
		radioButtonToInterrogation = new JRadioButtonMenuItem("Move to Interrogation Box");
		panel.add(radioButtonToInterrogation);
		bottonGroup.add(radioButtonToInterrogation);
		
		radioButtonToAcceptation = new JRadioButtonMenuItem("Move to Acceptation Box");
		panel.add(radioButtonToAcceptation);
		bottonGroup.add(radioButtonToAcceptation);
		
		radioButtonToRejection = new JRadioButtonMenuItem("Move to Rejection Box");
		panel.add(radioButtonToRejection);
		bottonGroup.add(radioButtonToRejection);
		
		radioButtonAcceptAllPlus = new JRadioButtonMenuItem("Accept all Plus Box items");
		panel.add(radioButtonAcceptAllPlus);
		bottonGroup.add(radioButtonAcceptAllPlus);
		
	
		esegui= new JButton("Execute");
		panel.add(esegui);
		esegui.addActionListener(this);
		
		
		
		selectedItemInfoTextArea= new JTextArea("");
		selectedItemInfoScrollPane = new JScrollPane(selectedItemInfoTextArea);
		selectedItemInfoScrollPane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		GridBagConstraints gbc_selectedItemInfoScrollPane = new GridBagConstraints();
		gbc_selectedItemInfoScrollPane.gridheight = 3;
		gbc_selectedItemInfoScrollPane.gridwidth = 2;
		gbc_selectedItemInfoScrollPane.fill = GridBagConstraints.BOTH;
		gbc_selectedItemInfoScrollPane.insets = new Insets(0, 0, 5, 5);
		gbc_selectedItemInfoScrollPane.gridx = 0;
		gbc_selectedItemInfoScrollPane.gridy = 4;
		p.add(selectedItemInfoScrollPane, gbc_selectedItemInfoScrollPane);
		
		gbc=new GridBagConstraints();
		gbc.gridx=0;
		gbc.gridy=2;
		gbc.anchor=GridBagConstraints.WEST;
		gbc.insets=new Insets(1,1,1,1);
		
		gbc=new GridBagConstraints();
		gbc.gridx=0;
		gbc.gridy=3;
		gbc.anchor=GridBagConstraints.NORTH;
		gbc.gridwidth=2;
		gbc.insets=new Insets(1,1,1,1);
		
		refresh(this);
			
		return p;
    }
    
    

    
    
    
    private void selectJList(BoxType boxType, Predicate<BoxLocation> filterPred){
    	if(boxJScrollPane!=null)
    		p.remove(boxJScrollPane);
    	JList<BoxLocation> jList = validatorJListManager.selectJlist(boxType, filterPred);
    	
    	if(filterPred==null)
    		boxJScrollPane = boxJScrollPanesMap.get(boxType);
    	else{
    		boxJScrollPane = new JScrollPane(jList);
    		
    	}
    	p.add(boxJScrollPane, gbc_boxJScrollPane);
    }
    
    
    private void execute(){
    	if(radioButtonToPlus.isSelected())
    		validatorJListManager.executeTransition(BoxType.PLUS);
    		
    	else if(radioButtonToInterrogation.isSelected())
    		validatorJListManager.executeTransition(BoxType.INTERROGATION);
    		
    	else if(radioButtonToRejection.isSelected())
    		validatorJListManager.executeTransition(BoxType.REJECTED);
    		
    	else if(radioButtonToAcceptation.isSelected())
    		validatorJListManager.executeTransition(BoxType.VALIDATED);
    		
    	else if(radioButtonAcceptAllPlus.isSelected()){
    		validatorJListManager.putIntoValidatedBoxAllPlusBoxItems();
    		radioButtonAcceptAllPlus.setSelected(false);
    	}
    	
    	radioButtonToPlus.setSelected(false);
    	radioButtonToPlus.setEnabled(false);
    	
    	radioButtonToInterrogation.setSelected(false);
    	radioButtonToInterrogation.setEnabled(false);
    	
    	radioButtonToRejection.setSelected(false);
    	radioButtonToRejection.setEnabled(false);
    	
    	radioButtonToAcceptation.setSelected(false);
    	radioButtonToAcceptation.setEnabled(false);
    	
    	if(filterPred!=null)
    		selectJList(validatorJListManager.getCurrBoxType(), filterPred);
    
    }
    
    
    public void actionPerformed(ActionEvent event){
        String command = event.getActionCommand();
        if(command.equalsIgnoreCase("execute")){
        	notifyRunning();
        	execute();
        	disableRunning();
        	refresh(this);       	
        		
        }
        
    }

    
    
    public void setSelectedItemInfo(String text){
    	selectedItemInfoTextArea.setText(text);
    	refresh(this);
    }
    

    
    
    public void createAndShowGUI(){
	    this.setExtendedState(JFrame.MAXIMIZED_BOTH);
    	Container contentPane = this.getContentPane();
		try{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch(Exception e){
		      e.printStackTrace();
		}
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	
		contentPane.add(createPane(), BorderLayout.CENTER);

		running_label = new JLabel();
		running_label.setVisible(false);
		
		btnPersist = new JButton("Persist");
		btnPersist.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				esegui.setEnabled(false);
	    		notifyRunning();
	    		persistAllvalidatedItems();
	    		GuiManager.executionStarted();
			}
		});
		GridBagConstraints gbc_btnPersist = new GridBagConstraints();
		gbc_btnPersist.gridheight = 2;
		gbc_btnPersist.fill = GridBagConstraints.BOTH;
		gbc_btnPersist.insets = new Insets(0, 0, 5, 0);
		gbc_btnPersist.gridx = 2;
		gbc_btnPersist.gridy = 5;
		p.add(btnPersist, gbc_btnPersist);
		running_label.setText("Running...");
		GridBagConstraints gbc_esecuzioneInCorso_label = new GridBagConstraints();
		gbc_esecuzioneInCorso_label.gridwidth = 2;
		gbc_esecuzioneInCorso_label.insets = new Insets(0, 0, 0, 5);
		gbc_esecuzioneInCorso_label.gridx = 0;
		gbc_esecuzioneInCorso_label.gridy = 7;
		p.add(running_label, gbc_esecuzioneInCorso_label);
		
		this.setSize(1200, 1800);
		this.pack();
		this.setVisible(true);
   }
    
    
    
    JPanel getPanel(){
    	return p;
    }
    
    
    public void enableRadioButtons(Set<BoxType> boxTypes){
    	radioButtonToPlus.setSelected(false);
    	radioButtonToInterrogation.setSelected(false);
    	radioButtonToRejection.setSelected(false);
    	radioButtonToAcceptation.setSelected(false);
    	radioButtonAcceptAllPlus.setSelected(false);
    	
    	if(boxTypes.contains(BoxType.PLUS))
    		radioButtonToPlus.setEnabled(true);
    	else radioButtonToPlus.setEnabled(false);
    	
    	if(boxTypes.contains(BoxType.INTERROGATION))
    		radioButtonToInterrogation.setEnabled(true);
    	else radioButtonToInterrogation.setEnabled(false);
    	
    	if(boxTypes.contains(BoxType.REJECTED))
    		radioButtonToRejection.setEnabled(true);
    	else radioButtonToRejection.setEnabled(false);
    	
    	if(boxTypes.contains(BoxType.VALIDATED))
    		radioButtonToAcceptation.setEnabled(true);
    	else radioButtonToAcceptation.setEnabled(false);
    	
    	
    }
    
    
    private void persistAllvalidatedItems(){
    	List<BoxLocation> validatedBoxItems = validatorJListManager.getBoxLocationListByBoxType(BoxType.VALIDATED);
    	ValidationManager.persistIntoOntology(validatedBoxItems);
    }
    

    private static void refresh(Component component){
    	component.repaint();
    	component.revalidate();
    }
    
    
    private void notifyRunning(){
    	running_label.setVisible(true);
    }
    
    private void disableRunning(){
    	running_label.setVisible(false);
    }
    
    private void computeFilterPred(){
    	if(!chckbxShowGeneratedTriples.isSelected() && StringUtils.isNotBlank(filterText))
			filterPred = boxLocation -> !boxLocation.isTripleInferred() && boxLocation.toString().trim().toLowerCase().contains(filterText.toString().toLowerCase());
		else if(!chckbxShowGeneratedTriples.isSelected())
			filterPred = boxLocation -> !boxLocation.isTripleInferred();
		else if(StringUtils.isNotBlank(filterText)) 
			filterPred = boxLocation -> boxLocation.toString().trim().toLowerCase().contains(filterText.toString().toLowerCase());
		else filterPred = null;
    }
  
  
}
