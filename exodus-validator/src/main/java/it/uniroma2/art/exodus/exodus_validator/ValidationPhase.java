package it.uniroma2.art.exodus.exodus_validator;

public enum ValidationPhase {
	AUTOMATIC_VALIDATION, MANUAL_VALIDATION
}
