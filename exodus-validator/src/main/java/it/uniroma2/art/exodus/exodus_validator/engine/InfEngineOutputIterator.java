package it.uniroma2.art.exodus.exodus_validator.engine;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import it.uniroma2.art.exodus.exodus_validator.structure.Triple;

public class InfEngineOutputIterator<T extends InfRule, B extends Binder<T>> implements Iterator<InfEngineOutput>{

	private Map<B, List<T>> infRulesByBinder;
	private InfEngine<T,B> infEngine;
	
	private Iterator<B> binderIterator;
	private Iterator<T> ruleIterator = null;
	private Iterator<InfEngineOutput> tripleListIterator = null;
	
	private B currBinder;
	private T currRule = null;
	
	private InfEngineOutput nextOutput = null;
	
	public InfEngineOutputIterator(Map<B, List<T>> infRulesByBinder, InfEngine<T,B> infEngine){
		this.infEngine = infEngine;
		this.infRulesByBinder = infRulesByBinder;
		this.binderIterator = infRulesByBinder.keySet().iterator();
	}
	
	
	
	
	private boolean prvHasNext() {
		return binderIterator.hasNext() || (ruleIterator!=null && ruleIterator.hasNext()) || (tripleListIterator!=null && tripleListIterator.hasNext());
	}
	
	
	@Override
	public boolean hasNext() {
		if(nextOutput!=null)
			return true;
		
		if(!prvHasNext())
			return false;
		
		InfEngineOutput candidateNextOutput = null;
			
		while (candidateNextOutput == null){
			if ( (ruleIterator==null || !ruleIterator.hasNext()) && (tripleListIterator==null || !tripleListIterator.hasNext()) ){
				currBinder= binderIterator.next();
				ruleIterator = infRulesByBinder.get(currBinder).iterator();
				currRule = ruleIterator.next();
				tripleListIterator = infEngine.getListOfOutputs(currRule, currBinder).iterator();
			}
				
			else if (tripleListIterator==null || !tripleListIterator.hasNext()){
				currRule = ruleIterator.next();
				tripleListIterator = infEngine.getListOfOutputs(currRule, currBinder).iterator();
			}
				
			if(tripleListIterator.hasNext())
				candidateNextOutput = tripleListIterator.next();
			
			if(candidateNextOutput==null && !prvHasNext())
				return false;
		}
			
		nextOutput = candidateNextOutput;
		
		return true;
		
	}
	
	@Override
	public InfEngineOutput next() {
		InfEngineOutput ret;
		if(nextOutput!=null){
			ret = nextOutput;
			nextOutput = null;
			return ret;
		}
		else if(hasNext()){
			ret = nextOutput;
			nextOutput = null;
			return ret;
		}
		else throw new IllegalArgumentException("Iterator has no next elements");	
	}

}
