package it.uniroma2.art.exodus.exodus_validator.engine;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;

import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;

import it.uniroma2.art.exodus.exodus_validator.PropertiesManager;
import it.uniroma2.art.exodus.exodus_validator.Validator;
import it.uniroma2.art.exodus.exodus_validator.util.FileArrayProvider;

public class InfRulesBuilder{
	private static boolean generateAllRulesForTemplate = PropertiesManager.getInstance().isCreateAllRulesForTemplate();
	
	public static final String PRECONDITION_SPLIT_REGEX = "\\)\\s*:\\s*";
	public static final String IMPLICATION_SPLIT_REGEX = "\\s*=>\\s*";
	public static final String BINDING_TRIPLE_SPLIT_REGEX = "\\)\\s*,\\s*";
	
	public static final String URI_REGEX = "<\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]>";
	public static final String VARIABLE_REGEX = "\\$[A-Za-z]+[A-Za-z0-9_]*";
	
	private static final Pattern URI_PATTERN = Pattern.compile(URI_REGEX);
	private static final Pattern VARIABLE_PATTERN = Pattern.compile(VARIABLE_REGEX);
	
	private static final Predicate<String> CONSISTENCE_RULE_IMPLICATION_CHECK = (implication) -> (implication==null || "".equals(implication.trim()) || "INCONSISTENCE".equalsIgnoreCase(implication.trim()));
	
	private static final Predicate<String> TRIPLE_GENERATION_RULE_IMPLICATION_CHECK = (implication) -> {
		if(implication==null)
			return false;
		String trimmedImplication = implication.trim();
		if(trimmedImplication.equals("") || !trimmedImplication.startsWith("(") || !trimmedImplication.endsWith(")"))
			return false;
		trimmedImplication = trimmedImplication.substring(1, trimmedImplication.length()-1);
		String[] bindingElementsStr = trimmedImplication.split(",");
		if(bindingElementsStr.length < 3)
			return false;
		String subjectElementStr = bindingElementsStr[0].trim();
		String predicateElementStr = bindingElementsStr[1].trim();
		if( (!URI_PATTERN.matcher(subjectElementStr).matches() && !VARIABLE_PATTERN.matcher(subjectElementStr).matches() ) || (!URI_PATTERN.matcher(predicateElementStr).matches() && !VARIABLE_PATTERN.matcher(predicateElementStr).matches()))
			return false;
		return true;
	};
	
	private static final Logger logger = LogManager.getLogger(InfRulesBuilder.class);
	
	public static List<ConsistenceInfRule> buildConsistenceInfRules (String filepath)  throws IOException{
		FileArrayProvider fap = new FileArrayProvider();
        List<String> lines = fap.readLines(filepath);
        List<ConsistenceInfRule> rules = lines.stream().map(InfRulesBuilder::getConsistenceRule).distinct().collect(Collectors.toList());
        if(generateAllRulesForTemplate){
        	List<ConsistenceInfRule> allRules = new ArrayList<>(rules);
        	
        	List<ConsistenceInfRule> generatedRules = rules.stream().map(InfRulesBuilder::generateAllConsistenceInfRules).reduce( (list1, list2) -> {
        		List<ConsistenceInfRule> mergedList = new ArrayList<>(list1);
        		mergedList.addAll(list2);
        		return mergedList;
        	}).orElse(Collections.emptyList());
        	
        	allRules.addAll(generatedRules);
        	
        	return allRules.stream().distinct().collect(Collectors.toList());
        	
        }
        
        else return rules;
	}
	
	
	public static List<TripleGenerationInfRule> buildTripleGenerationInfRules (String filepath)  throws IOException{
		FileArrayProvider fap = new FileArrayProvider();
        List<String> lines = fap.readLines(filepath);
        List<TripleGenerationInfRule> rules = lines.stream().map(InfRulesBuilder::getTripleGenerationRule).distinct().collect(Collectors.toList());
        
        if(generateAllRulesForTemplate){
        	List<TripleGenerationInfRule> allRules = new ArrayList<>(rules);
        	
        	List<TripleGenerationInfRule> generatedRules = rules.stream().map(InfRulesBuilder::generateAllTripleGenInfRules).reduce( (list1, list2) -> {
        		List<TripleGenerationInfRule> mergedList = new ArrayList<>(list1);
        		mergedList.addAll(list2);
        		return mergedList;
        	}).orElse(Collections.emptyList());
        	
        	allRules.addAll(generatedRules);
        	
        	return allRules.stream().distinct().collect(Collectors.toList());
      
        }
        else return rules;
	}
	
	
	public static ConsistenceInfRule getConsistenceRule(String ruleStr){
		String[] preSplit = ruleStr.split(PRECONDITION_SPLIT_REGEX); 
		if(preSplit.length!=2)
			throw new IllegalArgumentException("'" + ruleStr + "' is not a valid string for a consistence rule");
		
		String precondition = preSplit[0] +")";
		
		String[] postSplit = preSplit[1].split(IMPLICATION_SPLIT_REGEX);
		if(postSplit.length>2)
			throw new IllegalArgumentException("'" + ruleStr + "' is not a valid string for a consistence rule");
		
		String bindingTriplesStr = postSplit[0].trim();
		
		if(postSplit.length==2){
			String implication = postSplit[1];
			if(!CONSISTENCE_RULE_IMPLICATION_CHECK.test(implication))   
				throw new IllegalArgumentException("'" + ruleStr + "' is not a valid string for a consistence rule");
		}
		
		
		
		BindingTriple preconditionBindingTriple = null;
		List<BindingTriple> bindingTriples = null;
		
		try{
			preconditionBindingTriple = getBindingTriple(precondition);
			bindingTriples = getBindingTripleList(bindingTriplesStr);
		}
		catch(RuntimeException e){
			throw new IllegalArgumentException("'" + ruleStr + "' is not a valid string for a consistence rule");
		}
		
		ConsistenceInfRule rule = new ConsistenceInfRule(preconditionBindingTriple, bindingTriples);
		
		
		return rule;
	}
	
	
	
	public static List<ConsistenceInfRule> generateAllConsistenceInfRules(ConsistenceInfRule rule){
		BindingTriple precond = rule.getPreconditionBindingTriple();
		Set<BindingTriple> conditions = rule.getTriggingConditions();
		return conditions.stream().map(newRulePrecond -> {
			Set<BindingTriple> newRuleConditions = new TreeSet<>(conditions);
			newRuleConditions.remove(newRulePrecond);
			newRuleConditions.add(precond);
			return new ConsistenceInfRule(newRulePrecond, newRuleConditions);
		}).collect(Collectors.toList());
	}
	
	public static List<TripleGenerationInfRule> generateAllTripleGenInfRules(TripleGenerationInfRule rule){
		BindingTriple precond = rule.getPreconditionBindingTriple();
		BindingTriple inferred = rule.getInferred();
		Set<BindingTriple> conditions = rule.getTriggingConditions();
		return conditions.stream().map(newRulePrecond -> {
			Set<BindingTriple> newRuleConditions = new TreeSet<>(conditions);
			newRuleConditions.remove(newRulePrecond);
			newRuleConditions.add(precond);
			return new TripleGenerationInfRule(newRulePrecond, newRuleConditions, inferred);
		}).collect(Collectors.toList());
	}
	
	
	
	public static TripleGenerationInfRule getTripleGenerationRule(String ruleStr){
		String[] preSplit = ruleStr.split(PRECONDITION_SPLIT_REGEX); 
		if(preSplit.length!=2)
			throw new IllegalArgumentException("'" + ruleStr + "' is not a valid string for a consistence rule");
		
		String precondition = preSplit[0] +")";
		
		String[] postSplit = preSplit[1].split(IMPLICATION_SPLIT_REGEX);
		if(postSplit.length!=2)
			throw new IllegalArgumentException("'" + ruleStr + "' is not a valid string for a consistence rule");
		
		String bindingTriplesStr = postSplit[0].trim();
		String implication = postSplit[1];
		if(!TRIPLE_GENERATION_RULE_IMPLICATION_CHECK.test(implication))   
			throw new IllegalArgumentException("'" + ruleStr + "' is not a valid string for a consistence rule");
		
		BindingTriple preconditionBindingTriple = null;
		List<BindingTriple> bindingTriples = null;
		BindingTriple implicationBindingTriple = null;
		
		try{
			preconditionBindingTriple = getBindingTriple(precondition);
			bindingTriples = getBindingTripleList(bindingTriplesStr);
			implicationBindingTriple = getBindingTriple(implication);
		}
		catch(RuntimeException e){
			throw new IllegalArgumentException("'" + ruleStr + "' is not a valid string for a consistence rule");
		}
		
		TripleGenerationInfRule rule = new TripleGenerationInfRule(preconditionBindingTriple, bindingTriples, implicationBindingTriple);
		
		return rule;
	}
	
	
	
	
	
	
	
	private static List<BindingTriple> getBindingTripleList(String bindingTriplesStr){
		String[] bindingTripleStr =  bindingTriplesStr.trim().split(BINDING_TRIPLE_SPLIT_REGEX);

		List<BindingTriple> ret = new ArrayList<>();
		
		if(!bindingTripleStr[0].trim().equals("")){
			for(int i = 0 ; i < bindingTripleStr.length ; i++){
				String toAdd = bindingTripleStr[i].trim();
				if(i < bindingTripleStr.length -1)
					toAdd = toAdd+")";
				ret.add(getBindingTriple(toAdd));
			}
		}
		
		return ret;
	}
	
	
	
	
	
	
	private static BindingTriple getBindingTriple(String bindingTripleStr){
		String trimmedBindingTripleStr = bindingTripleStr.trim();
		String toSplit = trimmedBindingTripleStr.substring(1,trimmedBindingTripleStr.length()-1);
		String[] bindingElementsStr = toSplit.split(",");
		if(bindingElementsStr.length < 3)
			throw new IllegalArgumentException();
		String subjectElementStr = bindingElementsStr[0];
		String predicateElementStr = bindingElementsStr[1];
		String objectElementStr = bindingElementsStr[2];
		for(int i=3 ; i < bindingElementsStr.length ; i++)
			objectElementStr = objectElementStr + "," + bindingElementsStr[3];

		BindingTripleElement bindingTripleSubject = getBindingTripleElementByString(subjectElementStr);
		BindingTripleElement bindingTriplePredicate = getBindingTripleElementByString(predicateElementStr);
		BindingTripleElement bindingTripleObject = getBindingTripleElementByString(objectElementStr);
		
		return new BindingTriple(bindingTripleSubject, bindingTriplePredicate, bindingTripleObject);
	}
	
	
	
	private static BindingTripleElement getBindingTripleElementByString(String elementStr){
		ValueFactory valueFactory = SimpleValueFactory.getInstance();
		BindingTripleValueFactory btvFactory = BindingTripleValueFactory.getInstance();
		String trimmedElementStr = elementStr.trim(); 
		if(URI_PATTERN.matcher(trimmedElementStr).matches()){
			IRI iri = valueFactory.createIRI(trimmedElementStr.substring(1,trimmedElementStr.length()-1 ));
			return btvFactory.createBindingTripleValue(iri);
		}
		
		else if(VARIABLE_PATTERN.matcher(trimmedElementStr).matches()){
			return new Variable(trimmedElementStr.substring(1));
		}
		
		else return btvFactory.createBindingTripleValue(createLiteral(trimmedElementStr));
			 
	}
	
	
	
	
	private static Literal createLiteral(String literalStr){
		ValueFactory valueFactory = SimpleValueFactory.getInstance();
		try{
			long millis = Date.parse(literalStr);
			Date date = new Date(millis);
			return valueFactory.createLiteral(date);
		}	
		catch(Exception e){
			//va avanti
		}
		
		try{
			int integer = Integer.parseInt(literalStr);
			return valueFactory.createLiteral(integer);
		}
		catch(Exception e){
			//va avanti
		}
		
		try{
			float dec = Float.parseFloat(literalStr);
			return valueFactory.createLiteral(dec);
		}
		catch(Exception e){
			//va avanti
		}
		
		try{
			double dec = Double.parseDouble(literalStr);
			return valueFactory.createLiteral(dec);
		}
		catch(Exception e){
			//va avanti
		}
		
		try{
			long dec = Long.parseLong(literalStr);
			return valueFactory.createLiteral(dec);
		}
		catch(Exception e){
			
		}
		
		try{
			byte byt = Byte.parseByte(literalStr);
			return valueFactory.createLiteral(byt);
		}
		catch(Exception e){
			return valueFactory.createLiteral(literalStr);
		}
		
	}
	
	
	
	public static void main(String[] args){
		System.out.println(getConsistenceRule("($A, <http://www.w3.org/2000/01/rdf-schema#subClassOf>, $B) : (D, <http://www.w3.org/2002/07/owl#equivalentClass>, $A) , ($D, <http://www.w3.org/2002/07/owl#disjointWith>, $B)  => INCONSISTENCE "));
		//System.out.println(getTripleGenerationRule("(A, <http://www.w3.org/2002/07/owl#disjointWith>, $B) : ($A, <http://www.w3.org/2002/07/pippo#lit>, 'nel mezzo del cammin di nostra vita, mi ritrovai per una selva oscura'), ($B , <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://www.w3.org/TR/2004/REC-owl-guide-20040210/wine#Wine>)   => ($A,$B,$C) "));
	}
	
	

}
