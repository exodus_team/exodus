package it.uniroma2.art.exodus.exodus_validator.structure;

public enum BoxType {
	MINUS, REJECTED, INTERROGATION, PLUS, VALIDATED
}
