package it.uniroma2.art.exodus.exodus_validator;

import java.io.IOException;

import java.util.List;

import java.util.stream.Stream;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.ValueFactoryImpl;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.vocabulary.OWL;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.RDFS;

import it.uniroma2.art.exodus.exodus_validator.engine.ConsistenceInfEngine;
import it.uniroma2.art.exodus.exodus_validator.engine.ConsistenceInfEngineBinder;

import it.uniroma2.art.exodus.exodus_validator.engine.ConsistenceInfRule;
import it.uniroma2.art.exodus.exodus_validator.engine.InfEngineOutputIterator;
import it.uniroma2.art.exodus.exodus_validator.engine.InfRulesBuilder;
import it.uniroma2.art.exodus.exodus_validator.engine.TripleGenerationInfEngine;

import it.uniroma2.art.exodus.exodus_validator.engine.TripleGenerationInfRule;

import it.uniroma2.art.exodus.exodus_validator.structure.BoxLocation;
import it.uniroma2.art.exodus.exodus_validator.structure.BoxLocationEvaluatorFactory;
import it.uniroma2.art.exodus.exodus_validator.structure.BoxType;
import it.uniroma2.art.exodus.exodus_validator.structure.Constraint;
import it.uniroma2.art.exodus.exodus_validator.structure.Evaluator;
import it.uniroma2.art.exodus.exodus_validator.structure.Expression;
import it.uniroma2.art.exodus.exodus_validator.structure.MultipleExpression;
import it.uniroma2.art.exodus.exodus_validator.structure.MultipleExpression.Type;
import it.uniroma2.art.exodus.exodus_validator.ui.MainClass;
import it.uniroma2.art.exodus.exodus_validator.structure.SingleExpression;
import it.uniroma2.art.exodus.exodus_validator.structure.Triple;
import it.uniroma2.art.exodus.exodus_validator.structure.ValidationHandler;

public class Validator {
	
	private int maxInferenceLevel;
	private ValidationHandler validationHandler;
	private BFSOntologyClassExplorer ontologyExplorer;
	private BFSOntologyClassExplorer boxesOntologyExplorer;
	private ConsistenceInfEngine consistenceEngine;
	private TripleGenerationInfEngine tripleGenerationEngine;
	private StatementGenerator statementGenerator;
	
	private static final Logger logger = LogManager.getLogger(Validator.class);
	
	private void initConsistenceEngine() throws IOException{
		String rulesFilepath = PropertiesManager.getInstance().getConsistenceInfRulesFilepath();
		List<ConsistenceInfRule> consistenceRules = InfRulesBuilder.buildConsistenceInfRules(rulesFilepath);
		consistenceEngine = new ConsistenceInfEngine(boxesOntologyExplorer, consistenceRules.toArray(new ConsistenceInfRule[consistenceRules.size()]));
	}
	
	
	private void initTripleGenerationEngine() throws IOException{
		String rulesFilepath = PropertiesManager.getInstance().getTripleGenInfRulesFilepath();
		List<TripleGenerationInfRule> tripleGenerationRules = InfRulesBuilder.buildTripleGenerationInfRules(rulesFilepath);
		tripleGenerationEngine = new TripleGenerationInfEngine(boxesOntologyExplorer, tripleGenerationRules.toArray(new TripleGenerationInfRule[tripleGenerationRules.size()]));
	}
	
	
	
	
	public Validator(Model ontologyModel){
		validationHandler = ValidationHandler.getInstance();
		this.maxInferenceLevel = PropertiesManager.getInstance().getMaxInferenceLevel();
		ontologyExplorer = new BFSOntologyClassExplorer(ontologyModel, true);
		boxesOntologyExplorer = new BFSOntologyClassExplorer(ontologyModel, true);
		
		try{
			initConsistenceEngine();
			initTripleGenerationEngine();
		}
		catch(IOException e){
			throw new RuntimeException("Syntax error at consistence and/or triple generation file");
		}
		statementGenerator = new StatementGenerator(tripleGenerationEngine, this);
	}
	
	
	private Validator(BFSOntologyClassExplorer ontologyExplorer, BFSOntologyClassExplorer boxesOntologyExplorer){
		validationHandler = ValidationHandler.getInstance();
		this.maxInferenceLevel = PropertiesManager.getInstance().getMaxInferenceLevel();
		this.ontologyExplorer = ontologyExplorer;
		this.boxesOntologyExplorer = boxesOntologyExplorer;
		try{
			initConsistenceEngine();
			initTripleGenerationEngine();
		}
		catch(IOException e){
			String errorMessage = "Syntax error at consistence and/or triple generation file";
			logger.error(errorMessage, e);
			throw new RuntimeException(errorMessage);
		}
		statementGenerator = new StatementGenerator(tripleGenerationEngine, this);
	}
	
	
	public BFSOntologyClassExplorer getBoxesOntologyExplorer(){
		return boxesOntologyExplorer;
	}
	
	
	
	public void validate(List<Model> graphModels){
		graphModels.forEach(this::validate);
		
		int consistenceQueries = OntologyExplorer.getQueryCounterByRuleClass(ConsistenceInfRule.class);
		logger.info("# SPARQL queries for consistence checking: " + consistenceQueries);
		
		int tripleGenerationQueries = OntologyExplorer.getQueryCounterByRuleClass(TripleGenerationInfRule.class);
		logger.info("# SPARQL queries for triple generation: " + tripleGenerationQueries);
		
	}
	
	
	public void validate(Model graphModel){
		Stream<Statement> allStatements = graphModel.stream();
		allStatements.forEach(this::validateStatement);
	}
	
	
	
	
	public void validateStatement(Statement statement){
		Resource subject = statement.getSubject();
		IRI predicate = statement.getPredicate();
		Value object = statement.getObject();
		Triple triple = new Triple(subject, predicate, object);
		BoxLocation boxLocation = new BoxLocation(triple);
		validateStatement(boxLocation, 0);
	}


	public BoxType validateStatement(BoxLocation boxLocation, int inferenceLevel) {
		logger.debug("Processing triple: " + boxLocation);
		
		ConstraintHandler constraintHandler = ConstraintHandler.getInstance();
		
		Triple triple = boxLocation.getTriple();
		
		if(boxesOntologyExplorer.checkTriple(triple.getSubject(), triple.getPredicate(), triple.getObject()) ){
			if(!boxLocation.isTripleInferred()){
				logger.debug("Not inferred triple yet into the ontology or into IN +/? BOX: " + boxLocation.getCompleteInfo());
				return null;
			}
			
		}
			
		
		
		MultipleExpression<BoxLocation> orExpression = new MultipleExpression<>(Type.OR);
		
		InfEngineOutputIterator<ConsistenceInfRule, ConsistenceInfEngineBinder> triplesInANDiterator = consistenceEngine.fire(triple);
		
		
		while(triplesInANDiterator.hasNext()){
			List<Triple> triplesInAND = triplesInANDiterator.next().getTriplesInAND();
			MultipleExpression<BoxLocation> andExpression = new MultipleExpression<>(Type.AND);
			triplesInAND.forEach(tripleInAND -> {
				BoxLocation boxLoc = validationHandler.getPlusOrInterrogationBoxLocation(tripleInAND);
				if(boxLoc!=null)
					andExpression.addSubExpression(new SingleExpression<>(boxLoc));
			});
			
			if(andExpression.isEmptyExpression()){
				validationHandler.addToMinusBox(boxLocation);
				if(!boxLocation.isTripleInferred()){
					return BoxType.MINUS;
				}
				else {
					//createCCforInferredInconsistentStmt(triple)
					constraintHandler.setConflictConstraintsForInferredLS(boxLocation, null);
				}
					
			}
			
			orExpression.addSubExpression(andExpression);
			
		}
		
		
		if(orExpression.isEmptyExpression()){
			if(!boxLocation.isTripleInferred()){
				validationHandler.addToPlusBox(boxLocation);
				logger.debug("Triple asserted without conflicts: " + boxLocation.getCompleteInfo());
			}
				
			else {
				Constraint depConstraint = boxLocation.getDependencyConstraint();
				Expression<BoxLocation> depConstrExpr = depConstraint.getTarget();
				Evaluator<BoxLocation> evaluator = BoxLocationEvaluatorFactory.getDefaultEvaluator();
				if(depConstrExpr.evaluate(evaluator)){
					validationHandler.addToPlusBox(boxLocation);
					logger.debug("Triple inferred without conflicts (DEP CONSTR EXPR=TRUE): " + boxLocation.getCompleteInfo());
				}
				else {
					validationHandler.addToInterrogationBox(boxLocation);
					logger.debug("Triple inferred without conflicts (DEP CONSTR EXPR=FALSE): " + boxLocation.getCompleteInfo());
				}
			}
			boxesOntologyExplorer.addTriple(triple);
		}
		
		else {
			Expression<BoxLocation> normalizedOrExpression = constraintHandler.normalizeExpression(orExpression);
			
			if(!boxLocation.isTripleInferred()){
				constraintHandler.setConflictConstraint(boxLocation, normalizedOrExpression);
				validationHandler.addToInterrogationBox(boxLocation);
				logger.debug("Triple asserted with conflicts: " + boxLocation.getCompleteInfo());
				boxesOntologyExplorer.addTriple(triple);
			}	
			
			else {
				//FARE TUTTO IL PERCORSO DI NORMALIZZAZIONE ANCHE DELLA BOX LOCATION LS
				//TRASFORMAZIONE IN DNF, ETC. DA CHIAMARE TUTTO IN UN UNICO METODO
				//DENTRO IL CONSTRAINT HANDLER
				constraintHandler.setConflictConstraintsForInferredLS(boxLocation, normalizedOrExpression);
				validationHandler.addToMinusBox(boxLocation);
				logger.debug("Triple inferred with conflicts: " + boxLocation.getCompleteInfo());
				return BoxType.MINUS;
			}
		}
		
		
		
		if(maxInferenceLevel <0 || inferenceLevel < maxInferenceLevel){
			List<BoxLocation> generatedBoxLocations = statementGenerator.generateStatements(boxLocation, inferenceLevel);    
        }

		return boxLocation.getBoxType();
		
	}
	
	
	
	public static void main(String[] args){
		ModelBuilder modelBuilder = new ModelBuilder();
		Model ontologyModel= modelBuilder.build();
		ValueFactory valueFactory = new ValueFactoryImpl();
		ontologyModel.add(valueFactory.createIRI("http://dummyPrefix/rome"), RDF.TYPE, valueFactory.createIRI("http://dummyPrefix/CaputMundi"));
		ontologyModel.add(valueFactory.createIRI("http://dummyPrefix/CaputMundi"), OWL.DISJOINTWITH, valueFactory.createIRI("http://dummyPrefix/Area"));
		Validator validator = new Validator(ontologyModel);
		
		
		validator.validateStatement(valueFactory.createStatement(valueFactory.createIRI("http://dummyPrefix/City"), RDFS.SUBCLASSOF, valueFactory.createIRI("http://dummyPrefix/AdministrativeArea")));
		
		validator.validateStatement(valueFactory.createStatement(valueFactory.createIRI("http://dummyPrefix/City"), RDFS.SUBCLASSOF, valueFactory.createIRI("http://dummyPrefix/Region")));
		
		
		validator.validateStatement(valueFactory.createStatement(valueFactory.createIRI("http://dummyPrefix/AdministrativeArea"), RDFS.SUBCLASSOF, valueFactory.createIRI("http://dummyPrefix/Area")));
		
		validator.validateStatement(valueFactory.createStatement(valueFactory.createIRI("http://dummyPrefix/Region"), RDFS.SUBCLASSOF, valueFactory.createIRI("http://dummyPrefix/Area")));
		
		validator.validateStatement(valueFactory.createStatement(valueFactory.createIRI("http://dummyPrefix/rome"), RDF.TYPE, valueFactory.createIRI("http://dummyPrefix/City")));
		
		
		System.out.println("PLUS:");
		ValidationHandler.getInstance().getBoxByType(BoxType.PLUS).getBoxLocationsList().forEach(boxLoc -> System.out.println(boxLoc.getTriple()));
		System.out.println("\nINTERROGATION:");
		ValidationHandler.getInstance().getBoxByType(BoxType.INTERROGATION).getBoxLocationsList().forEach(boxLoc -> System.out.println(boxLoc.getTriple()+ boxLoc.getConflictConstraint().toString()));
		System.out.println("\nMINUS:");
		ValidationHandler.getInstance().getBoxByType(BoxType.MINUS).getBoxLocationsList().forEach(boxLoc -> System.out.println(boxLoc.getTriple()));
		
	}
	
	
	

}
