package it.uniroma2.art.exodus.exodus_validator.engine;

import java.util.List;

import it.uniroma2.art.exodus.exodus_validator.structure.Triple;

public class TripleGenerationInfEngineOutput extends InfEngineOutput {
	
	private Triple generatedTriple;

	public TripleGenerationInfEngineOutput(List<Triple> triplesInAND, Triple generatedTriple) {
		super(triplesInAND);
		this.generatedTriple = generatedTriple;
	}

	public Triple getGeneratedTriple() {
		return generatedTriple;
	}
	
	public String toString(){
		return super.toString() + " -- generatedTriple: "+generatedTriple;
	}
	
	
	
	

}
