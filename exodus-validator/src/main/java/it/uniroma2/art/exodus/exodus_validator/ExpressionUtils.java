package it.uniroma2.art.exodus.exodus_validator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import it.uniroma2.art.exodus.exodus_validator.structure.Expression;
import it.uniroma2.art.exodus.exodus_validator.structure.MultipleExpression;
import it.uniroma2.art.exodus.exodus_validator.structure.MultipleExpression.Type;
import it.uniroma2.art.exodus.exodus_validator.structure.SingleExpression;

public class ExpressionUtils {
	
	public static <T> Expression<T> replaceAll(Expression<T> origin, T toReplace, Expression<T> replacement){
		Expression<T> ret;
		if(origin instanceof SingleExpression){
			SingleExpression<T> originSingleExpression = (SingleExpression<T>) origin;
			if(originSingleExpression.getItem().equals(toReplace))
				ret = replacement;
			else ret = origin;
		}
		else if(origin instanceof MultipleExpression){
			MultipleExpression<T> originMultipleExpression = (MultipleExpression<T>) origin;
			Type multipleExpressionType =originMultipleExpression.getType();
			Set<Expression<T>> subExpressions = originMultipleExpression.getSubExpressions();
			List<Expression<T>> newSubExpressions = subExpressions.stream().map(subExpr -> replaceAll(subExpr, toReplace, replacement)).collect(Collectors.toList());
			ret = new MultipleExpression<>(newSubExpressions, multipleExpressionType);
		}
		else throw new IllegalArgumentException("origin expression "+origin+ " not handled");
		
		return ret;
	}
	
	
	
	
	public static <T> boolean isComposedTypeExpression(Expression<T> expression, Type type){
		boolean ret = false;
		if(expression.equals(Expression.TRUE) || expression.equals(Expression.FALSE) )
			ret = true;
		else if (expression instanceof SingleExpression)
			ret = true;
		else if (expression instanceof MultipleExpression){
			MultipleExpression<T> multipleExpression = (MultipleExpression<T>) expression; 
			if(multipleExpression.getType().equals(type))
				ret = multipleExpression.getSubExpressions().stream().allMatch(subExpr -> isComposedTypeExpression(subExpr,type));
		}
		return ret;
	}
	
	
	public static <T> boolean isDNFexpression(Expression<T> expression){
		boolean ret = false;
		if(isComposedTypeExpression(expression, Type.AND))
			ret = true;
		else if (expression instanceof MultipleExpression){
			MultipleExpression<T> multipleExpression = (MultipleExpression<T>) expression; 
			if(multipleExpression.getType().equals(Type.OR))
				ret = multipleExpression.getSubExpressions().stream().allMatch(ExpressionUtils::isDNFexpression);
		}
		return ret;
	}
	
	
	public static <T> boolean isCNFexpression(Expression<T> expression){
		boolean ret = false;
		if(isComposedTypeExpression(expression, Type.OR))
			ret = true;
		else if (expression instanceof MultipleExpression){
			MultipleExpression<T> multipleExpression = (MultipleExpression<T>) expression; 
			if(multipleExpression.getType().equals(Type.AND))
				ret = multipleExpression.getSubExpressions().stream().allMatch(ExpressionUtils::isCNFexpression);
		}
		return ret;
	}
	
	
	public static <T> Expression<T> getRefinedExpression(Expression<T> expression){
		if(expression.equals(Expression.TRUE) || expression.equals(Expression.FALSE) || (expression instanceof SingleExpression))
			return expression;
		else if(expression instanceof MultipleExpression){
			MultipleExpression<T> multipleExpression = (MultipleExpression<T>) expression;
			Type type = multipleExpression.getType(); 
			Set<Expression<T>> subExpressions = multipleExpression.getSubExpressions();
			if(subExpressions.isEmpty()){
				if(type.equals(Type.AND))
					return Expression.TRUE;
				else if(type.equals(Type.OR))
					return Expression.FALSE;
				else throw new IllegalStateException ("Type "+ type + " not handled");
			}
			else if(subExpressions.size() == 1)
				return getRefinedExpression(subExpressions.iterator().next());
			else {
				boolean refined = false;
				List<Expression<T>> refinedSubExpressions = new ArrayList<>();
				for(Expression<T> subExpr : subExpressions){
					Expression<T> refinedSubExpr = getRefinedExpression(subExpr);
					if(refinedSubExpr.equals(Expression.TRUE)){
						if(type.equals(Type.OR))
							return Expression.TRUE;
						else {
							//non considero l'elemento corrente
							refined = true;
						}
					}
						
					else if (refinedSubExpr.equals(Expression.FALSE)) {
						if(type.equals(Type.AND))
							return expression.FALSE;
						else {
							//non considero l'elemento corrente
							refined = true;
						}
					}
						
					else if(refinedSubExpr instanceof MultipleExpression){
						MultipleExpression<T> multipleRefinedSubExpr = (MultipleExpression<T>) refinedSubExpr;
						if(multipleRefinedSubExpr.getType().equals(type)){
							multipleRefinedSubExpr.getSubExpressions().forEach(refinedSubExpressions::add);
							refined = true;
						}
							
						else {
							refinedSubExpressions.add(refinedSubExpr);
							refined = true;
						}
					}
					else {
						refinedSubExpressions.add(refinedSubExpr);
						refined = true;
					}
				}
				
				if(!refined)
					return expression;
				else if(refinedSubExpressions.isEmpty()){
					if(type.equals(Type.AND))
						return Expression.TRUE;
					else if(type.equals(Type.OR))
						return Expression.FALSE;
					else throw new IllegalStateException ("Type "+ type + " not handled");
				}
				else if(refinedSubExpressions.size()==1)
					return refinedSubExpressions.get(0);
				
				else return new MultipleExpression<>(refinedSubExpressions, type);
					 
			}
			
		}
		
		else throw new IllegalArgumentException("Expression class type not handled");
	}
	
	
	
	
	public static <T> Expression<T> toDNF(Expression<T> expression){
		Expression<T> refinedExpr = getRefinedExpression(expression);
		if(isSimpleExpression(refinedExpr))
			return refinedExpr;
		else if(refinedExpr instanceof MultipleExpression){
			MultipleExpression<T> multipleExpression = (MultipleExpression<T>) refinedExpr;
			Type type = multipleExpression.getType();
			if(type.equals(Type.AND)){
				return refinedAndExpressionToDNF(multipleExpression);
			}
			
			else if(type.equals(Type.OR)){
				Set<Expression<T>> subExpressions = multipleExpression.getSubExpressions();
				MultipleExpression<T> orExpression = new MultipleExpression<>(Type.OR);
				subExpressions.forEach(subExpr -> {
					if(subExpr.equals(Expression.TRUE) || subExpr.equals(Expression.FALSE) )
						throw new IllegalStateException("Bug nel refine");
					else if(subExpr instanceof SingleExpression)
						orExpression.addSubExpression(subExpr);
					else if(subExpr instanceof MultipleExpression){
						MultipleExpression<T> multipleSubExpr = (MultipleExpression<T>) subExpr;
						if(multipleSubExpr.getType().equals(Type.AND)){
							MultipleExpression<T> dnfSubExpr = refinedAndExpressionToDNF(multipleSubExpr);
							orExpression.addSubExpressions(dnfSubExpr.getSubExpressions());
						}
						else throw new IllegalStateException("Bug nel refine");	
					}
					else throw new IllegalStateException("Expression class type not handled");
				});
				
				return orExpression;
			}
			
			else throw new IllegalStateException ("Bug nel refine - casistica non gestita");		
		}
		else throw new IllegalStateException ("Bug nel refine - casistica non gestita");
	}
	
	
	private static <T> MultipleExpression<T> refinedAndExpressionToDNF(MultipleExpression<T> multipleAndExpression){
		List<List<T>> orOfAndList = refinedAndExpressionToOrOfAndList(multipleAndExpression);
		List<Expression<T>> andExpressions = new ArrayList<>();
		orOfAndList.forEach(andList -> {
			List<Expression<T>> singleExpressions = andList.stream().map(item -> new SingleExpression<>(item)).collect(Collectors.toList());
			MultipleExpression<T> andExpression = new MultipleExpression<>(singleExpressions, Type.AND);
			andExpressions.add(andExpression);
		});
		MultipleExpression<T> orExpression = new MultipleExpression<>(andExpressions, Type.OR);
		return orExpression;
	}
	
	
	
	//la refined expression mi garantisce l'alternanza tra OR ed AND
	//questo metodo mi ritorna un OR di AND sotto forma di liste
	private static <T> List<List<T>> refinedAndExpressionToOrOfAndList(MultipleExpression<T> multipleExpression){
			Type type = multipleExpression.getType();
			if(type.equals(Type.AND)){
				List<List<T>> ret = new ArrayList<>();
				List<List<List<T>>> andOfOrOfAndList = new ArrayList<>();
				Set<Expression<T>> subExpressions = multipleExpression.getSubExpressions();
				subExpressions.stream().forEach(subExpr -> {
					if(subExpr instanceof SingleExpression){
						List<List<T>> singletonListOfList = Collections.singletonList(Collections.singletonList(((SingleExpression<T>)subExpr).getItem()));
						andOfOrOfAndList.add(singletonListOfList);
					}
						
					else if (subExpr instanceof MultipleExpression){
						MultipleExpression<T> multipleSubExpr = (MultipleExpression<T>) subExpr;
						if(multipleSubExpr.getType().equals(Type.OR)) {//per forza così data l'alternanza di AND ed OR
							List<List<T>> mergedOrOfAndList = new ArrayList<>();
							Set<Expression<T>> subSubExpressions = multipleSubExpr.getSubExpressions();
							subSubExpressions.forEach(subSubExpr -> {
								if(subSubExpr instanceof SingleExpression){
									List<T> singletonList = Collections.singletonList(((SingleExpression<T>)subSubExpr).getItem());
									mergedOrOfAndList.add(singletonList);
								}
								else if(subSubExpr instanceof MultipleExpression){
									MultipleExpression<T> multipleSubSubExpr = (MultipleExpression<T>) subSubExpr;
									if(multipleSubSubExpr.getType().equals(Type.AND)){
										List<List<T>> subSubExprOrOfAndList = refinedAndExpressionToOrOfAndList(multipleSubSubExpr);
										mergedOrOfAndList.addAll(subSubExprOrOfAndList);
									}
									else throw new IllegalStateException("bug nel refine");
								}
								else throw new IllegalStateException("Type of Expression class not handled");
							});
							
							andOfOrOfAndList.add(mergedOrOfAndList);
						}
						else throw new IllegalStateException("bug nel refine");
					}
					else throw new IllegalStateException("Type of Expression class not handled");
				});
				
				
				List<List<List<T>>> cartProduct = cartesianProduct(andOfOrOfAndList);
				
				cartProduct.forEach(listOfLists -> {
					List<T> andList = new ArrayList<>();
					listOfLists.forEach(andList::addAll);
					ret.add(andList);
				});
				
				return ret;
				
			}
			
			else throw new IllegalArgumentException(multipleExpression + " is not a MultipleExpression with type = AND");
		
	}
	
	
	private static <T> boolean isSimpleExpression(Expression<T> expression){
		return expression.equals(Expression.TRUE) || expression.equals(Expression.FALSE) || (expression instanceof SingleExpression);
	}
	
	
	
	
	private static <T> List<List<T>> cartesianProduct(List<List<T>> lists) {
	    List<List<T>> resultLists = new ArrayList<>();
	    if (lists.isEmpty()) {
	        resultLists.add(new ArrayList<T>());
	        return resultLists;
	    } else {
	        List<T> firstList = lists.get(0);
	        List<List<T>> remainingLists = cartesianProduct(lists.subList(1, lists.size()));
	        for (T condition : firstList) {
	            for (List<T> remainingList : remainingLists) {
	                ArrayList<T> resultList = new ArrayList<>();
	                resultList.add(condition);
	                resultList.addAll(remainingList);
	                resultLists.add(resultList);
	            }
	        }
	    }
	    return resultLists;
	}
	
	
	
	
	public static void main(String[] args){
		Expression<String> a1 = new SingleExpression<>("a1");
		Expression<String> a2 = new SingleExpression<>("a2");
		
		Expression<String> b1 = new SingleExpression<>("b1");
		Expression<String> b2 = new SingleExpression<>("b2");
		Expression<String> b3 = new SingleExpression<>("b3");
		
		Expression<String> b4 = new SingleExpression<>("b4");
		
		Expression<String> c1 = new SingleExpression<>("c1");
		Expression<String> c2 = new SingleExpression<>("c2");
		
		
		List<Expression<String>> expressions = new ArrayList<>();
		expressions.add(a1);
		expressions.add(a2);
		
		Expression<String> expr = new MultipleExpression<>(Type.AND, new MultipleExpression<String>(Type.OR, b3, b4), a1, new MultipleExpression<String>(Type.OR, b1, b2, new MultipleExpression<String>(Type.AND, new MultipleExpression<String>(Type.AND, a2, b3), new MultipleExpression<String>(Type.OR, c1, c2))));
		
		//System.out.println(new MultipleExpression<String>(Type.AND, new MultipleExpression<String>(Type.AND, a2, b3), new MultipleExpression<String>(Type.OR, c1, c2)));
		//System.out.println(getRefinedExpression(new MultipleExpression<String>(Type.AND, a1, a2, new MultipleExpression<String>(Type.AND, b1, b2))));
		//System.out.println(toDNF(expr));
		
	}
	
	
	


}
