package it.uniroma2.art.exodus.exodus_validator.engine;

import org.eclipse.rdf4j.model.Value;

public class Variable extends BindingTripleElement{

	private String name;
	
	public Variable(String name){
		this.name = name;
	}
	
	@Override
	public boolean equals(BindingTripleElement other) {
		boolean ret = false;
		if(other instanceof Variable){
			Variable otherVariable = (Variable) other;
			if(this.name.equals(otherVariable.name))
				ret = true;
		}
		return ret;
	}

	@Override
	public boolean bind(Value rdf4jValue) {
		return true;
	}
	
	public String getName(){
		return this.name;
	}

	@Override
	public String toString() {
		return "?"+this.name;
	}

	
	public int hashCode(){
		return 16 * toString().hashCode();
	}
}
