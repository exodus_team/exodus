package it.uniroma2.art.exodus.exodus_validator.ui;

import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;



public class GuiManager {

	private static JFrame ui = null;
	private static boolean executionStarted = false;
	
	public static JFrame getUi() {
		return ui;
	}

	public static void setUi(JFrame ui) {
		GuiManager.ui = ui;
	}
	
	
	public static void makeUIinvisible(){
		ui.setVisible(false);
	}
	
	
	public static void closeUI(){
		ui.dispatchEvent(new WindowEvent(ui, WindowEvent.WINDOW_CLOSING));
	}
	
	public static void executionStarted(){
		executionStarted = true;
	}
	
	public static void reset(){
		executionStarted = false;
	}
	
	public static boolean isExecutionStarted(){
		return executionStarted;
	}
	
	

}