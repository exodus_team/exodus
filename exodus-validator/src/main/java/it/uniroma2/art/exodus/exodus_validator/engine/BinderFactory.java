package it.uniroma2.art.exodus.exodus_validator.engine;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import it.uniroma2.art.exodus.exodus_validator.structure.Triple;

public class BinderFactory<B extends Binder> {
	
	private Class<B> binderClazz;
	private Constructor<B> constructor;
	
	public BinderFactory(Class<B> binderClazz){
		try {
			this.binderClazz = binderClazz;
			constructor = binderClazz.getConstructor(Triple.class, BindingTriple.class);
		} catch (NoSuchMethodException | SecurityException e) {
			throw new IllegalStateException("No available constructor for " + binderClazz + " with Triple and BindingTriple objects", e);
		}
	}
	
	public B createBinder(Triple triple, BindingTriple precondition){
		try {
			return constructor.newInstance(triple, precondition);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			throw new IllegalStateException("Cannot instantiate class " + binderClazz + " with Triple and BindingTriple objects", e);
		}
	}

}
