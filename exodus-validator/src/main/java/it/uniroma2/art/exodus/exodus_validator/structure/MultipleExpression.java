package it.uniroma2.art.exodus.exodus_validator.structure;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;



public class MultipleExpression<T> extends Expression<T> {

	public enum Type{
		AND, OR
	}
	
	private Type type;
	private Set<Expression<T>> subExpressions;
	
	private Set<T> allItems = null;
	
	public MultipleExpression(Type type){
		this.type = type;
		subExpressions = new HashSet<>();
	}
	
	public MultipleExpression(Collection<Expression<T>> expressions, Type type){
		this.type = type;
		this.subExpressions = new HashSet<>(expressions);
	}
	
	@SafeVarargs
	public MultipleExpression(Type type, Expression<T>... expressions){
		this.type = type;
		this.subExpressions = new HashSet<>(Arrays.asList(expressions));
	}
	
	public Set<Expression<T>> getSubExpressions() {
		return subExpressions;
	}

	public Type getType() {
		return type;
	}
	
	public void addSubExpression(Expression<T> expression){
		subExpressions.add(expression);
		if(allItems!=null)
			allItems.addAll(expression.getAllItems());
	}
	
	public void addSubExpressions(Collection<Expression<T>> expressions){
		subExpressions.addAll(expressions);
		if(allItems!=null)
			allItems.addAll(expressions.stream().map(expr -> new ArrayList<T>(expr.getAllItems())).reduce( (list1,list2) -> {
				ArrayList<T> merged = new ArrayList<>(list1);
				merged.addAll(list2);
				return merged;
				}).orElse(new ArrayList<>())
			);
	}
	
	
	@Override
	public boolean evaluate(Evaluator<T> evaluator) {
		if(type.equals(Type.AND))
			return evaluateAND(evaluator);
		else if(type.equals(Type.OR))
			return evaluateOR(evaluator);
		else throw new IllegalStateException("Type "+ type + " not handled");
	}

	private boolean evaluateOR(Evaluator<T> evaluator) {
		return subExpressions.stream().anyMatch(subExpr -> subExpr.evaluate(evaluator));
	}

	private boolean evaluateAND(Evaluator<T> evaluator) {
		return subExpressions.stream().allMatch(subExpr -> subExpr.evaluate(evaluator));
	}
	
	
	@Override
	public String toString(){
		String ret = "( " + String.join(" " + type + " ", subExpressions.stream().map(expr -> expr.toString()).collect(Collectors.toList())) + " )";
		return ret;
	}

	@Override
	public boolean equals(Object other) {
		boolean ret = false;
		if(other instanceof MultipleExpression){
			MultipleExpression<T> otherMultipleExpression = (MultipleExpression<T>) other;
			ret = 	this.type.equals(otherMultipleExpression.type) &&
					this.subExpressions.equals(otherMultipleExpression.subExpressions);
		}
		return ret;
	}
	
	public boolean isEmptyExpression(){
		return subExpressions.isEmpty();
	}

	@Override
	public Set<T> getAllItems() {
		if(allItems==null){
			allItems = new HashSet<>();
			subExpressions.forEach(expr -> this.allItems.addAll(expr.getAllItems()));
		}
		return allItems;
	}


}
