package it.uniroma2.art.exodus.exodus_validator.engine;

import java.util.List;
import java.util.Map;

public class TripleGenerationInfEngineOutputIterator extends InfEngineOutputIterator<TripleGenerationInfRule, TripleGenerationInfEngineBinder>{

	public TripleGenerationInfEngineOutputIterator(
			Map<TripleGenerationInfEngineBinder, List<TripleGenerationInfRule>> infRulesByBinder,
			InfEngine<TripleGenerationInfRule, TripleGenerationInfEngineBinder> infEngine) {
		super(infRulesByBinder, infEngine);
		
	}
	
	@Override
	public TripleGenerationInfEngineOutput next() {
		return (TripleGenerationInfEngineOutput) super.next();
	}

}
