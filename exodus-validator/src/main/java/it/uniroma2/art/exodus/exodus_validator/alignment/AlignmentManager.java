package it.uniroma2.art.exodus.exodus_validator.alignment;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;

import it.uniroma2.art.exodus.exodus_validator.BFSOntologyClassExplorer;
import it.uniroma2.art.exodus.exodus_validator.LabeledResource;

public class AlignmentManager {
	private Double[][] classMatchingScore;
	//private Double[][] objectPropertyMatchingScore;
	//private Double[][] datatypePropertyMatchingScore;
	private Double[][] individualMatchingScore;
	
	private Map<String,Integer> ontologyClassMap = new HashMap<String,Integer>();
	//private Map<String,Integer> ontologyObjectPropertyMap = new HashMap<String,Integer>();
	//private Map<String,Integer> ontologyDatatypePropertyMap = new HashMap<String,Integer>();
	private Map<String,Integer> ontologyIndividualMap = new HashMap<String,Integer>();
	private Map<String,Integer> graphClassMap = new HashMap<String,Integer>();
	//private Map<String,Integer> graphObjectPropertyMap = new HashMap<String,Integer>();
	//private Map<String,Integer> graphDatatypePropertyMap = new HashMap<String,Integer>();
	private Map<String,Integer> graphIndividualMap = new HashMap<String,Integer>();
	
	private Model ontologyModel;
	private Model graphModel;
	
	private BFSOntologyClassExplorer ontologyClassExplorer;
	private BFSOntologyClassExplorer graphClassExplorer;
	
	
	public AlignmentManager (Model ontologyModel, Model graphModel){
		this.ontologyModel = ontologyModel;
		this.graphModel = ontologyModel;
		
		ontologyClassExplorer = new BFSOntologyClassExplorer(ontologyModel, true);
		graphClassExplorer = new BFSOntologyClassExplorer(graphModel, false);
		
		Set<Resource> allUriOntClasses = ontologyClassExplorer.getAllUriClasses();
		fillMap(ontologyClassMap, allUriOntClasses);
		
		Set<Resource> allUriGraphClasses = graphClassExplorer.getAllUriClasses();
		fillMap(graphClassMap, allUriGraphClasses);
		
		classMatchingScore = new Double[allUriOntClasses.size()][allUriGraphClasses.size()];
		
		while(graphClassExplorer.hasOtherItems()){
			Set<LabeledResource<Resource>> equivalentGraphLabeledClasses = graphClassExplorer.visit();
			
			Set<Resource> intermodelEquivalentClasses = new HashSet<Resource>();
			for(LabeledResource<Resource> currGraphLabeledClass : equivalentGraphLabeledClasses){
				Collection<Resource> currIntermodelEquivalentClasses = ontologyClassExplorer.getEquivalentClasses(currGraphLabeledClass.getResource());
				
				
				intermodelEquivalentClasses.addAll(currIntermodelEquivalentClasses);
			}
			
			
			
			while(ontologyClassExplorer.hasOtherItems()){
				Set<LabeledResource<Resource>> equivalentOntLabeledClasses = ontologyClassExplorer.visit();
				double score = computeMatchingScore(equivalentOntLabeledClasses, equivalentGraphLabeledClasses);
					
				/*if(score>0){
					Resource ontClass = null;
					Resource graphClass = null;
					for(LabeledResource<Resource> eqOntLabeledClass : equivalentOntLabeledClasses){
						ontClass = eqOntLabeledClass.getResource();
						break;
					}
					
					for(LabeledResource<Resource> eqGraphLabeledClass : equivalentGraphLabeledClasses){
						graphClass = eqGraphLabeledClass.getResource();
						break;
					}
					
					Set<Resource> disjointOntClasses = ontologyClassExplorer.getDisjointClassesWithoutConsideringSubclasses(ontClass);
					Set<Resource> equivalentGraphClasses = graphClassExplorer.getEquivalentClasses(graphClass);
					
					boolean classConsistence = Collections.disjoint(disjointOntClasses, equivalentGraphClasses);
					
					
				}*/
			}
		
		}
	}
	
	
	
	private double computeMatchingScore(Set<LabeledResource<Resource>> equivalentOntLabeledClasses, Set<LabeledResource<Resource>> equivalentGraphLabeledClasses) {
		List<Integer> ontIndexList = new ArrayList<Integer>();
		List<Integer> graphIndexList = new ArrayList<Integer>();
		
		double maxScore = 0;
		for(LabeledResource<Resource> equivalentOntLabeledClass : equivalentOntLabeledClasses){
			ontIndexList.add(ontologyClassMap.get(equivalentOntLabeledClass.getResource().stringValue()));
			for(LabeledResource<Resource> equivalentGraphLabeledClass : equivalentGraphLabeledClasses){
				graphIndexList.add(graphClassMap.get(equivalentGraphLabeledClass.getResource().stringValue()));
				if(maxScore<1)
					maxScore = Double.max(maxScore, computeSingleMatchingScore(equivalentOntLabeledClass,equivalentGraphLabeledClass));
			}
		}
		
		for(int ontIndex : ontIndexList)
			for(int graphIndex : graphIndexList)
				classMatchingScore[ontIndex][graphIndex] = maxScore;
		
		return maxScore;
			
	}


//renderlo compilabile, metterci wordnet
	private double computeSingleMatchingScore(LabeledResource<Resource> ontLabeledClass, LabeledResource<Resource> graphLabeledClass) {
		/*
		 * double score;
	String gClassURI = ontLabeledClass.getResource().stringValue();
	String oClassURI = graphLabeledClass.getResource().stringValue();
	if (gClassURI.equals(oClassURI)) 
		score = 1;
	
	else {
		String oClassNormalizedURI = normalize(oClassURI);
		String gClassNormalizedURI = normalize(gClassURI);
		if(oClassNormalizedURI.equals(gClassNormalizedURI))
			score = 0.999;
			
		//search in Wordnet
		else{
			String gCompareWord;
			String oCompareWord;
			if(oClassNormalizedURI exists in Wordnet)
				oCompareWord = oClassNormalizedURI;
			else{
				List<String> oClassLabels = ontLabeledClass.getLabels();
				oCompareWord = the first label in oClassLabels existing in Wordnet;
			}
			
			if(oCompareWord != null){
				if(gClassNormalizedURI exists in Wordnet)
					gCompareWord = gClassNormalizedURI;
				else{
					List<String> gClassLabels = graphLabeledClass.getLabels();
					gCompareWord = the first label in gClassLabels existing in Wordnet;
				}
				
				if(gCompareWord!=null){
					Set<Synset> oSynsets = wordnet.getSynsets(oCompareWord);
					Set<Synset> gSynsets = wordnet.getSynsets(gCompareWord);
					score = |oSynsets INTERSECT gSynsets| / |oSynsets UNION gSynsets| ;
				}
				else score = 0;

			}
			
			else score = 0;
			
		}	
		
	}
	return score;
		 */
		return 0;
	}



	private static void fillMap(Map<String,Integer> map, Set<Resource> resources){
		int i = 0;
		for(Resource resource : resources){
			map.put(resource.stringValue(), i);
			i++;
		}
	}
	
	
}
