package it.uniroma2.art.exodus.exodus_validator.ui;

import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultListModel;

import it.uniroma2.art.exodus.exodus_validator.structure.BoxLocation;

public class BoxLocationListModel extends DefaultListModel<BoxLocation> {
	
	
	private static final long serialVersionUID = 5958555797468865872L;
	
	
	public List<BoxLocation> getBoxLocationList(){
		List<BoxLocation> ret = new ArrayList<>();
		Object[] boxLocationsObj = super.toArray();
		for(int i = 0 ; i < boxLocationsObj.length ; i++)
			ret.add((BoxLocation) boxLocationsObj[i]);
		
		return ret;
	}

}
