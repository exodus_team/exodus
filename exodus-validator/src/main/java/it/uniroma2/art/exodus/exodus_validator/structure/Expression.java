package it.uniroma2.art.exodus.exodus_validator.structure;

import java.util.List;
import java.util.Set;

import it.uniroma2.art.exodus.exodus_validator.structure.MultipleExpression.Type;

public abstract class Expression<T> {
	
	public static final Expression TRUE = new MultipleExpression(Type.AND);
	public static final Expression FALSE = new MultipleExpression(Type.OR);
	
	public abstract boolean evaluate(Evaluator<T> evaluator);
	
	public abstract Set<T> getAllItems();
	
	@Override
	public abstract boolean equals(Object other);
	
	@Override
	public abstract String toString();
	
	@Override
	public int hashCode(){
		return this.toString().hashCode();
	}
	
}
