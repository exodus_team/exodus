package it.uniroma2.art.exodus.exodus_validator.structure;

public enum RuleType {
	CONSISTENCY_CHECKING, INFERRED_TRIPLES_GENERATION
}
