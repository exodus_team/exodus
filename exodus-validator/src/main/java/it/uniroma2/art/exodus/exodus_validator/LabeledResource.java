package it.uniroma2.art.exodus.exodus_validator;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.rdf4j.model.Resource;

public class LabeledResource<T extends Resource> {
	
	private T resource;
	private List<String> labels;
	
	public LabeledResource(T resource) {
		this.resource = resource;
		this.labels = new ArrayList<String>();
	}
	
	public void addLabel(String label){
		this.labels.add(label);
	}
	
	public void addLabels(List<String> labels){
		this.labels.addAll(labels);
	}
	
	public List<String> getLabels(){
		List<String> ret = new ArrayList<>(this.labels);
		return ret;
	}
	
	public T getResource(){
		return resource;
	}


}
