package it.uniroma2.art.exodus.exodus_validator.structure;

import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Value;

import it.uniroma2.art.exodus.exodus_validator.BoxTransitionHandler;
import it.uniroma2.art.exodus.exodus_validator.ValidationPhase;
import it.uniroma2.art.exodus.exodus_validator.Validator;

//prima che comincio a sviluppare la validazione manuale, questa classe dovrà essere pesantemente rifattorizzata
//in modo da gestire la differekza tra validazione automatica (che utilizza le mappe di appoggio) e quella manuale (che non le utilizza)
public class ValidationHandler extends BoxTransitionHandler{
	
	private static final Logger logger = LogManager.getLogger(Validator.class);
	
	private static ValidationHandler instance = null;
	
	private static Box plusBox;
	private static Box minusBox;
	private static Box interrogationBox;
	
	//it's useful to make box location search speeder
	private static Map<Triple,BoxLocation> plusOrInterrogationTriplesMapping;
	private static Map<Triple,BoxLocation> minusTriplesMapping;
	
	private static Box validatedBox;
	private static Box rejectedBox;
	
	
	private static Map<BoxType,Box> boxByBoxeType;
	private static Map<BoxType, Map<Triple,BoxLocation>> tripleMappingByBoxType;
	
	
	
	private static void initBoxByBoxType(){
		boxByBoxeType = new TreeMap<>();
		boxByBoxeType.put(BoxType.PLUS, plusBox);
		boxByBoxeType.put(BoxType.MINUS, minusBox);
		boxByBoxeType.put(BoxType.INTERROGATION, interrogationBox);
		boxByBoxeType.put(BoxType.VALIDATED, validatedBox);
		boxByBoxeType.put(BoxType.REJECTED, rejectedBox);
	}
	
	private static void initTripleMappingByBoxType(){
		tripleMappingByBoxType = new TreeMap<>();
		tripleMappingByBoxType.put(BoxType.PLUS, plusOrInterrogationTriplesMapping);
		tripleMappingByBoxType.put(BoxType.INTERROGATION, plusOrInterrogationTriplesMapping);
		tripleMappingByBoxType.put(BoxType.MINUS, minusTriplesMapping);
	}
	
	
	
	private ValidationHandler(){
		plusBox = new Box(BoxType.PLUS);
		minusBox = new Box(BoxType.MINUS);
		interrogationBox = new Box(BoxType.INTERROGATION);
		
		plusOrInterrogationTriplesMapping = new TreeMap<>();
		minusTriplesMapping = new TreeMap<>();
		
		validatedBox = new Box(BoxType.VALIDATED);
		rejectedBox = new Box(BoxType.REJECTED);
		
		initBoxByBoxType();
		initTripleMappingByBoxType();
	}
	
	
	public static ValidationHandler getInstance(){
		if (instance == null)
			instance = new ValidationHandler();
		return instance;
	}
	
	private void addToPlusBoxPostProcessing(BoxLocation boxLocation){
		boxLocation.setBoxType(BoxType.PLUS);
		plusBox.addBoxLocation(boxLocation); 
	}
	
	private void addToMinusBoxPostProcessing(BoxLocation boxLocation){
		boxLocation.setBoxType(BoxType.MINUS);
		minusBox.addBoxLocation(boxLocation); 
	}
	
	private void addToInterrogationBoxPostProcessing(BoxLocation boxLocation){
		boxLocation.setBoxType(BoxType.INTERROGATION);
		interrogationBox.addBoxLocation(boxLocation);
	}
	
	private void addToValidatedBoxPostProcessing(BoxLocation boxLocation){
		boxLocation.setBoxType(BoxType.VALIDATED);
		validatedBox.addBoxLocation(boxLocation); 
	}
	
	private void addToRejectedBoxPostProcessing(BoxLocation boxLocation){
		boxLocation.setBoxType(BoxType.REJECTED);
		rejectedBox.addBoxLocation(boxLocation); 
	}
	
	@Override
	public void remove(BoxLocation boxLocation){
		Box box = getBoxByType(boxLocation.getBoxType());
		box.removeBoxLocation(boxLocation);
	}
	
	@Override
	public void move(BoxLocation boxLocation, BoxType boxTypeTarget){
		BoxType boxTypeSource = boxLocation.getBoxType();
		
		if(boxTypeSource== null){
			if(BoxTransitionHandler.validationPhase.equals(ValidationPhase.AUTOMATIC_VALIDATION)){
				if(boxTypeTarget.equals(BoxType.PLUS))
					addToPlusBox(boxLocation);
				else if(boxTypeTarget.equals(BoxType.INTERROGATION))
					addToInterrogationBox(boxLocation);
				else if(boxTypeTarget.equals(BoxType.MINUS))
					addToMinusBox(boxLocation);
				else throw new IllegalArgumentException("Operation not hallowed for automatic validation phase");
			}
			else if (BoxTransitionHandler.validationPhase.equals(ValidationPhase.MANUAL_VALIDATION))
				addByBoxTypePostProcessing(boxLocation, boxTypeTarget);
			else throw new IllegalStateException("Validation phase "+ BoxTransitionHandler.validationPhase + "not handled");
		}
		
		
		else if (!boxTypeSource.equals(boxTypeTarget)){
			boxLocation.setBoxType(boxTypeTarget);
			boxByBoxeType.get(boxTypeTarget).addBoxLocation(boxLocation);
			boxByBoxeType.get(boxTypeSource).removeBoxLocation(boxLocation);
			
			if(BoxTransitionHandler.validationPhase.equals(ValidationPhase.AUTOMATIC_VALIDATION)){
				Map<Triple,BoxLocation> sourceTripleMapping = tripleMappingByBoxType.getOrDefault(boxTypeSource, null);
				Map<Triple,BoxLocation> targetTripleMapping = tripleMappingByBoxType.getOrDefault(boxTypeTarget, null);
				
				Triple triple = boxLocation.getTriple();
				
				if(sourceTripleMapping!=null && sourceTripleMapping.equals(targetTripleMapping)){
					if(!sourceTripleMapping.containsKey(triple))
						sourceTripleMapping.put(triple, boxLocation);
				}
				else {
					if(sourceTripleMapping!=null && sourceTripleMapping.containsKey(triple))
						sourceTripleMapping.remove(triple);
					
					if(targetTripleMapping!=null)
						targetTripleMapping.put(triple, boxLocation);
				}
				
			}
		}
		
	}
	
	
	public void addByBoxTypePostProcessing(BoxLocation boxLocation, BoxType boxTypeTarget){
		if(boxTypeTarget.equals(BoxType.PLUS))
			addToPlusBoxPostProcessing(boxLocation);
		else if(boxTypeTarget.equals(BoxType.MINUS))
			addToMinusBoxPostProcessing(boxLocation);
		else if(boxTypeTarget.equals(BoxType.INTERROGATION))
			addToInterrogationBoxPostProcessing(boxLocation);
		else if(boxTypeTarget.equals(BoxType.VALIDATED))
			addToValidatedBoxPostProcessing(boxLocation);
		else if(boxTypeTarget.equals(BoxType.REJECTED))
			addToRejectedBoxPostProcessing(boxLocation);
		else throw new IllegalArgumentException("Box Type "+boxTypeTarget+ "not handled");
	}
	
	
	private void moveFromPlusToInterrogationBox(BoxLocation boxLocation){
		boxLocation.setBoxType(BoxType.INTERROGATION);
		interrogationBox.addBoxLocation(boxLocation);
		plusBox.removeBoxLocation(boxLocation);
		Triple triple = boxLocation.getTriple();
		if(BoxTransitionHandler.validationPhase.equals(ValidationPhase.AUTOMATIC_VALIDATION) && !plusOrInterrogationTriplesMapping.containsKey(triple))
			plusOrInterrogationTriplesMapping.put(triple, boxLocation);
	}
	
	
	private void moveFromPlusToMinusBox(BoxLocation boxLocation){
		boxLocation.setBoxType(BoxType.MINUS);
		minusBox.addBoxLocation(boxLocation);
		plusBox.removeBoxLocation(boxLocation);
		Triple triple = boxLocation.getTriple();
		if(BoxTransitionHandler.validationPhase.equals(ValidationPhase.AUTOMATIC_VALIDATION)){
			if(plusOrInterrogationTriplesMapping.containsKey(triple))
				plusOrInterrogationTriplesMapping.remove(triple);
			
			minusTriplesMapping.put(triple, boxLocation);
		}
	}
	
	private void moveFromInterrogationToMinusBox(BoxLocation boxLocation){
		boxLocation.setBoxType(BoxType.MINUS);
		minusBox.addBoxLocation(boxLocation);
		interrogationBox.removeBoxLocation(boxLocation);
		Triple triple = boxLocation.getTriple();
		if(BoxTransitionHandler.validationPhase.equals(ValidationPhase.AUTOMATIC_VALIDATION)){
			if(plusOrInterrogationTriplesMapping.containsKey(triple))
				plusOrInterrogationTriplesMapping.remove(triple);
			
			minusTriplesMapping.put(triple, boxLocation);
		}
	}
	
	
	private void moveFromInterrogationToPlusBox(BoxLocation boxLocation){
		boxLocation.setBoxType(BoxType.PLUS);
		plusBox.addBoxLocation(boxLocation);
		interrogationBox.removeBoxLocation(boxLocation);
		Triple triple = boxLocation.getTriple();
		if(BoxTransitionHandler.validationPhase.equals(ValidationPhase.AUTOMATIC_VALIDATION) && !plusOrInterrogationTriplesMapping.containsKey(triple))
			plusOrInterrogationTriplesMapping.put(boxLocation.getTriple(), boxLocation);
	}
	
	
	
	
	
	
	
	
	
	
	public void addToPlusBox(BoxLocation boxLocation){
		Triple triple = boxLocation.getTriple();
		if(!plusOrInterrogationTriplesMapping.containsKey(triple)){
			boxLocation.setBoxType(BoxType.PLUS);
			plusBox.addBoxLocation(boxLocation);
			plusOrInterrogationTriplesMapping.put(boxLocation.getTriple(), boxLocation);
			logger.debug("Triple "+ boxLocation + " added to +Box");
		}
	}
	
	public void addToMinusBox(BoxLocation boxLocation){
		Triple triple = boxLocation.getTriple();
		if(!minusTriplesMapping.containsKey(triple)){
			boxLocation.setBoxType(BoxType.MINUS);
			minusBox.addBoxLocation(boxLocation);
			minusTriplesMapping.put(boxLocation.getTriple(), boxLocation);
			logger.debug("Triple "+ boxLocation + " added to -Box");
		}
	}
	
	public void addToInterrogationBox(BoxLocation boxLocation){
		Triple triple = boxLocation.getTriple();
		if(!plusOrInterrogationTriplesMapping.containsKey(triple)){
			boxLocation.setBoxType(BoxType.INTERROGATION);
			interrogationBox.addBoxLocation(boxLocation);
			plusOrInterrogationTriplesMapping.put(boxLocation.getTriple(), boxLocation);
			logger.debug("Triple "+ boxLocation + " added to ?Box");
		}
	}
	
	//it is not considering triples in "- Box", and all triples not put into "+ Box" or "? Box" at the beginning
	public BoxLocation getPlusOrInterrogationBoxLocation(Triple triple){
		return plusOrInterrogationTriplesMapping.get(triple);
	}
	
	public BoxLocation getPlusOrInterrogationBoxLocation(Resource subject, IRI predicate, Value object){
		//System.out.println("pre-processing");
		BoxLocation ret = plusOrInterrogationTriplesMapping.get(new Triple(subject, predicate, object));
		//System.out.println("post-processing");
		return ret;
	}
	
	public BoxLocation getMinusBoxLocation(Triple triple){
		return minusTriplesMapping.get(triple);
	}
	
	public BoxLocation getMinusBoxLocation(Resource subject, IRI predicate, Value object){
		return minusTriplesMapping.get(new Triple(subject, predicate, object));
	}
	
	public Map<Triple,BoxLocation> getPlusOrInterrogationTriplesMapping(){
		return plusOrInterrogationTriplesMapping;
	}
	
	public Map<Triple,BoxLocation> getMinusTriplesMapping(){
		return minusTriplesMapping;
	}
	
	
	public Box getBoxByType(BoxType type){
		Box ret;
		if(type.equals(BoxType.MINUS))
			ret = minusBox;
		else if(type.equals(BoxType.PLUS))
			ret = plusBox;
		else if(type.equals(BoxType.INTERROGATION))
			ret = interrogationBox;
		else if(type.equals(BoxType.REJECTED))
			ret = rejectedBox;
		else ret = validatedBox;
		return ret;
	}
	
	
	/*public void fillBoxes(){
		Collection<BoxLocation> minusBoxLocations = minusTriplesMapping.values();
		minusBoxLocations.stream().forEachOrdered(minusBox::addBoxLocation);
		
		Collection<BoxLocation> plusOrInterrogationBoxLocations = plusOrInterrogationTriplesMapping.values();
		plusOrInterrogationBoxLocations.stream().forEachOrdered(boxLocation -> {
			BoxType boxLocationType = boxLocation.getBoxType();
			if(boxLocationType.equals(BoxType.PLUS))
				plusBox.addBoxLocation(boxLocation);
			else interrogationBox.addBoxLocation(boxLocation);
		});
		
	}*/
	

}
