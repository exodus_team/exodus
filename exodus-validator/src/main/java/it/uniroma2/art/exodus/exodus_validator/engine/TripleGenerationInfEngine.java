package it.uniroma2.art.exodus.exodus_validator.engine;

import java.util.List;

import it.uniroma2.art.exodus.exodus_validator.BFSOntologyClassExplorer;

public class TripleGenerationInfEngine extends InfEngine<TripleGenerationInfRule, TripleGenerationInfEngineBinder>{

	public TripleGenerationInfEngine(BFSOntologyClassExplorer kbExplorer, List<PreFilter> preFilters, TripleGenerationInfRule...infRules) {
		super(TripleGenerationInfEngineBinder.class, kbExplorer, preFilters, infRules);
	}
	
	public TripleGenerationInfEngine(BFSOntologyClassExplorer kbExplorer, TripleGenerationInfRule...infRules) {
		super(TripleGenerationInfEngineBinder.class, kbExplorer, infRules);
	}
	
	

}
