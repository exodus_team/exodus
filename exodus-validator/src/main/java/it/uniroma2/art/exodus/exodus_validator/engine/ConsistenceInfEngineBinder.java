package it.uniroma2.art.exodus.exodus_validator.engine;

import it.uniroma2.art.exodus.exodus_validator.structure.Triple;

public class ConsistenceInfEngineBinder extends Binder<ConsistenceInfRule>{

	public ConsistenceInfEngineBinder(Triple triple, BindingTriple precondition) {
		super(triple, precondition);
		
	}

}
