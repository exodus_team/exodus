package it.uniroma2.art.exodus.exodus_validator.ui;



import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;







public class AutoValidationGUI extends JFrame implements ActionListener{
	
	
    
	private static final long serialVersionUID = -3563048391964999113L;
	
	
    private JLabel running_label;
    
    private JPanel p;
    private JButton esegui;
    
    
    
   
    
    
    public AutoValidationGUI(){
		super("Automatic Validator");
		createAndShowGUI();
    }
	


    public Container createPane(){
		
    	GridBagLayout gridBagLayout=new GridBagLayout();
    	GridBagConstraints gbc;
    	p = new JPanel();
    	p.setLayout(gridBagLayout);
    	p.setPreferredSize(new Dimension(400,180));
 		
		gbc=new GridBagConstraints();
		gbc.gridx=2;
		gbc.gridy=0;
		gbc.anchor=GridBagConstraints.CENTER;
		gbc.insets=new Insets(10,10,10,10);
			
		gbc=new GridBagConstraints();
		gbc.gridx=2;
		gbc.gridy=1;
		gbc.anchor=GridBagConstraints.CENTER;
		gbc.insets=new Insets(5,5,5,5);
	
		running_label = new JLabel();
		gbc=new GridBagConstraints();
		gbc.gridx=0;
		gbc.gridy=2;
		gbc.anchor=GridBagConstraints.WEST;
		gbc.insets=new Insets(1,1,1,1);
		running_label.setVisible(false);
		running_label.setText("Running...");
		((GridBagLayout)p.getLayout()).setConstraints(running_label, gbc);
		p.add(running_label);
			
		esegui= new JButton("Execute");
		esegui.addActionListener(this);
			
		gbc=new GridBagConstraints();
		gbc.gridx=0;
		gbc.gridy=3;
		gbc.anchor=GridBagConstraints.NORTH;
		gbc.gridwidth=2;
		gbc.insets=new Insets(1,1,1,1);
		((GridBagLayout)p.getLayout()).setConstraints(esegui, gbc);
		p.add(esegui);
		
		JLabel emptyLabel = new JLabel();
		gbc=new GridBagConstraints();
		gbc.gridx=0;
		gbc.gridy=5;
		gbc.anchor=GridBagConstraints.WEST;
		gbc.insets=new Insets(1,1,1,1);
		emptyLabel.setVisible(true);
		String emptyString = " ";
		emptyLabel.setText(emptyString);
		((GridBagLayout)p.getLayout()).setConstraints(emptyLabel, gbc);
		p.add(emptyLabel);
		
		JLabel emptyLabel1 = new JLabel();
		gbc=new GridBagConstraints();
		gbc.gridx=0;
		gbc.gridy=6;
		gbc.anchor=GridBagConstraints.WEST;
		gbc.insets=new Insets(1,1,1,1);
		emptyLabel1.setVisible(true);
		emptyLabel1.setText(emptyString);
		((GridBagLayout)p.getLayout()).setConstraints(emptyLabel1, gbc);
		p.add(emptyLabel1);
			
		return p;

    }
    
    
    /*@param event di tipo ActionEvent: rappresenta un evento (pressione di un pulsante)*/
    public void actionPerformed(ActionEvent event){
        String command = event.getActionCommand();
        if(command.equalsIgnoreCase("execute")){
	    	GuiManager.setUi(this);
	    	esegui.setEnabled(false);
			notifyEsecuzioneInCorso();
			GuiManager.executionStarted();
        }
    }

     
    public void createAndShowGUI(){
	    Container contentPane = this.getContentPane();
		try{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch(Exception e){
		      System.out.println(e.toString());
		      e.printStackTrace();
		}
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		contentPane.add(createPane(),BorderLayout.CENTER);
		this.setSize(400, 600);
		this.pack();
		this.setVisible(true);
    }
    
    
    private void notifyEsecuzioneInCorso(){
    	running_label.setVisible(true);
    }
	   
}
        



