package it.uniroma2.art.exodus.exodus_validator;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;


public class PropertiesManager {
	
	public static String PROPERTIES_FILE = "exodus-validator.properties";
	
	private static PropertiesManager instance = null;
	
	private static String ontologyFilepath;
	private static String inferredOntologyFilepath;
	private static boolean forceInferredOntoFileGeneration;
	private static String graphDir;
	private static String finalOntologyFilepath;
	
	private static String consistenceInfRulesFilepath;
	private static String tripleGenInfRulesFilepath;
	
	private static int maxInferenceLevel;
	
	private static boolean createAllRulesForTemplate;
	private static boolean persistGeneratedTriples;
	
	
	
	private PropertiesManager(){
		init();
	}
	
	
	public static PropertiesManager getInstance(){
		if (instance == null)
			instance = new PropertiesManager();
		return instance;
	}
	
	
	
	private static void init(){
		
		
		Properties prop = new Properties();
		InputStream input = null;

		try {

			Class<PropertiesManager> thisClass = PropertiesManager.class;
			URL propertiesURL = thisClass.getClassLoader().getResource(PROPERTIES_FILE);
			System.out.println(propertiesURL);
			input = propertiesURL.openStream();
			
			prop.load(input);

			ontologyFilepath = prop.getProperty("OntologyFilepath");
			
			inferredOntologyFilepath = prop.getProperty("InferredOntologyFilepath");
			
			finalOntologyFilepath = prop.getProperty("FinalOntologyFilepath", "");
			if(finalOntologyFilepath.trim().equals(""))
				finalOntologyFilepath = inferredOntologyFilepath;
			
			forceInferredOntoFileGeneration = Boolean.parseBoolean(prop.getProperty("ForceInferredOntoFileGeneration", "false"));
			
			graphDir = prop.getProperty("GraphDir");
			
			consistenceInfRulesFilepath = prop.getProperty("ConsistenceInfRulesFilepath");
			
			tripleGenInfRulesFilepath = prop.getProperty("TripleGenInfRulesFilepath");
			
			try{
				maxInferenceLevel = Integer.parseInt(prop.getProperty("MaxInferenceLevel","-1"));
			}
			catch (NumberFormatException e){
				maxInferenceLevel = -1;
			}
			
			createAllRulesForTemplate = true;
			String createAllRulesForTemplateStr = prop.getProperty("CreateAllRulesForTemplate", "Y").trim();
			if(createAllRulesForTemplateStr.equalsIgnoreCase("N") || createAllRulesForTemplateStr.equalsIgnoreCase("NO") || createAllRulesForTemplateStr.equalsIgnoreCase("false") || createAllRulesForTemplateStr.equalsIgnoreCase("0"))
				createAllRulesForTemplate = false;
			
			persistGeneratedTriples = false;
			String persistGeneratedTriplesStr = prop.getProperty("PersistGeneratedTriples", "N").trim();
			if(persistGeneratedTriplesStr.equalsIgnoreCase("Y") || persistGeneratedTriplesStr.equalsIgnoreCase("YES") || persistGeneratedTriplesStr.equalsIgnoreCase("true") || persistGeneratedTriplesStr.equalsIgnoreCase("1"))
				createAllRulesForTemplate = true;
			
			
			
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	  
		
	}


	public String getOntologyFilepath() {
		return ontologyFilepath;
	}
	
	public String getInferredOntologyFilepath() {
		return inferredOntologyFilepath;
	}
	
	public String getFinalOntologyFilepath() {
		return finalOntologyFilepath;
	}

	public boolean forceInferredOntoFileGeneration(){
		return forceInferredOntoFileGeneration;
	}

	public String getGraphDir() {
		return graphDir;
	}


	public static String getPROPERTIES_FILE() {
		return PROPERTIES_FILE;
	}


	public boolean isForceInferredOntoFileGeneration() {
		return forceInferredOntoFileGeneration;
	}


	public String getConsistenceInfRulesFilepath() {
		return consistenceInfRulesFilepath;
	}


	public String getTripleGenInfRulesFilepath() {
		return tripleGenInfRulesFilepath;
	}


	public int getMaxInferenceLevel() {
		return maxInferenceLevel;
	}


	public boolean isCreateAllRulesForTemplate() {
		return createAllRulesForTemplate;
	}


	public boolean isPersistGeneratedTriples() {
		return persistGeneratedTriples;
	}
	
	
	
	



	
	
	
	
	
	
	



}
