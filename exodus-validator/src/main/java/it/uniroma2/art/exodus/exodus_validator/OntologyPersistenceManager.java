package it.uniroma2.art.exodus.exodus_validator;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.eclipse.rdf4j.model.BNode;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.ModelFactory;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.LinkedHashModel;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.impl.StatementImpl;
import org.eclipse.rdf4j.model.impl.ValueFactoryImpl;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.vocabulary.OWL;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFHandlerException;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.UnsupportedRDFormatException;
import org.semanticweb.HermiT.Configuration;
import org.semanticweb.HermiT.Reasoner;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.formats.RDFXMLDocumentFormat;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAnnotation;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataPropertyExpression;
import org.semanticweb.owlapi.model.OWLDocumentFormat;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyFormat;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.reasoner.ConsoleProgressMonitor;
import org.semanticweb.owlapi.reasoner.InferenceType;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerConfiguration;
import org.semanticweb.owlapi.reasoner.SimpleConfiguration;
import org.semanticweb.owlapi.util.InferredAxiomGenerator;
import org.semanticweb.owlapi.util.InferredClassAssertionAxiomGenerator;
import org.semanticweb.owlapi.util.InferredDataPropertyCharacteristicAxiomGenerator;
import org.semanticweb.owlapi.util.InferredDisjointClassesAxiomGenerator;
import org.semanticweb.owlapi.util.InferredEquivalentClassAxiomGenerator;
import org.semanticweb.owlapi.util.InferredEquivalentDataPropertiesAxiomGenerator;
import org.semanticweb.owlapi.util.InferredEquivalentObjectPropertyAxiomGenerator;
import org.semanticweb.owlapi.util.InferredInverseObjectPropertiesAxiomGenerator;
import org.semanticweb.owlapi.util.InferredObjectPropertyCharacteristicAxiomGenerator;
import org.semanticweb.owlapi.util.InferredOntologyGenerator;
import org.semanticweb.owlapi.util.InferredPropertyAssertionGenerator;
import org.semanticweb.owlapi.util.InferredSubClassAxiomGenerator;
import org.semanticweb.owlapi.util.InferredSubDataPropertyAxiomGenerator;
import org.semanticweb.owlapi.util.InferredSubObjectPropertyAxiomGenerator;
import org.semanticweb.owlapi.util.NamespaceUtil;
import org.semanticweb.owlapi.vocab.PrefixOWLOntologyFormat;

import it.uniroma2.art.exodus.exodus_validator.structure.BoxLocation;
import it.uniroma2.art.exodus.exodus_validator.structure.PrefixMapper;
import it.uniroma2.art.exodus.exodus_validator.structure.Triple;
import it.uniroma2.art.exodus.exodus_validator.ui.MainClass;
import uk.ac.manchester.cs.jfact.JFactFactory;
import uk.ac.manchester.cs.owl.owlapi.OWLAxiomImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLDataFactoryImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLDataPropertyAssertionAxiomImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLDisjointClassesAxiomImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLObjectPropertyAssertionAxiomImpl;

public class OntologyPersistenceManager {
	
	public static final String DUMMY_PREFIX_FOR_BLANK_NODES = "http://art.uniroma2.it/bnprefix/";
	public static final String DUMMY_ALIAS_FOR_BLANK_NODES = "bnprefix";
	
	private static final Logger logger = LogManager.getLogger(OntologyPersistenceManager.class);
	
	private String ontologyFilepath;
	private String inferredOntologyFilepath;
	private String graphDirFilepath;
	
	private Model ontologyModel = null;
	private Model inferredOntologyModel = null;
	private List<Model> graphModels;
	
	private PrefixMapper prefixMapper;
	
	public OntologyPersistenceManager(String ontologyFilepath, String inferredOntologyFilepath, String graphDirFilepath){
		this.ontologyFilepath = ontologyFilepath;
		this.inferredOntologyFilepath = inferredOntologyFilepath;
		this.graphDirFilepath = graphDirFilepath;
		this.graphModels = new ArrayList<>();
		initializePrefixMapper();
	}
	
	private void initializePrefixMapper(){
		this.prefixMapper = PrefixMapper.getInstance();
		this.prefixMapper.put(DUMMY_PREFIX_FOR_BLANK_NODES, DUMMY_ALIAS_FOR_BLANK_NODES);
		NamespaceUtil util = new NamespaceUtil();
		util.getNamespace2PrefixMap().forEach(this.prefixMapper::put);
	}
	
	public void initializeModels(boolean forceInferredOntoFileGeneration){
		try{
			File graphDir = new File(graphDirFilepath);
			File[] graphFiles = graphDir.listFiles();
			
			Arrays.sort(graphFiles, (File f1, File f2) -> Long.compare(f1.lastModified(), f2.lastModified()) );
			
			File inferredOntologyModelFile = new File(inferredOntologyFilepath);
			if (!inferredOntologyModelFile.exists() || forceInferredOntoFileGeneration)
				initializeInfOntoModel();
			
			try{
				initializeOntologyModel();
			}
			catch (IOException e){
				logger.warn("ontology model cannot be instantied, but inferred ontology model exists, then processing will go on");
				System.err.println("ontology model cannot be instantied, but inferred ontology model exists, then processing will go on");
			}
			
			logger.debug("inferredOntologyFilepath: "+inferredOntologyFilepath);
			FileInputStream infOntoFileInputStream = new FileInputStream(inferredOntologyFilepath);
			
			Model inferredOntologyModelTemp = Rio.parse(infOntoFileInputStream, "", RDFFormat.RDFXML);
			//this.inferredOntologyModel = Rio.parse(infOntoFileInputStream, "", RDFFormat.RDFXML);
			this.inferredOntologyModel = new LinkedHashModel();
			
			this.inferredOntologyModel.addAll(inferredOntologyModelTemp.stream().map(OntologyPersistenceManager::getStatementWithIRIzedBlankNodes).collect(Collectors.toList()));
			
			logger.info("Begin Ontology statements size: " + this.inferredOntologyModel.size());
			
			for(int i = 0 ; i < graphFiles.length ; i++){
				try{
					Model currGraphModel = initializeModel(graphFiles[i]);
					graphModels.add(currGraphModel);
				}
				catch (UnsupportedRDFormatException e){
					System.err.println("This is not a (supported) ontology file: go on!!");
				}
				
			}
			
		}
		catch (IOException | OWLOntologyCreationException | OWLOntologyStorageException e){
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}
	
	
	
	
	public void persistIntoOntologyAsRDF(String finalOntologyFilepath, List<BoxLocation> boxLocationsToPersist, boolean persistGeneratedTriples){
		boxLocationsToPersist.forEach(boxLocation -> {
			if(persistGeneratedTriples || !boxLocation.isTripleInferred()){
				Triple triple = boxLocation.getTriple();
				inferredOntologyModel.add(triple.getSubject(), triple.getPredicate(), triple.getObject());
			}
		});
		
		try {
			Rio.write(inferredOntologyModel, new FileOutputStream(finalOntologyFilepath), RDFFormat.TURTLE);
		}
		catch (RDFHandlerException | IOException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		} 
	}
	
	
	
	
	public void persistIntoOntologyAsOWL(String finalOntologyFilepath, List<BoxLocation> boxLocationsToPersist){
		try {
			OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
			OWLDataFactory factory = manager.getOWLDataFactory();
			new File(finalOntologyFilepath).createNewFile();
			IRI ontology_iri = IRI.create(new File(inferredOntologyFilepath));
			OWLOntology ontology = manager.loadOntologyFromOntologyDocument(ontology_iri);
			boxLocationsToPersist.forEach(boxLocation -> {
				Triple triple = boxLocation.getTriple();
				OWLNamedIndividual owlSubject = factory.getOWLNamedIndividual(IRI.create(triple.getSubject().stringValue()));
				
				if(triple.getObject() instanceof Literal){
					OWLDataPropertyExpression owlPredicate = factory.getOWLDataProperty(IRI.create(triple.getPredicate().toString()));
					OWLLiteral owlObject = factory.getOWLLiteral( ((Literal) triple.getObject()).stringValue());
					manager.addAxiom(ontology, new OWLDataPropertyAssertionAxiomImpl(owlSubject, owlPredicate, owlObject, new HashSet<OWLAnnotation>()));
				}
				else {
					OWLObjectPropertyExpression owlPredicate = factory.getOWLObjectProperty(IRI.create(triple.getPredicate().toString()));
					OWLNamedIndividual owlObject = factory.getOWLNamedIndividual(IRI.create( ((Resource)triple.getObject()).stringValue()));
					manager.addAxiom(ontology, new OWLObjectPropertyAssertionAxiomImpl(owlSubject, owlPredicate, owlObject, new HashSet<OWLAnnotation>()));
				}
			});
			
			manager.saveOntology(ontology, new RDFXMLDocumentFormat(), new FileOutputStream(finalOntologyFilepath));
			
		} catch (RDFHandlerException | IOException | OWLOntologyCreationException | OWLOntologyStorageException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		} 


	}
	
	
	private void initializeInfOntoModel() throws IOException, OWLOntologyCreationException, OWLOntologyStorageException{
		new File(inferredOntologyFilepath).createNewFile();
		IRI ontology_iri = IRI.create(new File(ontologyFilepath));
		OWLOntologyManager m = OWLManager.createOWLOntologyManager();
		OWLOntology ontology = m.loadOntologyFromOntologyDocument(ontology_iri);
		
		OWLDocumentFormat documentFormat = m.getOntologyFormat(ontology);
		if(documentFormat instanceof PrefixOWLOntologyFormat ){
			PrefixOWLOntologyFormat  ontologyFormat = (PrefixOWLOntologyFormat ) documentFormat;
			ontologyFormat.getPrefixName2PrefixMap().forEach( (prefix,alias) -> {
				if(StringUtils.isNotBlank(prefix) && StringUtils.isNotBlank(alias))
					this.prefixMapper.put(prefix, alias);
			});
		}
		
		
		JFactFactory reasonerFactory = new JFactFactory();
		OWLReasonerConfiguration conf = new SimpleConfiguration(50000);
		
		OWLReasoner reasoner = reasonerFactory.createReasoner(ontology, conf);
		
		List<InferredAxiomGenerator<? extends OWLAxiom>> gens = new ArrayList<InferredAxiomGenerator<? extends OWLAxiom>>();
		gens.add(new InferredClassAssertionAxiomGenerator());
		gens.add(new InferredSubClassAxiomGenerator());
		gens.add(new InferredEquivalentClassAxiomGenerator());
		gens.add(new InferredDisjointClassesAxiomGenerator());
		//gens.add(new InferredDataPropertyCharacteristicAxiomGenerator());
		gens.add(new InferredEquivalentDataPropertiesAxiomGenerator());
		gens.add(new InferredSubDataPropertyAxiomGenerator());
		gens.add(new InferredPropertyAssertionGenerator());
		gens.add(new InferredInverseObjectPropertiesAxiomGenerator());
		gens.add(new InferredEquivalentObjectPropertyAxiomGenerator());
		//gens.add(new InferredObjectPropertyCharacteristicAxiomGenerator());
		gens.add(new InferredSubObjectPropertyAxiomGenerator());
		gens.add(new InferredSubDataPropertyAxiomGenerator());
		
		OWLOntology infOnt = m.createOntology();
		
		reasoner.precomputeInferences(InferenceType.CLASS_HIERARCHY, InferenceType.SAME_INDIVIDUAL, InferenceType.DISJOINT_CLASSES, InferenceType.DATA_PROPERTY_HIERARCHY, InferenceType.OBJECT_PROPERTY_HIERARCHY, InferenceType.CLASS_ASSERTIONS, InferenceType.DATA_PROPERTY_ASSERTIONS, InferenceType.OBJECT_PROPERTY_ASSERTIONS ) ;
		
		// create the inferred ontology generator
		InferredOntologyGenerator iog =
		new InferredOntologyGenerator(reasoner, gens);
		
		iog.fillOntology(new OWLDataFactoryImpl(), infOnt);
		
		infOnt.addAxioms(ontology.getAxioms(true));
		
		m.saveOntology(infOnt, new RDFXMLDocumentFormat(), new FileOutputStream(inferredOntologyFilepath));
		
		//computeMissingInferences();
		
	}
	
	
	private boolean isCorrectFormat(File file, RDFFormat format){
		boolean formatFind = true;
		try {
			FileInputStream fileInputStream = new FileInputStream(file);
			Rio.parse(fileInputStream, "", format);
		}  
		catch (UnsupportedRDFormatException | RDFParseException | IOException e) {
			formatFind = false;
		}
		
		return formatFind;
	}
	
	
	
	
	private Model initializeModel(File file) throws UnsupportedRDFormatException, FileNotFoundException{
		if(!file.exists())
			throw new FileNotFoundException("File "+ file.getAbsolutePath() + " not found!!");
		
		List<RDFFormat> formats = new ArrayList<>();
		formats.add(RDFFormat.BINARY);
		formats.add(RDFFormat.N3);
		formats.add(RDFFormat.NQUADS);
		formats.add(RDFFormat.NTRIPLES);
		formats.add(RDFFormat.RDFA);
		formats.add(RDFFormat.RDFJSON);
		formats.add(RDFFormat.RDFXML);
		formats.add(RDFFormat.TRIG);
		formats.add(RDFFormat.TRIX);
		formats.add(RDFFormat.TURTLE);
		
		logger.debug("File for model initialization: "+file.getAbsolutePath());
		
		Optional<RDFFormat> format = formats.stream().filter(frmt -> isCorrectFormat(file, frmt)).findFirst();
		
		if(!format.isPresent())
			throw new UnsupportedRDFormatException("RDF Format not supported");
		
		RDFFormat rightFormat = format.get();
		
		
		ModelBuilder modelBuilder = new ModelBuilder();
		Model model= modelBuilder.build();
		
		Model tempModel = null;
		
		try {
			FileInputStream fileInputStream = new FileInputStream(file);
			tempModel = Rio.parse(fileInputStream, "", rightFormat);
			tempModel.getNamespaces().forEach(namespace -> {
				String prefix = namespace.getName(); //quello che nel PrefixMapper ho chiamato prefix
				String alias = namespace.getPrefix(); //quello che nel PrefixMapper ho chiamato alias
				logger.debug("prefix: "+prefix);
				logger.debug("alias: "+alias);
				if(StringUtils.isNotBlank(alias))
					prefixMapper.put(prefix, alias);
			});
			tempModel.forEach(stat -> model.add(getStatementWithIRIzedBlankNodes(stat)));
		} catch (IOException e) {
			//impossible to catch
		}
		return model;
		
	}
	
	
	private static Statement getStatementWithIRIzedBlankNodes(Statement statement){
		ValueFactory valueFactory = new ValueFactoryImpl();
		Resource subject = statement.getSubject();
		org.eclipse.rdf4j.model.IRI predicate = statement.getPredicate();
		Value object = statement.getObject();
		if(subject instanceof BNode){
			String newIRIstring = DUMMY_PREFIX_FOR_BLANK_NODES+ subject.stringValue();
			subject = valueFactory.createIRI(newIRIstring);
		}
		if(object instanceof BNode){
			String newIRIstring = DUMMY_PREFIX_FOR_BLANK_NODES+ object.stringValue();
			object = valueFactory.createIRI(newIRIstring);
		}
		Statement ret = valueFactory.createStatement(subject, predicate, object);
		return ret;
	}
	
	private void initializeOntologyModel() throws UnsupportedRDFormatException, FileNotFoundException{
		File ontologyFile = new File(this.ontologyFilepath);
		this.ontologyModel = initializeModel(ontologyFile);
	}

	public Model getOntologyModel() {
		return ontologyModel;
	}

	public Model getInferredOntologyModel() {
		return inferredOntologyModel;
	}
	
	public List<Model> getGraphModels(){
		return this.graphModels;
	}
	
	
	
	public static void main (String[] args){
		PropertiesManager propertiesManager = PropertiesManager.getInstance();
		String inferredOntologyFilepath = propertiesManager.getInferredOntologyFilepath();
		String ontologyFilepath = propertiesManager.getOntologyFilepath();
		String graphDirFilepath = propertiesManager.getGraphDir();
		boolean forceInferredOntoFileGeneration = propertiesManager.forceInferredOntoFileGeneration();
		
		OntologyPersistenceManager ontoInitializer = new OntologyPersistenceManager(ontologyFilepath, inferredOntologyFilepath, graphDirFilepath);
		
		ontoInitializer.initializeModels(forceInferredOntoFileGeneration);
		
	}
	
	
	/*private void computeMissingInferences(){
		try {
			//Model model = Rio.parse(new FileInputStream(inferredOntologyFilepath), "", RDFFormat.RDFXML);
			Model model = initializeModel(new File(inferredOntologyFilepath));
			List<Statement> missingInferredStatements = new ArrayList<Statement>();
			
			ValueFactory vf = SimpleValueFactory.getInstance();
			
			model.filter(null, OWL.COMPLEMENTOF, null).forEach(currComplementOfStmt -> {
				System.out.println("currComplementOfStmt: "+currComplementOfStmt);
				
				Resource subject = currComplementOfStmt.getSubject();
				Value object = currComplementOfStmt.getObject();

				Statement inferredStatement1 = vf.createStatement(subject, OWL.DISJOINTWITH, object);
				missingInferredStatements.add(inferredStatement1);
				
				Statement inferredStatement2 = vf.createStatement((Resource)object, OWL.DISJOINTWITH, subject);
				missingInferredStatements.add(inferredStatement2);
			});
			
			model.addAll(missingInferredStatements);
			
			Rio.write(model, new PrintStream(inferredOntologyFilepath), RDFFormat.RDFXML);
			
			
		} catch (RDFParseException | UnsupportedRDFormatException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	*/
	
	

}
