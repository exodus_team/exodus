package it.uniroma2.art.exodus.exodus_validator.engine;


import java.util.Collection;
import java.util.List;

public class TripleGenerationInfRule extends InfRule{

	private BindingTriple inferred;
	
	
	public TripleGenerationInfRule(BindingTriple preconditionBindingTriple, Collection<BindingTriple> triggingConditions, BindingTriple inferred){
		super(preconditionBindingTriple, triggingConditions);
		this.inferred = inferred;
	}
	

	public BindingTriple getInferred() {
		return inferred;
	}
	
	@Override
	public String toString(){
		return super.toString() + " ==> "+ inferred;
	}
	
	@Override
	public boolean equals(Object other){
		boolean ret = false;
		if(other instanceof TripleGenerationInfRule){
			TripleGenerationInfRule otherTripleGenerationInfRule = (TripleGenerationInfRule) other;
			if(this.inferred.equals(otherTripleGenerationInfRule.inferred) && super.equals(otherTripleGenerationInfRule))
				ret = true;
		}
		return ret;
	}
	
	@Override
	public int hashCode(){
		return this.toString().hashCode();
	}
	
	
	
}
