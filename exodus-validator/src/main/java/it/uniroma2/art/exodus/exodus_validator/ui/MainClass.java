package it.uniroma2.art.exodus.exodus_validator.ui;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import it.uniroma2.art.exodus.exodus_validator.BoxTransitionHandler;
import it.uniroma2.art.exodus.exodus_validator.ValidationPhase;
import it.uniroma2.art.exodus.exodus_validator.structure.Box;
import it.uniroma2.art.exodus.exodus_validator.structure.BoxType;
import it.uniroma2.art.exodus.exodus_validator.structure.ValidationHandler;

public class MainClass {

	private static final Logger logger = LogManager.getLogger(MainClass.class);
	
	public static void main(String[] args) {
		
		try{
			
			logger.info("Execution beginning");
			new AutoValidationGUI();
			while (!GuiManager.isExecutionStarted()){
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					logger.error("Error on thread", e);
					e.printStackTrace();
					throw e;
				}
			}
			ValidationManager.init();
			ValidationManager.autoValidate();
			
			Box plusBox = ValidationHandler.getInstance().getBoxByType(BoxType.PLUS);
			Box interrogationBox = ValidationHandler.getInstance().getBoxByType(BoxType.INTERROGATION);
			Box minusBox = ValidationHandler.getInstance().getBoxByType(BoxType.MINUS);
			
			logger.info("+Box size: "+plusBox.size() + " (inferred "+plusBox.inferredSize() + ")");
			logger.info("?Box size: "+interrogationBox.size() + " (inferred "+interrogationBox.inferredSize() + ")");
			logger.info("-Box size: "+minusBox.size());
			
			logger.info("Execution end");
			
			GuiManager.makeUIinvisible();
			logger.info("Closed Window \n\n");
			
			GuiManager.reset();
			
			BoxTransitionHandler.validationPhase = ValidationPhase.MANUAL_VALIDATION;
			
			new ManualValidationGUI();
			while (!GuiManager.isExecutionStarted()){
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					logger.error("Error on thread", e);
					e.printStackTrace();
					throw e;
				}
			}
		}
		
		catch(Throwable t){
			logger.error("Execution terminated for an error", t);
			t.printStackTrace();
		}
		finally{
			if(GuiManager.getUi()!=null)
				GuiManager.closeUI();
		}
		
		
	}

}
