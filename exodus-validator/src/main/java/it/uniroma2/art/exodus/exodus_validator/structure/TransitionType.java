package it.uniroma2.art.exodus.exodus_validator.structure;

public enum TransitionType {
	
	FROM_PLUS_TO_INTERROGATION(BoxType.PLUS, BoxType.INTERROGATION),
	FROM_INTERROGATION_TO_PLUS(BoxType.INTERROGATION, BoxType.PLUS),
	FROM_PLUS_TO_VALIDATED(BoxType.PLUS, BoxType.VALIDATED),
	FROM_PLUS_TO_REJECTED(BoxType.PLUS, BoxType.REJECTED),
	FROM_INTERROGATION_TO_REJECTED(BoxType.INTERROGATION, BoxType.REJECTED),
	
	//only for automatic validation
	FROM_PLUS_TO_MINUS(BoxType.PLUS, BoxType.MINUS),
	FROM_INTERROGATION_TO_MINUS(BoxType.INTERROGATION, BoxType.MINUS);
	
	private final BoxType sourceType;
	private final BoxType targetType;
	
	private TransitionType (final BoxType sourceType, final BoxType targetType){
		this.sourceType = sourceType;
		this.targetType = targetType;
	}

	public BoxType getSourceType() {
		return sourceType;
	}
	
	public BoxType getTargetType() {
		return targetType;
	}
	
	
	public static TransitionType get(BoxType sourceType, BoxType targetType){
		if(sourceType.equals(BoxType.PLUS) && targetType.equals(BoxType.INTERROGATION))
			return FROM_PLUS_TO_INTERROGATION;
		else if(sourceType.equals(BoxType.INTERROGATION) && targetType.equals(BoxType.PLUS))
			return FROM_INTERROGATION_TO_PLUS;
		else if(sourceType.equals(BoxType.PLUS) && targetType.equals(BoxType.VALIDATED))
			return FROM_PLUS_TO_VALIDATED;
		else if(sourceType.equals(BoxType.PLUS) && targetType.equals(BoxType.REJECTED))
			return FROM_PLUS_TO_REJECTED;
		else if(sourceType.equals(BoxType.INTERROGATION) && targetType.equals(BoxType.REJECTED))
			return FROM_INTERROGATION_TO_REJECTED;
		
		else if(sourceType.equals(BoxType.PLUS) && targetType.equals(BoxType.MINUS))
			return FROM_PLUS_TO_MINUS;
		else if(sourceType.equals(BoxType.INTERROGATION) && targetType.equals(BoxType.MINUS))
			return FROM_INTERROGATION_TO_MINUS;
					
		else throw new IllegalArgumentException("Transition from " + sourceType + " to " + targetType + " not allowed");
	}

	
	
	
}
