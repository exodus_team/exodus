package it.uniroma2.art.exodus.exodus_validator.engine;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Value;

import it.uniroma2.art.exodus.exodus_validator.structure.Triple;

//CONSIDERARE L'IDEA DI FAR BINDARE SOLO UNA TRIGGING CONDITION ALLA VOLTA
//(CON UN METODO NEXT EVENTUALMENTE), IN MODO DA EVITARE DI CALCOLARE LE 
//TRIGGING CONDITION BINDATE PER TUTTE QUELLE TRIPLE CHE FINIREBBERO IN -BOX
//GIA' CON LE PRIME TRIGGING CONDITION
public class Binder<T extends InfRule> {
	
	private static final Logger logger = LogManager.getLogger(Binder.class);
	
	protected BindingTriple precondition;
	protected Map<Variable, Value> bindings;
	
	protected List<BindingTriple> bindingTriples;
	protected List<BindingTriple> boundTriples = null;
	
	protected Boolean freeVariables = null;
	
	
	
	
	public Binder(Triple triple, BindingTriple precondition){
		
		if(!precondition.bind(triple))
			throw new IllegalArgumentException("Precondition "+precondition+ " does not match with triple "+triple);
		
		this.precondition = precondition;
		
		bindings = new HashMap<>();
		
		BindingTripleElement bindingSubject = precondition.getBindingSubject();
		BindingTripleElement bindingPredicate = precondition.getBindingPredicate();
		BindingTripleElement bindingObject = precondition.getBindingObject();
		
		if(bindingSubject instanceof Variable)
			bindings.put((Variable) bindingSubject, triple.getSubject());
		
		if(bindingPredicate instanceof Variable)
			bindings.put((Variable) bindingPredicate, triple.getPredicate());
		
		if(bindingObject instanceof Variable)
			bindings.put((Variable) bindingObject, triple.getObject());
			
		this.bindingTriples = new ArrayList<>();
		
		this.boundTriples = new ArrayList<>();
	}
	
	
	public void bind(T infRule){
		if(!this.precondition.equals(infRule.getPreconditionBindingTriple()))
			throw new IllegalArgumentException("Consistence rule has to have the same precondition of the binder");
		
		this.bindingTriples = new ArrayList<>(infRule.getTriggingConditions());
		
		this.boundTriples = getBoundTriples(this.bindingTriples, this.bindings);
		
		this.freeVariables = this.boundTriples.stream().anyMatch(boundTriple -> 
		 	(boundTriple.getBindingSubject() instanceof Variable) || 
		 	(boundTriple.getBindingPredicate() instanceof Variable) ||
		 	(boundTriple.getBindingObject() instanceof Variable)
		 );
			
	}
	
	
	public boolean hasFreeVariables(){
		if(this.freeVariables == null)
			throw new RuntimeException("Method cannot be invoked before binding");
		return this.freeVariables;
	}
	
	
	
	public InfEngineOutput getOutputByBindings(Map<Variable,Value> mapper){
		List<Triple> currBoundTriples = this.boundTriples.stream().map(boundTriple -> getTriple(boundTriple, mapper)).collect(Collectors.toList());
		InfEngineOutput output =  new InfEngineOutput(currBoundTriples);
		logger.debug("InfEngineOutput: " + output);
		return output;
	}
	
	
	
	
	
	public List<BindingTriple> getBoundTriples(){
		return new ArrayList<>(this.boundTriples);
	}
	
	
	
	
	
	protected List<BindingTriple> getBoundTriples(List<BindingTriple> bindingTriplesInAND, Map<Variable, Value> bindings){
		List<BindingTriple> boundTriplesInAND = bindingTriplesInAND.stream().map(triple -> getBoundTriple(triple, bindings)).collect(Collectors.toList());
		return boundTriplesInAND;
	}
	
	
	protected BindingTriple getBoundTriple(BindingTriple bindingTriple, Map<Variable, Value> bindings){
		BindingTriple boundedTriple = null;
		if(bindingTriple != null){
			BindingTripleElement boundedTripleSubject = getBoundTripleElement(bindingTriple.getBindingSubject(), bindings);
			BindingTripleElement boundedTriplePredicate = getBoundTripleElement(bindingTriple.getBindingPredicate(), bindings);
			BindingTripleElement boundedTripleObject = getBoundTripleElement(bindingTriple.getBindingObject(), bindings);
			boundedTriple = new BindingTriple(boundedTripleSubject, boundedTriplePredicate, boundedTripleObject);
		}
		return boundedTriple;
	}
	
	
	private BindingTripleElement getBoundTripleElement (BindingTripleElement bindingTripleElement, Map<Variable, Value> bindings){
		BindingTripleElement boundTripleElement = null;
			
			Optional<Entry<Variable, Value>> optional = 
				bindings.entrySet().stream().filter(entry -> {
					Variable variable = entry.getKey();
					return bindingTripleElement.equals(variable);
				}).findFirst();
			
			if(optional.isPresent()){
				Entry<Variable, Value> entry = optional.get();
				Value value = entry.getValue();
				BindingTripleValueFactory factory = BindingTripleValueFactory.getInstance();
				boundTripleElement = factory.createBindingTripleValue(value);
			}
				
			else boundTripleElement = bindingTripleElement;
		
		return boundTripleElement;
	}
	
	
	
	protected Triple getTriple(BindingTriple bindingTriple, Map<Variable, Value> bindings){
		BindingTriple boundTriple = getBoundTriple(bindingTriple, bindings);
		
		BindingTripleElement boundTripleSubject = boundTriple.getBindingSubject();
		BindingTripleElement boundTriplePredicate = boundTriple.getBindingPredicate();
		BindingTripleElement boundTripleObject = boundTriple.getBindingObject();
		
		if(boundTripleSubject instanceof BindingTripleValue && boundTriplePredicate instanceof BindingTripleValue && boundTripleObject instanceof BindingTripleValue){
			BindingTripleValue<Resource> boundTripleValueSubject  = (BindingTripleValue<Resource>) boundTripleSubject;
			BindingTripleValue<IRI> boundTripleValuePredicate  = (BindingTripleValue<IRI>) boundTriplePredicate;
			BindingTripleValue<Value> boundTripleValueObject  = (BindingTripleValue<Value>) boundTripleObject;
			
			return new Triple(boundTripleValueSubject.getRDF4jValue(), boundTripleValuePredicate.getRDF4jValue(), boundTripleValueObject.getRDF4jValue());
		}
		else throw new IllegalArgumentException("Some BindingTripleElement is not a BindingTripleValue: <"+boundTripleSubject+" , "+boundTriplePredicate+ " , "+boundTripleObject+">");
	}
	
	
	
	
	

}
