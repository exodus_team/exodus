package it.uniroma2.art.exodus.exodus_validator.structure;


import java.util.Map.Entry;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Value;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

public class PrefixMapper {
	
	private BiMap<String, String> prefixMap;
	
	
	private static PrefixMapper instance = null;
	
	private PrefixMapper(){
		prefixMap =  HashBiMap.create();
	}
	
	public static PrefixMapper getInstance(){
		if (instance == null)
			instance = new PrefixMapper();
		return instance;
	}
	
	
	
	public void put(String prefix, String alias){
		prefixMap.put(prefix, alias);
	}
	
	public String getPrefixByAlias(String alias){
		Entry<String, String> matchedEntry = prefixMap.entrySet().stream().filter(entry -> entry.getValue().equals(alias)).findFirst().orElse(null);
		if(matchedEntry!=null)
			return matchedEntry.getKey();
		else return null;
	}
	
	public String getAliasByPrefix(String prefix){
		return prefixMap.get(prefix);
	}
	
	
	public CompactTriple map(Triple triple){
		return new CompactTriple(map(triple.getSubject()), map(triple.getPredicate()), map(triple.getObject()));
	}
	
	private String map(Value value){
		if(value instanceof IRI){
			IRI iri = (IRI) value;
			return mapIRIstring(iri.stringValue());
		}
		else if(value instanceof Literal){
			Literal literal = (Literal) value;
			return getLiteralStringValue(literal);
		}
		else throw new IllegalArgumentException("Value "+ value.stringValue() + " not handled");
	}
	
	public String mapIRIstring (String iriString){
		int i;
		for(i = iriString.length() -1 ;  i >= 0 && iriString.charAt(i)!='/' && iriString.charAt(i)!='#' ; i--){
		}
		i++;
		if(i==0)
			return iriString;
		else {
			String searchingPrefix = iriString.substring(0, i);
			String searchedAlias = prefixMap.get(searchingPrefix);
			if(searchedAlias!=null)
				return searchedAlias+":"+ iriString.substring(i);
			else return iriString;
		}
	}
	
	
	public static String getLiteralStringValue(Literal literal){
		String ret = literal.stringValue();
		if(literal.getLanguage().isPresent()){
			ret = "\"" + ret + "\"@" + literal.getLanguage().get();
		}
		return ret;
	}
	
	
	public static void main(String[] args){
		PrefixMapper instance = PrefixMapper.getInstance();
		instance.put("http://www.w3.org/2002/07/owl#", "owl");
		instance.put("http://www.w3.org/TR/2003/PR-owl-guide-20031209/food#", "food");
		instance.put("http://art.uniroma2.it/", "art");
		System.out.println(instance.mapIRIstring("http://art.uniroma2.it/dummyURI"));
		System.out.println(instance.mapIRIstring("http://www.w3.org/TR/2003/PR-owl-guide-20031209/food#Tomato"));
		System.out.println(instance.mapIRIstring("http://art.uniroma3.it/dummyURI"));
		System.out.println(instance.mapIRIstring("sdfghjukolpòàè+"));
	}
	
	
}
