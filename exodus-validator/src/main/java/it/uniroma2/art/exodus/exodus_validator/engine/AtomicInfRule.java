package it.uniroma2.art.exodus.exodus_validator.engine;


public class AtomicInfRule {
	
	private TriggingCondition triggingCondition;
	
	public AtomicInfRule(TriggingCondition triggingCondition) {
		this.triggingCondition = triggingCondition;
	}
	
	public AtomicInfRule (BindingTriple...bindingTriplesInAND){
		TriggingCondition triggingCondition = new TriggingCondition();
		for(BindingTriple bindingTripleInAND : bindingTriplesInAND)
			triggingCondition.addBindingTriple(bindingTripleInAND);
		this.triggingCondition = triggingCondition;
	}
	
	public TriggingCondition getTriggingCondition(){
		return this.triggingCondition;
	}
	
	
	
	
}
