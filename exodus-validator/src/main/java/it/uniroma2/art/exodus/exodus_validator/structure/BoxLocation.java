package it.uniroma2.art.exodus.exodus_validator.structure;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


public class BoxLocation implements Comparable<BoxLocation> {
	private static int nBoxLocations = 0;
	
	private int id;
	private Date time;
	private Triple triple;
	private CompactTriple compactTriple;
	private Constraint dependencyConstraint;
	private Constraint indirectDependencyConstraint;
	private Constraint conflictConstraint;
	private boolean tripleInferred = false;
	private String message = "";
	private BoxType boxType;
	
	private Set<Constraint> pointingDependencyConstraints;
	private Set<Constraint> pointingIndirectDependencyConstraints;
	
	private Set<Constraint> pointingConflictConstraints;
	
	public BoxLocation(Triple triple, Constraint dependencyConstraint, Constraint indirectDependencyConstraint, Constraint conflictConstraint){
		this.triple = triple;
		this.compactTriple = PrefixMapper.getInstance().map(triple);
		this.id = nBoxLocations;
		this.time = new Date();
		this.dependencyConstraint = dependencyConstraint;
		this.indirectDependencyConstraint = indirectDependencyConstraint;
		this.conflictConstraint = conflictConstraint;
		
		pointingDependencyConstraints = new HashSet<>();
		pointingConflictConstraints = new HashSet<>();
		pointingIndirectDependencyConstraints = new HashSet<>();
		
		nBoxLocations++;
	}
	
	public BoxLocation(Triple triple, Constraint dependencyConstraint, Constraint indirectDependencyConstraint, Constraint conflictConstraint, boolean tripleInferred){
		this(triple, dependencyConstraint, indirectDependencyConstraint, conflictConstraint);
		this.tripleInferred = tripleInferred;
	}
	
	
	public BoxLocation(Triple triple, Constraint dependencyConstraint, Constraint indirectDependencyConstraint){
		this(triple, dependencyConstraint, indirectDependencyConstraint, null);
	}
	
	public BoxLocation(Triple triple, Constraint dependencyConstraint, Constraint indirectDependencyConstraint, boolean tripleInferred){
		this(triple, dependencyConstraint, indirectDependencyConstraint);
		this.tripleInferred = tripleInferred;
	}
	
	public BoxLocation(Constraint conflictConstraint, Triple triple){
		this(triple, null, null, conflictConstraint);
	}
	
	public BoxLocation(Constraint conflictConstraint, Triple triple, boolean tripleInferred){
		this(conflictConstraint, triple);
		this.tripleInferred = tripleInferred;
	}
	
	public BoxLocation(Triple triple){
		this(triple, null, null, null);
	}
	
	public BoxLocation(Triple triple, boolean tripleInferred){
		this(triple);
		this.tripleInferred = tripleInferred;
	}
	

	public int getId() {
		return id;
	}

	public Date getTime() {
		return time;
	}

	public Triple getTriple() {
		return triple;
	}

	public Constraint getDependencyConstraint() {
		return dependencyConstraint;
	}
	
	public Constraint getConflictConstraint() {
		return conflictConstraint;
	}
	
	public boolean isTripleInferred(){
		return tripleInferred;
	}
	
	public void setTripleInferred(boolean tripleInferred){
		this.tripleInferred = tripleInferred;
	}
	
	public void setConflictConstraint (Constraint conflictConstraint){
		this.conflictConstraint = conflictConstraint;
	}
	
	public Constraint getIndirectDependencyConstraint() {
		return indirectDependencyConstraint;
	}

	public void setIndirectDependencyConstraint(Constraint indirectDependencyConstraint) {
		this.indirectDependencyConstraint = indirectDependencyConstraint;
	}

	public void setDependencyConstraint (Constraint dependencyConstraint){
		this.dependencyConstraint = dependencyConstraint;
	}
	
	public void addPointingDependencyConstraint(Constraint pointingDependencyConstraint){
		pointingDependencyConstraints.add(pointingDependencyConstraint);
	}
	
	public void addPointingIndirectDependencyConstraint(Constraint pointingDependencyConstraint){
		pointingIndirectDependencyConstraints.add(pointingDependencyConstraint);
	}
	
	public void addPointingConflictConstraint(Constraint pointingConflictConstraint){
		pointingConflictConstraints.add(pointingConflictConstraint);
	}
	
	public void setMessage(String message){
		this.message = message;
	}
	
	public void addMessage(String messageToAdd){
		this.message = this.message + System.lineSeparator() +messageToAdd;
	}
	
	public Iterator<Constraint> getPointingDependencyConstraints(){
		return pointingDependencyConstraints.iterator();
	}
	
	public Iterator<Constraint> getPointingIndirectDependencyConstraints(){
		return pointingIndirectDependencyConstraints.iterator();
	}
	
	public Iterator<Constraint> getPointingConflictConstraints(){
		return pointingConflictConstraints.iterator();
	}
	
	
	public BoxType getBoxType(){
		return boxType;
	}
	
	public void setBoxType(BoxType boxType){
		this.boxType = boxType;
	}
	
	@Override
	public boolean equals(Object other){
		boolean ret = false;
		if(other instanceof BoxLocation){
			BoxLocation otherBoxLocation = (BoxLocation) other;
			ret = this.id==otherBoxLocation.id;
		}
		return ret;
	}

	@Override
	public int compareTo(BoxLocation other) {
		return new Integer(this.id).compareTo(other.id);
	}
	
	@Override
	public int hashCode(){
		return 1000+this.id;
	}
	
	
	@Override
	public String toString(){
		return compactTriple.toString();
	}
	
	
	public String getCompleteInfo(){
		String ret = "{boxLocationId: "+ id + " boxType: " + boxType + " , triple "+ compactTriple.toString()+ " is ";
		if(!tripleInferred)
			ret = ret + "not ";
		ret = ret + "generated}";
		return ret;
	}
	
}
