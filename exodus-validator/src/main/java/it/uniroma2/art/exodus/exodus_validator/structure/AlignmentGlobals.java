package it.uniroma2.art.exodus.exodus_validator.structure;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.rdf4j.model.Resource;

public class AlignmentGlobals {
	
	private static List<RDFvaluePair<Resource>> candidateAlignedClassPairs;
	private static List<RDFvaluePair<Resource>> candidateAlignedIndividualPairs;
	
	static {
		candidateAlignedClassPairs = new ArrayList<RDFvaluePair<Resource>>();
		candidateAlignedIndividualPairs = new ArrayList<RDFvaluePair<Resource>>();
	}
	
	public static List<RDFvaluePair<Resource>> getCandidateAlignedClassPairs(){
		return candidateAlignedClassPairs;
	}
	
	public static List<RDFvaluePair<Resource>> getCandidateAlignedIndividuals(){
		return candidateAlignedIndividualPairs;
	}
	
	public static void addCandidateAlignedClassPair(RDFvaluePair<Resource> classPair){
		candidateAlignedClassPairs.add(classPair);
	}
	
	public static void addCandidateAlignedIndividualPair(RDFvaluePair<Resource> individualPair){
		candidateAlignedIndividualPairs.add(individualPair);
	}

}
