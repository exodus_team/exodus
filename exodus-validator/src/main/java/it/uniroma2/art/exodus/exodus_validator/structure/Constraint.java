package it.uniroma2.art.exodus.exodus_validator.structure;

public class Constraint {
	private BoxLocation source;
	private Expression<BoxLocation> target;
	
	
	public Constraint(BoxLocation source, Expression<BoxLocation> target){
		this.source = source;
		this.target = target;
	}
	 
	public void setSource(BoxLocation source){
		this.source = source;
	}
	
	public BoxLocation getSource(){
		return source;
	}
	
	
	
	public Expression<BoxLocation> getTarget() {
		return target;
	}

	public void setTarget(Expression<BoxLocation> target) {
		this.target = target;
	}

	@Override
	public String toString(){
		return "sourceBoxLocation: "+ source.getId() + " --- targetExpression: "+target ;
	}
	
	@Override
	public boolean equals(Object other){
		boolean ret = false;
		if(other instanceof Constraint){
			Constraint otherConstraint = (Constraint) other;
			if (this.source.equals(otherConstraint.source) && this.target.equals(otherConstraint.target))
				ret = true;
		}
		return ret;
	}
	
	@Override
	public int hashCode(){
		return this.source.hashCode() + 10000*this.target.hashCode();
	}

}
