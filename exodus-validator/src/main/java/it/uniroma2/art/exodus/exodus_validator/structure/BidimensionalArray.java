package it.uniroma2.art.exodus.exodus_validator.structure;

import java.util.ArrayList;

public class BidimensionalArray<T> {
	private ArrayList<ArrayList<T>> listOfLists;
	private int rowSize;
	private int columsSize;
	
	public BidimensionalArray(int rowSize, int columsSize) throws IllegalArgumentException{
		if(rowSize < 0 || columsSize < 0) throw new IllegalArgumentException("rowSize or columnSize smaller than 0");
		
		this.rowSize = rowSize;
		this.columsSize = columsSize;
		listOfLists = new ArrayList<ArrayList<T>>(columsSize);
		for(int i = 0 ; i < columsSize ; i++)
			listOfLists.add(i, new ArrayList<T>(rowSize));

	}
	
	public void ensureCapacity(int rowMinCapacity, int columnMinCapacity){
		listOfLists.ensureCapacity(columnMinCapacity);
		
		if(columnMinCapacity > columsSize){
			for(int i = columsSize ; i < columnMinCapacity ; i++)
				listOfLists.add(i, new ArrayList<T>(Integer.max(rowSize, rowMinCapacity)));
			
			columsSize = Integer.max(columsSize, columnMinCapacity);
		}
		if(rowMinCapacity  >  rowSize){
			for(int i = 0 ; i < columsSize ; i++)
				listOfLists.get(i).ensureCapacity(rowMinCapacity);
			rowSize = rowMinCapacity;
		}
		
	}
	
	
	public T getElement(int row, int column){
		if(row >= rowSize || row < 0 || column >= columsSize || column < 0) 
			throw new IllegalArgumentException("row and/or column have wrong values");
		return listOfLists.get(column).get(row);
	}
	
	
	public void putElement(T element, int row, int column){
		if(row >= rowSize || row < 0 || column >= columsSize || column < 0) 
			throw new IllegalArgumentException("row and/or column have wrong values");
		listOfLists.get(column).add(row, element);
	}
	
	
}
