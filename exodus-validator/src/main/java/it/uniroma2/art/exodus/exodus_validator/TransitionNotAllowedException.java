package it.uniroma2.art.exodus.exodus_validator;

public class TransitionNotAllowedException extends RuntimeException {

	public TransitionNotAllowedException(String message) {
		super(message);
	}
	
	public TransitionNotAllowedException(Exception e) {
		super(e);
	}
	
	public TransitionNotAllowedException(String message, Exception e) {
		super(message, e);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -7980054109133849074L;

}
