package it.uniroma2.art.exodus.exodus_validator;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import it.uniroma2.art.exodus.exodus_validator.engine.InfEngineOutputIterator;
import it.uniroma2.art.exodus.exodus_validator.engine.InfRulesBuilder;
import it.uniroma2.art.exodus.exodus_validator.engine.TripleGenerationInfEngine;
import it.uniroma2.art.exodus.exodus_validator.engine.TripleGenerationInfEngineBinder;
import it.uniroma2.art.exodus.exodus_validator.engine.TripleGenerationInfEngineOutput;
import it.uniroma2.art.exodus.exodus_validator.engine.TripleGenerationInfRule;
import it.uniroma2.art.exodus.exodus_validator.structure.BoxLocation;
import it.uniroma2.art.exodus.exodus_validator.structure.BoxLocationEvaluatorFactory;
import it.uniroma2.art.exodus.exodus_validator.structure.Evaluator;
import it.uniroma2.art.exodus.exodus_validator.structure.Expression;
import it.uniroma2.art.exodus.exodus_validator.structure.MultipleExpression;
import it.uniroma2.art.exodus.exodus_validator.structure.MultipleExpression.Type;
import it.uniroma2.art.exodus.exodus_validator.structure.SingleExpression;
import it.uniroma2.art.exodus.exodus_validator.structure.Triple;
import it.uniroma2.art.exodus.exodus_validator.structure.ValidationHandler;

public class StatementGenerator {
	
	private TripleGenerationInfEngine tripleGenerationEngine;
	private ValidationHandler validationHandler;
	private ConstraintHandler constraintHandler;
	private Validator validator;
	
	private static final Logger logger = LogManager.getLogger(StatementGenerator.class);
	
	public StatementGenerator(TripleGenerationInfEngine tripleGenerationEngine, Validator validator){
		this.tripleGenerationEngine = tripleGenerationEngine;
		this.validationHandler = ValidationHandler.getInstance();
		this.constraintHandler = ConstraintHandler.getInstance();
		this.validator = validator;
	}
	
	
	public List<BoxLocation> generateStatements(BoxLocation boxLocation){
		return generateStatements(boxLocation, 0);
	}
	
	public List<BoxLocation> generateStatements(BoxLocation boxLocation, int inferenceLevel){
		List<BoxLocation> ret = new ArrayList<>();
		
		Triple triple = boxLocation.getTriple();
		InfEngineOutputIterator<TripleGenerationInfRule, TripleGenerationInfEngineBinder> iterator = tripleGenerationEngine.fire(triple);
		
		while(iterator.hasNext()){
			boolean yetPutInPlusOrInterrogationBox = false;
			
			TripleGenerationInfEngineOutput engineOutput = (TripleGenerationInfEngineOutput) iterator.next();
			
			Triple inferredTriple = engineOutput.getGeneratedTriple();
			List<Triple> triplesInAND = engineOutput.getTriplesInAND();
			
			BoxLocation inferredBoxLoc = validationHandler.getPlusOrInterrogationBoxLocation(inferredTriple);
			if(inferredBoxLoc==null){
				inferredBoxLoc = validationHandler.getMinusBoxLocation(inferredTriple);
				if(inferredBoxLoc==null)
					inferredBoxLoc = new BoxLocation(inferredTriple, true);
			}
			else yetPutInPlusOrInterrogationBox = true;
			
			List<Expression<BoxLocation>> boxLocInAnd = triplesInAND.stream().filter(tripleInAND -> validationHandler.getPlusOrInterrogationBoxLocation(tripleInAND) != null).map(tripleInAND -> new SingleExpression<BoxLocation>(validationHandler.getPlusOrInterrogationBoxLocation(tripleInAND))).collect(Collectors.toList());
			Expression<BoxLocation> dependencyConstrExpr;
			if(boxLocInAnd.isEmpty()){
				dependencyConstrExpr = new SingleExpression<>(boxLocation);
			}
			else {
				boxLocInAnd.add(new SingleExpression<>(boxLocation));
				dependencyConstrExpr = new MultipleExpression<>(boxLocInAnd, Type.AND);
			}
			
			logger.debug("Triple inferred: "+ inferredBoxLoc);
			logger.debug("from " + dependencyConstrExpr);
			
			//no devo aggiungerlo se la tripla è già presente in un'altra boxloc non scartata e non coinvolge altre triple primitive (cioè non generate)
			if(!yetPutInPlusOrInterrogationBox || ( inferredBoxLoc.getDependencyConstraint()!=null && !this.constraintHandler.normalizeExpression(dependencyConstrExpr).getAllItems().containsAll(inferredBoxLoc.getIndirectDependencyConstraint().getTarget().getAllItems()))){
				ret.add(inferredBoxLoc);
				logger.debug("Triple inferred " + inferredBoxLoc.getId() + " scheduling for processing" );
				this.constraintHandler.setDependencyConstraint(inferredBoxLoc, dependencyConstrExpr);
				
				validator.validateStatement(inferredBoxLoc, inferenceLevel +1);
			}
			else{
				logger.debug("Triple inferred " + inferredBoxLoc.getId() + " NOT scheduling for processing" );
			}
				
				
			
			
		}
		
		return ret;
		
		
	}
		
	
	
	

}
