package it.uniroma2.art.exodus.exodus_validator.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileArrayProvider {

    public List<String> readLines(String filename) throws IOException {
        FileReader fileReader = new FileReader(filename);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        List<String> lines = new ArrayList<>();
        String line = null;
        while ((line = bufferedReader.readLine()) != null) {
            if(!line.trim().equals(""))
            	lines.add(line.trim());
        }
        bufferedReader.close();
        return lines;
    }
    
    
    public static void main(String[] args)  throws IOException {
    	 FileArrayProvider fap = new FileArrayProvider();
         List<String> lines = fap
                 .readLines("food.rdf");
         for (String line : lines) {
             System.out.println(line);
         }
    }
}