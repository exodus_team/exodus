package it.uniroma2.art.exodus.exodus_validator.engine;

import org.eclipse.rdf4j.model.Value;

public abstract class BindingTripleElement {
	
	@Override
	public boolean equals(Object other){
		boolean ret = false;
		if(other instanceof BindingTripleElement){
			BindingTripleElement otherBindingTripleElement = (BindingTripleElement) other;
			ret = equals(otherBindingTripleElement);
		}
		return ret;
	}
	
	public abstract boolean equals(BindingTripleElement other);
	
	public abstract boolean bind(Value rdf4jValue);
	
	public abstract String toString();
}
