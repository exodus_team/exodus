package it.uniroma2.art.exodus.exodus_validator.structure;

import java.util.Collections;
import java.util.Set;

public class SingleExpression<T> extends Expression<T> {
	private T item;

	public SingleExpression(T item){
		this.item = item;
	}
	
	public T getItem(){
		return item;
	}
	
	@Override
	public boolean evaluate(Evaluator<T> evaluator) {
		return evaluator.evaluate(item);
	}

	@Override
	public boolean equals(Object other) {
		boolean ret = false;
		if(other instanceof SingleExpression){
			SingleExpression<T> otherSingleExpression = (SingleExpression<T>) other;
			if(this.item.equals(otherSingleExpression.item))
				ret = true;
		}
		return ret;
	}

	@Override
	public String toString() {
		return item.toString();
	}

	@Override
	public Set<T> getAllItems() {
		return Collections.singleton(item);
	}
	
}
