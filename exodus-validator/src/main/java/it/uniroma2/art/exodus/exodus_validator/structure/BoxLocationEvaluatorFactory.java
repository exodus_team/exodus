package it.uniroma2.art.exodus.exodus_validator.structure;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiFunction;

public class BoxLocationEvaluatorFactory {
	
	private static Evaluator<BoxLocation> defaultEvaluator;
	
	private BoxLocationEvaluatorFactory(){
		
	}
	
	static{
		defaultEvaluator = new Evaluator<>(
			 boxLoc -> boxLoc.getBoxType().compareTo(BoxType.PLUS) >= 0
			 );
	}
	
	public static Evaluator<BoxLocation> getEvaluator(TransitionType transitionType, RuleType ruleType){
		Evaluator<BoxLocation> ret;
		BoxType sourceBoxType = transitionType.getSourceType();
		BoxType targetBoxType = transitionType.getTargetType();
		if(ruleType.equals(RuleType.INFERRED_TRIPLES_GENERATION)){
			if(targetBoxType.compareTo(sourceBoxType)>0)
				ret = new Evaluator<>(
						boxLoc -> boxLoc.getBoxType().compareTo(targetBoxType) >= 0	
					);
			else 
				ret = new Evaluator<>(
					boxLoc -> boxLoc.getBoxType().compareTo(targetBoxType) > 0	
					);
		}
			
		else if(ruleType.equals(RuleType.CONSISTENCY_CHECKING)){
			ret = defaultEvaluator;
		}
		else throw new IllegalArgumentException("RuleType "+ ruleType + " not handled");
		return ret;
	}
	
	
	
	public static Evaluator<BoxLocation> getEvaluator(Evaluator<BoxLocation> baseEvaluator, Set<BoxLocation> forceEvaluationToTRUE, Set<BoxLocation> forceEvaluationToFALSE){
		return new Evaluator<>(baseEvaluator.getPredicate(), forceEvaluationToTRUE, forceEvaluationToFALSE);
	}
	
	
	public static Evaluator<BoxLocation> getDefaultEvaluator(){
		return defaultEvaluator;
	}
	
}
