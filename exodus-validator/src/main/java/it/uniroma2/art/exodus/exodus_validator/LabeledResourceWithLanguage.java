package it.uniroma2.art.exodus.exodus_validator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.eclipse.rdf4j.model.Resource;

public class LabeledResourceWithLanguage<T extends Resource> {
	
	T resource;
	private Map<String,List<String>> labelsBylanguage;
	
	public LabeledResourceWithLanguage(T resource) {
		this.resource = resource;
		this.labelsBylanguage = new TreeMap<>();
	}
	
	
	public void putLabels(String language, List<String> labels){
		if(labelsBylanguage.containsKey(language))
			labelsBylanguage.put(language, new ArrayList<String>(labels));
		else{
			List<String> oldLabels = labelsBylanguage.get(language);
			List<String> newlabels = new ArrayList<>(oldLabels);
			newlabels.addAll(labels);
			labelsBylanguage.put(language, new ArrayList<String>(newlabels));
		}
	}
	
	public List<String> getLabelsByLanguage(String language){
		List<String> ret = new ArrayList<>();
		ret.addAll(labelsBylanguage.get(language));
		return ret;
	}
	
	public List<String> getAllLabels(){
		List<String> ret = new ArrayList<>();
		for(List<String> currList : labelsBylanguage.values())
			ret.addAll(currList);
		return ret;
	}
	
	

}
