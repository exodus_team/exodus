package it.uniroma2.art.exodus.exodus_validator.engine;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Value;

import it.uniroma2.art.exodus.exodus_validator.structure.Triple;

public class BindingTriple implements Comparable<BindingTriple>{
	
	//for example: <A, rdfs:subClassOf, B>
	private BindingTripleElement bindingSubject;
	private BindingTripleElement bindingPredicate;
	private BindingTripleElement bindingObject;
	
	public BindingTriple(BindingTripleElement bindingSubject, BindingTripleElement bindingPredicate,
			BindingTripleElement bindingObject) {
		this.bindingSubject = bindingSubject;
		this.bindingPredicate = bindingPredicate;
		this.bindingObject = bindingObject;
	}

	public BindingTripleElement getBindingSubject() {
		return bindingSubject;
	}

	public BindingTripleElement getBindingPredicate() {
		return bindingPredicate;
	}

	public BindingTripleElement getBindingObject() {
		return bindingObject;
	}
	
	public boolean bind(Triple triple){
		boolean ret = false;
		Resource subject = triple.getSubject();
		IRI predicate = triple.getPredicate();
		Value object = triple.getObject();
		if(this.bindingSubject.bind(subject) && this.bindingPredicate.bind(predicate) && this.bindingObject.bind(object)){
			ret = true;
			//the same varible cannot match with different rdf4j values
			if( (this.bindingSubject instanceof Variable) && (this.bindingPredicate instanceof Variable)){
				Variable bindingSubjectVariable = (Variable) this.bindingSubject;
				Variable bindingPredicateVariable = (Variable) this.bindingPredicate;
				if(bindingSubjectVariable.equals(bindingPredicateVariable) && !predicate.equals(subject))
					ret = false;
			}
			else if( (this.bindingSubject instanceof Variable) && (this.bindingObject instanceof Variable)){
				Variable bindingSubjectVariable = (Variable) this.bindingSubject;
				Variable bindingObjectVariable = (Variable) this.bindingObject;
				if(bindingSubjectVariable.equals(bindingObjectVariable) && !object.stringValue().equals(subject.stringValue()))
					ret = false;
			}
			else if( (this.bindingObject instanceof Variable) && (this.bindingPredicate instanceof Variable)){
				Variable bindingObjectVariable = (Variable) this.bindingObject;
				Variable bindingPredicateVariable = (Variable) this.bindingPredicate;
				if(bindingObjectVariable.equals(bindingPredicateVariable) && !predicate.equals(object))
					ret = false;
			}
			
		}
		
		return ret;
	}
	
	
	@Override
	public boolean equals(Object other){
		boolean ret = false;
		if(other instanceof BindingTriple){
			BindingTriple otherBindingTriple = (BindingTriple) other;
			if(otherBindingTriple.bindingSubject.equals(this.bindingSubject) && otherBindingTriple.bindingPredicate.equals(this.bindingPredicate) && otherBindingTriple.bindingObject.equals(this.bindingObject))
				ret = true;
		}
		return ret;
	}
	
	@Override
	public int hashCode(){
		return toString().hashCode();
	}
	
	@Override
	public String toString(){
		return "<"+ this.bindingSubject +" , "+this.bindingPredicate + " , "+this.bindingObject + " >";
	}

	@Override
	public int compareTo(BindingTriple other) {
		return this.toString().compareTo(other.toString());
	}
	
	
	
	
	

}
