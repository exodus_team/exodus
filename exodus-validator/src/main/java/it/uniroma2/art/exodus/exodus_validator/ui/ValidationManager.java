package it.uniroma2.art.exodus.exodus_validator.ui;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.eclipse.rdf4j.model.Model;

import it.uniroma2.art.exodus.exodus_validator.OntologyPersistenceManager;
import it.uniroma2.art.exodus.exodus_validator.PropertiesManager;
import it.uniroma2.art.exodus.exodus_validator.Validator;
import it.uniroma2.art.exodus.exodus_validator.structure.BoxLocation;
import it.uniroma2.art.exodus.exodus_validator.structure.Triple;

class ValidationManager {
	
	private static List<Model> graphModels = new ArrayList<>();
	private static Validator validator = null;
	private static OntologyPersistenceManager ontoPersistenceManager ;
	
	private static final Logger logger = LogManager.getLogger(ValidationManager.class);
	
	
	static void init(){
		
		logger.info("Initializing Validator");
		
		long millisStart = new Date().getTime();
		
		PropertiesManager propertiesManager = PropertiesManager.getInstance();
		String inferredOntologyFilepath = propertiesManager.getInferredOntologyFilepath();
		String ontologyFilepath = propertiesManager.getOntologyFilepath();
		String graphDirFilepath = propertiesManager.getGraphDir();
		boolean forceInferredOntoFileGeneration = propertiesManager.forceInferredOntoFileGeneration();
		
		
		ontoPersistenceManager = new OntologyPersistenceManager(ontologyFilepath, inferredOntologyFilepath, graphDirFilepath);
		
		ontoPersistenceManager.initializeModels(forceInferredOntoFileGeneration);
		
		Model inferredOntologyModel = ontoPersistenceManager.getInferredOntologyModel();
		
		validator = new Validator(inferredOntologyModel);
		
		graphModels = ontoPersistenceManager.getGraphModels();
		
		long millisEnd = new Date().getTime();
		
		logger.info("#graph triples: "+ graphModels.stream().mapToInt(model -> model.size()).sum());
		
		logger.info("Validator initialized in " + (int) ( (millisEnd - millisStart)/1000 ) + " seconds");
		
	}
	
	
	
	static void autoValidate(){
		logger.info("Initializing Automatic Validation");
		long millisStart = new Date().getTime();
		
		validator.validate(graphModels);
		
		long millisEnd = new Date().getTime();
		logger.info("Automatic Validation completed in " + (int) ( (millisEnd - millisStart)/1000 ) + " seconds");
	}
	
	
	
	static void persistIntoOntology(List<BoxLocation> boxLocationsToPersist){
		logger.info("Initializing Persisting Phase");
		long millisStart = new Date().getTime();
		
		PropertiesManager propertiesManager = PropertiesManager.getInstance();
		boolean persistGeneratedTriples = propertiesManager.isPersistGeneratedTriples();
		
		String finalOntologyFilepath = propertiesManager.getFinalOntologyFilepath();
		
		ontoPersistenceManager.persistIntoOntologyAsRDF(finalOntologyFilepath, boxLocationsToPersist, persistGeneratedTriples);
		
		long millisEnd = new Date().getTime();
		logger.info("Persisting Phase completed in " + (int) ( (millisEnd - millisStart)/1000 ) + " seconds");
	}
	
	
	
	
	
	

}
