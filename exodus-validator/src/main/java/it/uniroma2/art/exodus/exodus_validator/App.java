package it.uniroma2.art.exodus.exodus_validator;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.StatementImpl;
import org.eclipse.rdf4j.model.impl.ValueFactoryImpl;
import org.eclipse.rdf4j.model.vocabulary.OWL;
import org.eclipse.rdf4j.model.vocabulary.RDF;

import it.uniroma2.art.exodus.exodus_validator.structure.BoxLocation;
import it.uniroma2.art.exodus.exodus_validator.structure.BoxType;
import it.uniroma2.art.exodus.exodus_validator.structure.Constraint;
import it.uniroma2.art.exodus.exodus_validator.structure.Triple;
import it.uniroma2.art.exodus.exodus_validator.structure.ValidationHandler;

public class App {

	 
	public static void main(String[] args) {
		long millisStart = new Date().getTime();
		
		PropertiesManager propertiesManager = PropertiesManager.getInstance();
		String inferredOntologyFilepath = propertiesManager.getInferredOntologyFilepath();
		String ontologyFilepath = propertiesManager.getOntologyFilepath();
		String graphDirFilepath = propertiesManager.getGraphDir();
		boolean forceInferredOntoFileGeneration = propertiesManager.forceInferredOntoFileGeneration();
		
		OntologyPersistenceManager ontoInitializer = new OntologyPersistenceManager(ontologyFilepath, inferredOntologyFilepath, graphDirFilepath);
		
		ontoInitializer.initializeModels(forceInferredOntoFileGeneration);
		
		Model inferredOntologyModel = ontoInitializer.getInferredOntologyModel();
		List<Model> graphModels = ontoInitializer.getGraphModels();
		
		ValidationHandler validationHandler = ValidationHandler.getInstance();
		ValueFactory valueFactory = new ValueFactoryImpl();
		
		
		/*Triple triple1 = new Triple(valueFactory.createIRI("http://art.uniroma2.it/prova/ItalianRegion"),RDF.TYPE,OWL.CLASS );
		BoxLocation boxLocation1 = new BoxLocation(triple1);
		validationHandler.addToPlusBox(boxLocation1);
		
		Triple triple2 = new Triple(valueFactory.createIRI("http://www.w3.org/TR/2003/PR-owl-guide-20031209/wine#Region"),RDF.TYPE,OWL.CLASS );
		BoxLocation boxLocation2 = new BoxLocation(triple2);
		validationHandler.addToPlusBox(boxLocation2);
		
		Triple triple3 = new Triple(valueFactory.createIRI("http://art.uniroma2.it/prova/ItalianRegion"),OWL.DISJOINTWITH,valueFactory.createIRI("http://www.w3.org/TR/2003/PR-owl-guide-20031209/wine#Region") );
		BoxLocation boxLocation3 = new BoxLocation(triple3);
		validationHandler.addToPlusBox(boxLocation3);
		
		Triple triple4 = new Triple(valueFactory.createIRI("http://www.w3.org/TR/2003/PR-owl-guide-20031209/wine#Region"),OWL.DISJOINTWITH,valueFactory.createIRI("http://art.uniroma2.it/prova/ItalianRegion") );
		BoxLocation boxLocation4 = new BoxLocation(triple4);
		validationHandler.addToPlusBox(boxLocation4);*/
		
		Validator validator = new Validator(inferredOntologyModel);
		
		/*BFSOntologyClassExplorer  explorer = validator.getBoxesOntologyExplorer();
		explorer.addTriple(triple1);
		explorer.addTriple(triple2);
		explorer.addTriple(triple3);
		explorer.addTriple(triple4);*/
		
		validator.validate(graphModels);
		
		/*System.out.println("start box filling");
		validationHandler.fillBoxes();
		System.out.println("stop box filling");*/
		
		/*System.out.println("+/?box : ");
		Iterator<BoxLocation> plusOrInterrogationBoxLocations = validationHandler.getPlusOrInterrogationTriplesMapping().values().iterator();
		while(plusOrInterrogationBoxLocations.hasNext()){
			BoxLocation boxLoc = plusOrInterrogationBoxLocations.next();
			System.out.println("Box Location: "+boxLoc);
			System.out.println("Conflict constraint: "+boxLoc.getConflictConstraint()+"");
			System.out.println("Dependency constraint: "+boxLoc.getDependencyConstraint()+"");
			System.out.println("Pointing conflict constraints:");
			Iterator<Constraint> pointingConflictConstrs = boxLoc.getPointingConflictConstraints();
			while(pointingConflictConstrs.hasNext())
				System.out.println("\tPointing Conflict Constraint: "+pointingConflictConstrs.next());
			System.out.println("Pointing dependency constraints:");
			Iterator<Constraint> pointingDependencyConstrs = boxLoc.getPointingDependencyConstraints();
			while(pointingDependencyConstrs.hasNext())
				System.out.println("\tPointing Dep Constraint: "+pointingDependencyConstrs.next());
			System.out.println();
		}
		 
		*/
		
		
		System.out.println("+box : " + validationHandler.getBoxByType(BoxType.PLUS));
		/*Iterator<BoxLocation> plusBoxLocations = validationHandler.getBoxByType(BoxType.PLUS).getBoxLocationIterator();
		while(plusBoxLocations.hasNext()){
			BoxLocation boxLoc = plusBoxLocations.next();
			System.out.println("Box Location: "+boxLoc);
			System.out.println("Conflict constraint: "+boxLoc.getConflictConstraint()+"");
			System.out.println("Dependency constraint: "+boxLoc.getDependencyConstraint()+"");
			System.out.println("Indirect Dependency constraint: "+boxLoc.getIndirectDependencyConstraint()+"");
			System.out.println("Pointing conflict constraints:");
			Iterator<Constraint> pointingConflictConstrs = boxLoc.getPointingConflictConstraints();
			while(pointingConflictConstrs.hasNext())
				System.out.println("\tPointing Conflict Constraint: "+pointingConflictConstrs.next());
			System.out.println("Pointing dependency constraints:");
			Iterator<Constraint> pointingDependencyConstrs = boxLoc.getPointingDependencyConstraints();
			while(pointingDependencyConstrs.hasNext())
				System.out.println("\tPointing Dep Constraint: "+pointingDependencyConstrs.next());*/
			/*System.out.println("Pointing indirect dependency constraints:");
			Iterator<Constraint> pointingIndirectDependencyConstrs = boxLoc.getPointingIndirectDependencyConstraints();
			while(pointingIndirectDependencyConstrs.hasNext())
				System.out.println("\tPointing Indirect Dep Constraint: "+pointingIndirectDependencyConstrs.next());
			System.out.println();
		}*/
			
		System.out.println();
		System.out.println();
		
		System.out.println("?box : " + validationHandler.getBoxByType(BoxType.INTERROGATION));
		/*Iterator<BoxLocation> interrogationBoxLocations = validationHandler.getBoxByType(BoxType.INTERROGATION).getBoxLocationIterator();
		while(interrogationBoxLocations.hasNext()){
			BoxLocation boxLoc = interrogationBoxLocations.next();
			System.out.println("Box Location: "+boxLoc);
			System.out.println("Conflict constraint: "+boxLoc.getConflictConstraint()+"");
			System.out.println("Dependency constraint: "+boxLoc.getDependencyConstraint()+"");
			System.out.println("Indirect Dependency constraint: "+boxLoc.getIndirectDependencyConstraint()+"");
			System.out.println("Pointing conflict constraints:");
			Iterator<Constraint> pointingConflictConstrs = boxLoc.getPointingConflictConstraints();
			while(pointingConflictConstrs.hasNext())
				System.out.println("\tPointing Conflict Constraint: "+pointingConflictConstrs.next());
			System.out.println("Pointing dependency constraints:");
			Iterator<Constraint> pointingDependencyConstrs = boxLoc.getPointingDependencyConstraints();
			while(pointingDependencyConstrs.hasNext())
				System.out.println("\tPointing Dep Constraint: "+pointingDependencyConstrs.next());
			/*System.out.println("Pointing indirect dependency constraints:");
			Iterator<Constraint> pointingIndirectDependencyConstrs = boxLoc.getPointingIndirectDependencyConstraints();
			while(pointingIndirectDependencyConstrs.hasNext())
				System.out.println("\tPointing Indirect Dep Constraint: "+pointingIndirectDependencyConstrs.next());
			System.out.println();
		}*/
			
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		
		
		
		System.out.println("-box : " + validationHandler.getBoxByType(BoxType.MINUS));
		System.out.println();
		
		/*System.out.println("+ Box");
		plusBoxLocations = validationHandler.getBoxByType(BoxType.PLUS).getBoxLocationIterator();
		while(plusBoxLocations.hasNext()){
			BoxLocation boxLoc = plusBoxLocations.next();
			System.out.println(boxLoc.getId());
		}
		
		System.out.println();
		System.out.println("? Box");
		interrogationBoxLocations = validationHandler.getBoxByType(BoxType.INTERROGATION).getBoxLocationIterator();
		while(interrogationBoxLocations.hasNext()){
			BoxLocation boxLoc = interrogationBoxLocations.next();
			System.out.println(boxLoc.getId());
		}
		*/
		//System.out.println("plusOrInterrogationTriplesMapping : "+validationHandler.getPlusOrInterrogationTriplesMapping());
		
		long millisEnd = new Date().getTime();
		
		System.out.println("#seconds: "+ (millisEnd - millisStart)/1000 );
		
	}

}
