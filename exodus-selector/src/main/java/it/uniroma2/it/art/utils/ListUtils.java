package it.uniroma2.it.art.utils;

import java.util.ArrayList;
import java.util.List;

public class ListUtils {
	
	public static <T> List<T> getMergedList(List<T> list, T element){
		List<T> ret = new ArrayList<T>(list);
		ret.add(element);
		return ret;
	}

}
