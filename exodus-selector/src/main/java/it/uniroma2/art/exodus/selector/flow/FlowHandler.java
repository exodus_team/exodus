package it.uniroma2.art.exodus.selector.flow;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.ia.matex.controller.MatexController;
import org.osgi.framework.BundleContext;

import it.uniroma2.art.exodus.exodus_flow.Flow;
import it.uniroma2.art.exodus.selector.GrammarHandler;
import it.uniroma2.art.exodus.selector.Metadata;
import it.uniroma2.art.exodus.selector.MetadataEvaluator;
import it.uniroma2.art.exodus.selector.MetadataExtractor;
import it.uniroma2.art.exodus.selector.SelectorFlowsPropertiesManager;
import it.uniroma2.art.exodus.selector.SelectorPropertiesManager;
import it.uniroma2.art.exodus.selector.expression.Expression;

public class FlowHandler {
	
	
	 private static FlowHandler handler;
	 private Map<String,String> flowsMap;
	 private String outputOntologyDirPath;
	 private String inputFilesDirPath;
	 //private String metadataFilesDirPath;
	  
 
	    private FlowHandler() {
	    	SelectorPropertiesManager selectorProp = SelectorPropertiesManager.getInstance();
	    	SelectorFlowsPropertiesManager selectorFlowsProp = SelectorFlowsPropertiesManager.getInstance();
	    	
	    	flowsMap = selectorFlowsProp.getFlowsMap();
	    	
	    	outputOntologyDirPath = selectorProp.getOutputOntologyDirPath();
	    	if(!outputOntologyDirPath.endsWith(File.separator))
	    		outputOntologyDirPath = outputOntologyDirPath + File.separator;
	    	
	    	//metadataFilesDirPath = ini.getMetadataFilesDirPath();
	    	//if(!metadataFilesDirPath.endsWith(File.separator))
	    	//	metadataFilesDirPath = metadataFilesDirPath + File.separator;
	    	
	    	inputFilesDirPath = selectorProp.getInputFilesDirPath();
	    	if(!inputFilesDirPath.endsWith(File.separator))
	    		inputFilesDirPath = inputFilesDirPath + File.separator;
	    }

	    public static synchronized FlowHandler getInstance() {
	        if (handler == null) {
	            handler = new FlowHandler();
	        }
	        return handler;
	    }
	    
	    
	    
	    
	    public void produceAllOntologies(BundleContext context) throws Exception{
	    	MatexController controller = new MatexController();
	    	
	    	//controller.setPitiaExtractor(new ExodusPitiaExtractor(metadataFilesDirPath, "dummyPath"));
	    	//String matexDirFilepath = metadataFilesDirPath + "matex"+File.separator;
	    	//controller.setPathToSave(matexDirFilepath);
	    	//new File(matexDirFilepath).mkdir();
	    	
	    	File inputDir = new File (inputFilesDirPath);
	    	List<File> listFileToAnalyze = new ArrayList<>(FileUtils.listFiles(inputDir, TrueFileFilter.TRUE, TrueFileFilter.TRUE));
//	    	listFileToAnalyze = FileUtils.listf(inputDir,listFileToAnalyze);
	    	
	    	Set<String> handledFlowClassNames = flowsMap.keySet();
	    	
	    	//FlowService flowService = FlowService.getInstance();
	    	FlowService flowService = new FlowService(context);
	    	
	    	MetadataExtractor extractor = new MetadataExtractor();
	    	
	    	for(int i=0;i<listFileToAnalyze.size();i++) {
    	        File input = listFileToAnalyze.get(i);
    	        controller.extractFromFile(input);
    	        File xmlFile = controller.saveAsXML();
    	        
    	        Metadata metadata = extractor.extractMetadata(xmlFile.getAbsolutePath());
				System.out.println("Autore: " + metadata.getAuthor());
				System.out.println("Language: " + metadata.getLanguage());
				System.out.println("Resource Name: " + metadata.getResourceName());
				System.out.println("Type: " + metadata.getType());
				System.out.println("Data Modifica: " + metadata.getModifiedDate());
				System.out.println("Data Salvataggio: " + metadata.getSaveDate());
				System.out.println();
				
				MetadataEvaluator metadataEvaluator = new MetadataEvaluator(metadata);
				
				Iterator<Flow> flowsIt = flowService.getAvailableFlows();
				

				while (flowsIt.hasNext()) {
					Flow flow = flowsIt.next();
					//System.out.println(flow.toString());
					
					for(String flowClassName : handledFlowClassNames){
						//Class<?> clazz;
						//clazz = Class.forName(flowClassName);
						//if(flow.getClass().equals(clazz)){
						if(flow.getClass().getName().equals(flowClassName)){
							String exprString = flowsMap.get(flowClassName);
							Expression triggerExpr = GrammarHandler.getExpressionByString(exprString);
							if(metadataEvaluator.evaluate(triggerExpr)){
								System.out.println("triggered expression "+ triggerExpr.toString() + " for this file");
								File outputOntologyFile = new File(outputOntologyDirPath + flowClassName + "_" + input.getName() + ".rdf");
								flow.produceOntology(input,outputOntologyFile);
							}
								
						}
						
					}
					
					
					
				}
				
				System.out.println();
				System.out.println();
    	 }
   
	    	
	}
	    
	  
	
	
	    
	    
//	public static void main(String[] args){
//		try {
//			FlowHandler.getInstance().produceAllOntologies();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
	
	

}
