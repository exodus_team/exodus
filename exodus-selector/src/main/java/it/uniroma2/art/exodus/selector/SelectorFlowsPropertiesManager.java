package it.uniroma2.art.exodus.selector;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;


public class SelectorFlowsPropertiesManager {
	
public static String PROPERTIES_FILE = "selector-flows.properties";
	
	private static SelectorFlowsPropertiesManager instance = null;
	
	private static Map<String, String> flowsMap;
	

	private SelectorFlowsPropertiesManager(){
		init();
	}
	
	
	public static SelectorFlowsPropertiesManager getInstance(){
		if (instance == null)
			instance = new SelectorFlowsPropertiesManager();
		return instance;
	}
	
	
	
	private static void init(){
		
		
		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream(PROPERTIES_FILE);

			// load a properties file
			prop.load(input);
			

			flowsMap = new HashMap<String,String>();
			
			Set<Object> keys = prop.keySet();
			for(Object key : keys){
				Object value = prop.get(key);
				flowsMap.put(key.toString(), value.toString());
			}
			

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	  
		
	}


	
	public Map<String, String> getFlowsMap() {
		return flowsMap;
	}
	
	

}
