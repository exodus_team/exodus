package it.uniroma2.art.exodus.selector.expression;

public interface Expression {
	
	public String toString();
	
	//public boolean evaluate();
}
