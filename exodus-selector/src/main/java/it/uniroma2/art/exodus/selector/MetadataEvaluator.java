package it.uniroma2.art.exodus.selector;

import java.util.Map;
import java.util.Set;

import it.uniroma2.art.exodus.selector.expression.AndExpression;
import it.uniroma2.art.exodus.selector.expression.ComparisonOperator;
import it.uniroma2.art.exodus.selector.expression.Expression;
import it.uniroma2.art.exodus.selector.expression.NotExpression;
import it.uniroma2.art.exodus.selector.expression.OrExpression;
import it.uniroma2.art.exodus.selector.expression.SimpleExpression;

public class MetadataEvaluator {
	
	//Map<SimpleExpression, Boolean> evaluationMap;
	private Metadata metadata;
	private Map<String,Comparable<?>> metadataMap;
	
	public MetadataEvaluator(Metadata metadata){
		//populateEvaluationMap(expression);
		this.metadata = metadata;
		this.metadataMap = metadata.getValuesMap();
	}
	

	
	public boolean evaluate (Expression expression){
		boolean ret;
		
		if (expression instanceof SimpleExpression<?>){
			SimpleExpression<?> simpleExp = (SimpleExpression<?>) expression; 
			ret = evaluateSimpleExpression(simpleExp);
		}
		
		else if (expression instanceof OrExpression){
			ret = false;
			OrExpression orExpression = (OrExpression) expression;
			for(Expression exp : orExpression.getExpressionList())
				if (evaluate(exp)== true){
					ret = true;
					break;
				}
		}
		
		else if (expression instanceof AndExpression){
			ret = true;
			AndExpression andExpression = (AndExpression) expression;
			for(Expression exp : andExpression.getExpressionList())
				if (evaluate(exp)== false){
					ret = false;
					break;
				}
		}
		
		else if (expression instanceof NotExpression){
			NotExpression notExpression = (NotExpression) expression;
			Expression exp = notExpression.getBaseExpression();
			ret = !evaluate(exp);
		}
		
		else throw new IllegalArgumentException(expression.getClass() + " not handled");
		
		return ret;
		
	}
	
	
	
	private <T extends Comparable<T>> boolean evaluateSimpleExpression(SimpleExpression<T> simpleExpr){
		String key = simpleExpr.getKey();
		T value = simpleExpr.getValue();
		ComparisonOperator comparisonOperator = simpleExpr.getComparisonOperator();
		
		boolean ret = true;
		
		Comparable<?> metadataValue = metadataMap.get(key);
		
		
		if(metadataValue!=null){
			
			Comparable<T> parametrizedMetadataValue = (Comparable<T>) metadataValue;
			int compare = Integer.MIN_VALUE;
			try{
				compare = parametrizedMetadataValue.compareTo(value);
			}
			catch(Exception e){
				ret = false;
				e.printStackTrace();
			}
			
			
			if(ret){
				
				if(comparisonOperator.equals(ComparisonOperator.CONTAINS))
					ret = parametrizedMetadataValue.toString().contains(value.toString());
				
				else if(comparisonOperator.equals(ComparisonOperator.SUBSTRING_OF))
					ret = value.toString().contains(parametrizedMetadataValue.toString());
				
				else if(comparisonOperator.equals(ComparisonOperator.EQUALS))
					ret = compare==0;
				
				else if(comparisonOperator.equals(ComparisonOperator.DIFFERENT_FROM))
					ret = compare!=0;
				
				else if(comparisonOperator.equals(ComparisonOperator.GRATER_THEN))
					ret = compare ==1;
				
				else if(comparisonOperator.equals(ComparisonOperator.LESS_THEN))
					ret = compare ==-1;
				
				else if(comparisonOperator.equals(ComparisonOperator.GRATER_EQUALS))
					ret = compare == 1 || compare == 0;
				
				else if(comparisonOperator.equals(ComparisonOperator.LESS_EQUALS))
					ret = compare == -1 || compare == 0;
				
				
				else throw new IllegalArgumentException("Comparison Operator "+ comparisonOperator + " not handled");
				
			}
			

		}
		
		
		else ret = false;
		
		
		return ret;
		
		
	}
	
	
	
	
	

	

}
