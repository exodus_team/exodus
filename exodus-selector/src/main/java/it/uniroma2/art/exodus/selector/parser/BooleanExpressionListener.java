// Generated from BooleanExpression.g4 by ANTLR 4.6
package it.uniroma2.art.exodus.selector.parser;

  import it.uniroma2.art.exodus.selector.expression.*;
  import java.time.LocalDateTime;
  import java.time.format.DateTimeFormatter;

import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link BooleanExpressionParser}.
 */
public interface BooleanExpressionListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link BooleanExpressionParser#parse}.
	 * @param ctx the parse tree
	 */
	void enterParse(BooleanExpressionParser.ParseContext ctx);
	/**
	 * Exit a parse tree produced by {@link BooleanExpressionParser#parse}.
	 * @param ctx the parse tree
	 */
	void exitParse(BooleanExpressionParser.ParseContext ctx);
	/**
	 * Enter a parse tree produced by {@link BooleanExpressionParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(BooleanExpressionParser.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link BooleanExpressionParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(BooleanExpressionParser.ExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link BooleanExpressionParser#andExpression}.
	 * @param ctx the parse tree
	 */
	void enterAndExpression(BooleanExpressionParser.AndExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link BooleanExpressionParser#andExpression}.
	 * @param ctx the parse tree
	 */
	void exitAndExpression(BooleanExpressionParser.AndExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link BooleanExpressionParser#completedAndExpression}.
	 * @param ctx the parse tree
	 */
	void enterCompletedAndExpression(BooleanExpressionParser.CompletedAndExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link BooleanExpressionParser#completedAndExpression}.
	 * @param ctx the parse tree
	 */
	void exitCompletedAndExpression(BooleanExpressionParser.CompletedAndExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link BooleanExpressionParser#orExpression}.
	 * @param ctx the parse tree
	 */
	void enterOrExpression(BooleanExpressionParser.OrExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link BooleanExpressionParser#orExpression}.
	 * @param ctx the parse tree
	 */
	void exitOrExpression(BooleanExpressionParser.OrExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link BooleanExpressionParser#completedOrExpression}.
	 * @param ctx the parse tree
	 */
	void enterCompletedOrExpression(BooleanExpressionParser.CompletedOrExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link BooleanExpressionParser#completedOrExpression}.
	 * @param ctx the parse tree
	 */
	void exitCompletedOrExpression(BooleanExpressionParser.CompletedOrExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link BooleanExpressionParser#simpleExpression}.
	 * @param ctx the parse tree
	 */
	void enterSimpleExpression(BooleanExpressionParser.SimpleExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link BooleanExpressionParser#simpleExpression}.
	 * @param ctx the parse tree
	 */
	void exitSimpleExpression(BooleanExpressionParser.SimpleExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link BooleanExpressionParser#comparator}.
	 * @param ctx the parse tree
	 */
	void enterComparator(BooleanExpressionParser.ComparatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link BooleanExpressionParser#comparator}.
	 * @param ctx the parse tree
	 */
	void exitComparator(BooleanExpressionParser.ComparatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link BooleanExpressionParser#binary}.
	 * @param ctx the parse tree
	 */
	void enterBinary(BooleanExpressionParser.BinaryContext ctx);
	/**
	 * Exit a parse tree produced by {@link BooleanExpressionParser#binary}.
	 * @param ctx the parse tree
	 */
	void exitBinary(BooleanExpressionParser.BinaryContext ctx);
	/**
	 * Enter a parse tree produced by {@link BooleanExpressionParser#date}.
	 * @param ctx the parse tree
	 */
	void enterDate(BooleanExpressionParser.DateContext ctx);
	/**
	 * Exit a parse tree produced by {@link BooleanExpressionParser#date}.
	 * @param ctx the parse tree
	 */
	void exitDate(BooleanExpressionParser.DateContext ctx);
}