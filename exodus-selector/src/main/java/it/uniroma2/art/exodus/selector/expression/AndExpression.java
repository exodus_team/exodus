package it.uniroma2.art.exodus.selector.expression;

import java.util.List;


public class AndExpression extends ComposedExpression {
	
	
	public AndExpression(List<Expression> expressionList) {
		super(expressionList);
		booleanOperator = BooleanOperator.AND;
	}
	
	/*
	@Override
	public boolean evaluate(){
		boolean ret = true;
		for(Expression exp : expressionList)
			if (exp.evaluate()== false){
				ret = false;
				break;
			}
		return ret;
	}*/
	
	

}
