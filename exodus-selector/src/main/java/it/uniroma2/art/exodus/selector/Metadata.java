package it.uniroma2.art.exodus.selector;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;


public class Metadata {
	
	private String language;
	private String type;
	private String resourceName;
	
	private String author;
	private LocalDateTime saveDate;
	private LocalDateTime modifiedDate;
	
	
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getResourceName() {
		return resourceName;
	}
	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public LocalDateTime getSaveDate() {
		return saveDate;
	}
	public void setSaveDate(LocalDateTime saveDate) {
		this.saveDate = saveDate;
	}
	public LocalDateTime getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(LocalDateTime modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	
	
	public Map<String,Comparable<?>> getValuesMap(){
		Map<String,Comparable<?>> map = new HashMap<String,Comparable<?>>();
		
		if(language != null){
			map.put("LANGUAGE", language);
		}
		
		if(type != null){
			map.put("TYPE", type);
		}
		
		if(resourceName != null){
			map.put("RESOURCENAME", resourceName);
		}
		
		if(author != null){
			map.put("AUTHOR", author);
		}
		
		if(saveDate != null){
			map.put("SAVEDATE", saveDate);
		}
		
		if(modifiedDate != null){
			map.put("MODIFIEDDATE", modifiedDate);
		}
		
		return map;
	}
	
	
	
	
	
	

}
