package it.uniroma2.art.exodus.selector;

/**
 * Hello world!
 *
 */
public class App {
	
	private static FlowTriggerCondition[] ftcArray;
	/*
	static{
		FlowTriggerCondition ftc1 = new FlowTriggerCondition(new Flow(), "(language = it or language = ro)");
		FlowTriggerCondition ftc2 = new FlowTriggerCondition(new Flow(), "language contains it");
		FlowTriggerCondition ftc3 = new FlowTriggerCondition(new Flow(), "(!type CONTAINS pdf AND author substring of lorEnzo)");
		//FlowTriggerCondition ftc3 = new FlowTriggerCondition(new Flow(), "modifydate > 2016-01-22T20:15:46Z");
		//
		FlowTriggerCondition ftc4 = new FlowTriggerCondition(new Flow(), "(resourceName = vocabolario.xls)");
		FlowTriggerCondition ftc5 = new FlowTriggerCondition(new Flow(), "(type contains /zip or resourceName = &dummy)");
		FlowTriggerCondition ftc6 = new FlowTriggerCondition(new Flow(), "modifydate > 3456");
		
		//FlowTriggerCondition ftcDate = new FlowTriggerCondition(new Flow(), "modifydate > @2016/01/22");
	
		ftcArray = new FlowTriggerCondition[]{ftc1, ftc2, ftc3, ftc4, ftc5, ftc6};
	}
    
    public static void main(String args[]) throws ResourceInitializationException, IOException, SAXException,
        ParserConfigurationException, AnalysisEngineProcessException, TransformerException {
    
    	
    	
    	MatexController controller = new MatexController();
    	controller.setPathToSave("matex\\");
    	
    	ArrayList<File> listFileToAnalyze = new ArrayList<File>();
    	File inputDir = new File ("C:\\Users\\Andrea\\Desktop\\progetti con sorgenti\\");
    	listFileToAnalyze = FileUtils.listf(inputDir,listFileToAnalyze);
    	
    	MetadataExtractor extractor = new MetadataExtractor();
    	 for(int i=0;i<listFileToAnalyze.size();i++) {
    	        File input = listFileToAnalyze.get(i);
    	        controller.extractFromFile(input);
    	        File xmlFile = controller.saveAsXML();
    	        try {
					Metadata metadata = extractor.extractMetadata(xmlFile.getAbsolutePath());
					System.out.println("Autore: " + metadata.getAuthor());
					System.out.println("Language: " + metadata.getLanguage());
					System.out.println("Resource Name: " + metadata.getResourceName());
					System.out.println("Type: " + metadata.getType());
					System.out.println("Data Modifica: " + metadata.getModifiedDate());
					System.out.println("Data Salvataggio: " + metadata.getSaveDate());
					System.out.println();
					
					MetadataEvaluator metadataEvaluator = new MetadataEvaluator(metadata);
					
					for(int j = 0 ; j < ftcArray.length ; j++){
						FlowTriggerCondition ftcCurr = ftcArray[j];
						String exprString = ftcCurr.getExpression();
						Expression triggerExpr = GrammarHandler.getExpressionByString(exprString);
						if(metadataEvaluator.evaluate(triggerExpr))
							System.out.println("triggered expression "+ triggerExpr.toString() + " for this file");
						
					}
					
					System.out.println();
					System.out.println();
				} catch (JDOMException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    	 }
   

	   


    }
    
    
    */
}
