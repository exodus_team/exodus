package it.uniroma2.art.exodus.selector.expression;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public abstract class ComposedExpression  implements Expression {
	
	protected BooleanOperator booleanOperator;
	
	protected List<Expression> expressionList; 
	
	public ComposedExpression(List<Expression> expressionList){
		this.expressionList = new ArrayList<Expression>(expressionList);
	}

	public List<Expression> getExpressionList() {
		return expressionList;
	}
	
	public String toString(){
		List<Expression> expressionList = getExpressionList();
		String ret = "( ";
		ret = ret + expressionList.stream().map((Expression exp) -> String.join(" ", booleanOperator.name(),  exp.toString(), " "))
			    .collect(Collectors.joining());
		
		ret = ret + " )";
		
		ret = ret.replaceFirst(" " + booleanOperator.name() + " " , " ");
		return ret;
	}
	
	
	

}
