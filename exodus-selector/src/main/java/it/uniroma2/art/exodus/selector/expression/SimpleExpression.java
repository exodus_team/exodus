package it.uniroma2.art.exodus.selector.expression;



public class SimpleExpression<T extends Comparable<T>> implements Expression {
	
	private String key;
	private T value;
	private ComparisonOperator comparisonOperator;
	
	//private Boolean booleanExpressionValue;
	
	public SimpleExpression(String key, T value, ComparisonOperator comparisonOperator) {
		super();
		this.key = key;
		this.value = value;
		this.comparisonOperator = comparisonOperator;
	}

	public String getKey() {
		return key;
	}

	public T getValue() {
		return value;
	}
	
	public ComparisonOperator getComparisonOperator() {
		return comparisonOperator;
	}
	
	@Override
	public String toString(){
		String ret = key + " " + comparisonOperator + " " + value;
		return ret;
	}
	
	/*@Override
	public boolean evaluate() throws NullPointerException{
		if (booleanExpressionValue != null)
			return booleanExpressionValue;
		else throw new NullPointerException("Boolean Expression Value Not Set");
	}
	
	public void setBooleanExpressionValue(boolean booleanExpressionValue){
		this.booleanExpressionValue = booleanExpressionValue;
	}*/
	
	
	@Override
	public boolean equals(Object other){
		boolean ret = false;
		if(other instanceof SimpleExpression){
			SimpleExpression<T> otherSimpleExpression = (SimpleExpression<T>) other;
			if(this.key.equals(otherSimpleExpression.key) && this.value.toString().equals(otherSimpleExpression.value.toString()))
				ret = true;
		}
		return ret;
	}
	
	@Override
	public int hashCode(){
		return 1000*this.key.hashCode() + this.value.hashCode();
	}




	
	
	
	
	
	
}
