package it.uniroma2.art.exodus.selector;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;

import org.ini4j.Config;
import org.ini4j.Ini;


public class INIManager {
	
	public static String INI_FILE = "selector.INI";
	
	private static INIManager instance = null;
	
	private static Map<String, String> flowsMap;
	
	private static String inputFilesDirPath;
	//private static String metadataFilesDirPath;
	private static String flowExpressionsDirPath;
	
	

	private INIManager(){
		init();
	}
	
	
	public static INIManager getInstance(){
		if (instance == null)
			instance = new INIManager();
		return instance;
	}
	
	
	
	private static void init(){
		
		Config.getGlobal().setEscape(false);
		
		
        Ini ini = new Ini();

        try {
        	ini.load(new InputStreamReader(new FileInputStream(INI_FILE), "ISO-8859-1"));
			
			flowsMap = ini.get("Flows");
			
			Ini.Section locationsSection = ini.get("Locations");
			
			inputFilesDirPath = locationsSection.get("InputFilesDirPath").trim();
			//metadataFilesDirPath = locationsSection.get("MetadataFilesDirPath").trim();
			flowExpressionsDirPath = locationsSection.get("FlowExpressionsDirPath").trim();
			
			
			
			
		} catch (IOException e) {
			e.printStackTrace();
		} 
		
	}


	public Map<String, String> getFlowsMap() {
		return flowsMap;
	}
	
	public String getInputFilesDirPath(){
		return inputFilesDirPath;
	}
	
	/*public String getMetadataFilesDirPath(){
		return metadataFilesDirPath;
	}*/
	
	public String getFlowExpressionsDirPath(){
		return flowExpressionsDirPath;
	}


	

	
	
	
}
