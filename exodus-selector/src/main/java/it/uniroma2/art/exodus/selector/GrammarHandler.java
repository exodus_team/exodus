package it.uniroma2.art.exodus.selector;


import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;

import it.uniroma2.art.exodus.selector.expression.Expression;
import it.uniroma2.art.exodus.selector.parser.BooleanExpressionLexer;
import it.uniroma2.art.exodus.selector.parser.BooleanExpressionParser;
import it.uniroma2.art.exodus.selector.parser.BooleanExpressionParser.ParseContext;


public class GrammarHandler {
	
	public static Expression getExpressionByString(String exprString){
		CharStream in = new ANTLRInputStream(exprString);
		BooleanExpressionLexer lexer = new BooleanExpressionLexer(in);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        BooleanExpressionParser parser = new BooleanExpressionParser(tokens);
        ParseContext context = parser.parse();
        Expression ret = context.exp;
        return ret;
	}
	
	public static void main(String[] args) throws Exception {
        //ANTLRStringStream in = new ANTLRStringStream("12*(5-6)");
      
		//CharStream in = new ANTLRInputStream("(Campo1=valore1 AND Campo2<valore2)");
		//CharStream in = new ANTLRInputStream("modifydate > @2016/01/22");
		CharStream in = new ANTLRInputStream("modifydate >= @2016 /01/22");
		
		//String content = StandardCharsets.readFile("test.txt", StandardCharsets.UTF_8);
		
        BooleanExpressionLexer lexer = new BooleanExpressionLexer(in);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        BooleanExpressionParser parser = new BooleanExpressionParser(tokens);
        ParseContext context = parser.parse();
        System.out.println(context.exp); // print the value*/
        //ComparisonOperator.valueOf(arg0)
    }
	
	
	
	
}
