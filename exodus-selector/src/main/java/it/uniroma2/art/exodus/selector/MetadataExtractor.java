package it.uniroma2.art.exodus.selector;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.filter.Filters;
import org.jdom2.input.SAXBuilder;
import org.jdom2.xpath.XPathExpression;
import org.jdom2.xpath.XPathFactory;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


/*
 * Raffinare la parte di memorizzazione dei metadati nella clase Metadata (ad esempio, potrei volere il solo formato senza la parte "application/")
   e decidere se ritornare un solo oggetto di tipo Metadata alla volta oppure tenermi la metadataList (che eventualmente svuoto ogni volta che viene chiamato un metodo che ne "scarica" il contenuto)
 */
public class MetadataExtractor {
	
	/*private List<Metadata> metadataList;
	
	
	public MetadataExtractor(){
		metadataList = new ArrayList<Metadata>();
	}*/
	
	public MetadataExtractor(){
		
	}
	
	
	public Metadata extractMetadata(String xmlSource) throws JDOMException, IOException{
		Metadata metadata = new Metadata();
		
		SAXBuilder jdomBuilder = new SAXBuilder();
		  
        Document jdomDocument = jdomBuilder.build(xmlSource);
  
        Element metadataNode = jdomDocument.getRootElement().getChild("metadata");
        
        Element pitiaNode = metadataNode.getChild("pitia");
        Element tikaNode = metadataNode.getChild("tika");
        
        XPathFactory xFactory = XPathFactory.instance();
        
        XPathExpression<Element> expr = xFactory.compile("//meta[@name='language']", Filters.element());
        List<Element> languageNodeList = expr.evaluate(pitiaNode);
        Element languageNode; 
        if (!languageNodeList.isEmpty()){
        	languageNode = languageNodeList.get(0);
        	String language = languageNode.getText();
        	metadata.setLanguage(language);
        }
        
        
        expr = xFactory.compile("//meta[@name='type']", Filters.element());
        List<Element> typeNodeList = expr.evaluate(pitiaNode);
        Element typeNode; 
        if (!typeNodeList.isEmpty()){
        	typeNode = typeNodeList.get(0);
        	String type = typeNode.getText();
        	metadata.setType(type);
        }
        
        
        expr = xFactory.compile("//meta[@name='resourceName']", Filters.element());
        List<Element> resourceNameNodeList = expr.evaluate(pitiaNode);
        Element resourceNameNode; 
        if (!resourceNameNodeList.isEmpty()){
        	resourceNameNode = resourceNameNodeList.get(0);
        	String resourceName = resourceNameNode.getText();
        	metadata.setResourceName(resourceName);
        }
        
        
        
        
        
        expr = xFactory.compile("//meta[@name='Last-Author']", Filters.element());
        List<Element> lastAuthorNodeList = expr.evaluate(tikaNode);
        Element lastAuthorNode; 
        if (!lastAuthorNodeList.isEmpty()){
        	lastAuthorNode = lastAuthorNodeList.get(0);
        	String lastAuthor = lastAuthorNode.getText();
        	metadata.setAuthor(lastAuthor);
        }
        
        
        
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");  
        
        
        expr = xFactory.compile("//meta[@name='meta:save-date']", Filters.element());
        List<Element> saveDateNodeList = expr.evaluate(tikaNode);
        Element saveDateNode; 
        if (!saveDateNodeList.isEmpty()){
        	saveDateNode = saveDateNodeList.get(0);
        	String saveDateText = saveDateNode.getText();
        	LocalDateTime saveDate = LocalDateTime.parse(saveDateText, formatter);
        	metadata.setSaveDate(saveDate);
        }
        
        
        
        
        expr = xFactory.compile("//meta[@name='modified']", Filters.element());
        List<Element> modifiedNodeList = expr.evaluate(tikaNode);
        Element modifiedNode; 
        if (!modifiedNodeList.isEmpty()){
        	modifiedNode = modifiedNodeList.get(0);
        	String modifiedDateText = modifiedNode.getText();
        	LocalDateTime modifiedDate = LocalDateTime.parse(modifiedDateText, formatter);
        	metadata.setModifiedDate(modifiedDate);
        }
        
        	
        
        
        
        
        
        return metadata;
	}
	
	

}
