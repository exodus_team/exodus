package it.uniroma2.art.exodus.selector.flow;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;

import it.uniroma2.art.exodus.exodus_flow.Flow;

public class FlowService {
	
	 private static FlowService service;
	 private List<Flow> flowList;

	 public FlowService(BundleContext context) {
		 flowList = new ArrayList<Flow>();
		try {
			generateFlowList(context);
		} catch (Exception e) {
			e.printStackTrace();
		}
	 }

	 /*public static synchronized FlowServiceTemp getInstance() {
	     if (service == null) {
	         service = new FlowServiceTemp();
	     }
	     return service;
	 }*/


	 private void generateFlowList(BundleContext context) throws InvalidSyntaxException{
		 ServiceReference[] allServicerefs = context.getAllServiceReferences(Flow.class.getName(), null);
			for(int i = 0 ; i < allServicerefs.length ; i++){
				ServiceReference<Flow> sr = allServicerefs[i];
				Flow flow = (Flow) context.getService(sr);
				flowList.add(flow);
			}
	 }
	    
	 public Iterator<Flow> getAvailableFlows(){
		 return flowList.iterator();
	 }
	    
	    

}
