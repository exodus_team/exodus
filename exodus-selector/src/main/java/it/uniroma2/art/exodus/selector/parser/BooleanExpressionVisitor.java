// Generated from BooleanExpression.g4 by ANTLR 4.6
package it.uniroma2.art.exodus.selector.parser;

  import it.uniroma2.art.exodus.selector.expression.*;
  import java.time.LocalDateTime;
  import java.time.format.DateTimeFormatter;

import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link BooleanExpressionParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface BooleanExpressionVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link BooleanExpressionParser#parse}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParse(BooleanExpressionParser.ParseContext ctx);
	/**
	 * Visit a parse tree produced by {@link BooleanExpressionParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpression(BooleanExpressionParser.ExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link BooleanExpressionParser#andExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAndExpression(BooleanExpressionParser.AndExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link BooleanExpressionParser#completedAndExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCompletedAndExpression(BooleanExpressionParser.CompletedAndExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link BooleanExpressionParser#orExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOrExpression(BooleanExpressionParser.OrExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link BooleanExpressionParser#completedOrExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCompletedOrExpression(BooleanExpressionParser.CompletedOrExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link BooleanExpressionParser#simpleExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSimpleExpression(BooleanExpressionParser.SimpleExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link BooleanExpressionParser#comparator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComparator(BooleanExpressionParser.ComparatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link BooleanExpressionParser#binary}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBinary(BooleanExpressionParser.BinaryContext ctx);
	/**
	 * Visit a parse tree produced by {@link BooleanExpressionParser#date}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDate(BooleanExpressionParser.DateContext ctx);
}