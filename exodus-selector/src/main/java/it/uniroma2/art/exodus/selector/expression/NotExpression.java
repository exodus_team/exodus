package it.uniroma2.art.exodus.selector.expression;



public class NotExpression implements Expression{

	private static BooleanOperator booleanOp = BooleanOperator.NOT;
	
	private Expression baseExpression;
	
	public NotExpression(Expression baseExpression) {
		this.baseExpression = baseExpression;
	}
	
	public String toString(){
		String ret = booleanOp + " " + baseExpression.toString();
		return ret;
	}
	
	public Expression getBaseExpression(){
		return this.baseExpression;
	}
	
	
	
}
