package it.uniroma2.art.exodus.selector.activator;

import java.lang.reflect.InvocationTargetException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import it.uniroma2.art.exodus.selector.flow.FlowHandler;


public class SelectorActivator implements BundleActivator
{
   

	private FlowHandler flowHandler;
	private JFrame jframe;


	public void start(BundleContext context)
    {
    	flowHandler = FlowHandler.getInstance();
    	try {
			jframe = new JFrame();
			JPanel jpanel = new JPanel();
			JButton jbutton = new JButton();
			jbutton.setText("Produce all ontologies");
			jbutton.addActionListener(e -> {
				try {
					flowHandler.produceAllOntologies(context);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			});
			jpanel.add(jbutton);
			jframe.add(jpanel);
			jframe.pack();
			jframe.setLocationRelativeTo(null);
			jframe.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
 
 
    public void stop(BundleContext context)
    {
    	try {
			SwingUtilities.invokeAndWait(() -> {
				jframe.setVisible(false);
				jframe.dispose();
				jframe = null;
			});
		} catch (InvocationTargetException | InterruptedException e) {
			e.printStackTrace();
		}
    }

   
}
