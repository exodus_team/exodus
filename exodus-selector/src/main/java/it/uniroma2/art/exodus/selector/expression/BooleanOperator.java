package it.uniroma2.art.exodus.selector.expression;

public enum BooleanOperator {
	AND, OR, NOT
}
