package it.uniroma2.art.exodus.selector;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;



public class SelectorPropertiesManager {
	
	
	
	public static String PROPERTIES_FILE = "selector.properties";
	
	private static SelectorPropertiesManager instance = null;
	
	private static String inputFilesDirPath;
	private static String outputOntologyDirPath;
	

	private SelectorPropertiesManager(){
		init();
	}
	
	
	public static SelectorPropertiesManager getInstance(){
		if (instance == null)
			instance = new SelectorPropertiesManager();
		return instance;
	}
	
	
	
	private static void init(){
		
		
		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream(PROPERTIES_FILE);

			// load a properties file
			prop.load(input);
			

			// get the property value and print it out
			inputFilesDirPath = prop.getProperty("InputFilesDirPath");
			outputOntologyDirPath = prop.getProperty("OutputOntologyDirPath");
			

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	  
		
	}


	public String getInputFilesDirPath() {
		return inputFilesDirPath;
	}
	
	
	public String getOutputOntologyDirPath(){
		return outputOntologyDirPath;
	}
	
	
	


}