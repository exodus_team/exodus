package it.uniroma2.art.exodus.selector.expression;

import java.util.List;

public class OrExpression extends ComposedExpression {
	

	public OrExpression(List<Expression> expressionList) {
		super(expressionList);
		booleanOperator = BooleanOperator.OR;
	}
	
	/*
	@Override
	public boolean evaluate(){
		boolean ret = false;
		for(Expression exp : expressionList)
			if (exp.evaluate()== true){
				ret = true;
				break;
			}
		return ret;
	}
	 */
}
