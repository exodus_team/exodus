// Generated from BooleanExpression.g4 by ANTLR 4.6
package it.uniroma2.art.exodus.selector.parser;

  import java.time.LocalDateTime;
  import java.time.format.DateTimeFormatter;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;

import it.uniroma2.art.exodus.selector.expression.AndExpression;
import it.uniroma2.art.exodus.selector.expression.ComparisonOperator;
import it.uniroma2.art.exodus.selector.expression.Expression;
import it.uniroma2.art.exodus.selector.expression.NotExpression;
import it.uniroma2.art.exodus.selector.expression.OrExpression;
import it.uniroma2.art.exodus.selector.expression.SimpleExpression;

import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class BooleanExpressionParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.6", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		AND=1, OR=2, DF=3, GT=4, GE=5, LT=6, LE=7, EQ=8, NOT=9, CONTAINS=10, SUBSTRING_OF=11, 
		LPAREN=12, RPAREN=13, SLASH=14, DECIMAL=15, YEAR=16, MONTH=17, DAY=18, 
		AT=19, STRING=20, WS=21;
	public static final int
		RULE_parse = 0, RULE_expression = 1, RULE_andExpression = 2, RULE_completedAndExpression = 3, 
		RULE_orExpression = 4, RULE_completedOrExpression = 5, RULE_simpleExpression = 6, 
		RULE_comparator = 7, RULE_binary = 8, RULE_date = 9;
	public static final String[] ruleNames = {
		"parse", "expression", "andExpression", "completedAndExpression", "orExpression", 
		"completedOrExpression", "simpleExpression", "comparator", "binary", "date"
	};

	private static final String[] _LITERAL_NAMES = {
		null, null, null, null, "'>'", "'>='", "'<'", "'<='", "'='", null, null, 
		null, "'('", "')'", "'/'", null, null, null, null, "'@'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "AND", "OR", "DF", "GT", "GE", "LT", "LE", "EQ", "NOT", "CONTAINS", 
		"SUBSTRING_OF", "LPAREN", "RPAREN", "SLASH", "DECIMAL", "YEAR", "MONTH", 
		"DAY", "AT", "STRING", "WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "BooleanExpression.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public BooleanExpressionParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ParseContext extends ParserRuleContext {
		public Expression exp;
		public ExpressionContext expression;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode EOF() { return getToken(BooleanExpressionParser.EOF, 0); }
		public ParseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parse; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BooleanExpressionListener ) ((BooleanExpressionListener)listener).enterParse(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BooleanExpressionListener ) ((BooleanExpressionListener)listener).exitParse(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BooleanExpressionVisitor ) return ((BooleanExpressionVisitor<? extends T>)visitor).visitParse(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParseContext parse() throws RecognitionException {
		ParseContext _localctx = new ParseContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_parse);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(20);
			((ParseContext)_localctx).expression = expression();
			setState(21);
			match(EOF);
			((ParseContext)_localctx).exp =  ((ParseContext)_localctx).expression.exp; 
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public Expression exp;
		public CompletedAndExpressionContext completedAndExpression;
		public CompletedOrExpressionContext completedOrExpression;
		public ExpressionContext expression;
		public SimpleExpressionContext simpleExpression;
		public CompletedAndExpressionContext completedAndExpression() {
			return getRuleContext(CompletedAndExpressionContext.class,0);
		}
		public CompletedOrExpressionContext completedOrExpression() {
			return getRuleContext(CompletedOrExpressionContext.class,0);
		}
		public TerminalNode NOT() { return getToken(BooleanExpressionParser.NOT, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public SimpleExpressionContext simpleExpression() {
			return getRuleContext(SimpleExpressionContext.class,0);
		}
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BooleanExpressionListener ) ((BooleanExpressionListener)listener).enterExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BooleanExpressionListener ) ((BooleanExpressionListener)listener).exitExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BooleanExpressionVisitor ) return ((BooleanExpressionVisitor<? extends T>)visitor).visitExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		ExpressionContext _localctx = new ExpressionContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_expression);
		try {
			setState(37);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,0,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(24);
				((ExpressionContext)_localctx).completedAndExpression = completedAndExpression();
				((ExpressionContext)_localctx).exp =  ((ExpressionContext)_localctx).completedAndExpression.exp; 
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(27);
				((ExpressionContext)_localctx).completedOrExpression = completedOrExpression();
				((ExpressionContext)_localctx).exp =  ((ExpressionContext)_localctx).completedOrExpression.exp; 
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(30);
				match(NOT);
				setState(31);
				((ExpressionContext)_localctx).expression = expression();
				((ExpressionContext)_localctx).exp =  new NotExpression(((ExpressionContext)_localctx).expression.exp); 
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(34);
				((ExpressionContext)_localctx).simpleExpression = simpleExpression();
				((ExpressionContext)_localctx).exp =  ((ExpressionContext)_localctx).simpleExpression.exp; 
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AndExpressionContext extends ParserRuleContext {
		public AndExpression exp;
		public ExpressionContext expression;
		public AndExpressionContext andExpression;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode AND() { return getToken(BooleanExpressionParser.AND, 0); }
		public AndExpressionContext andExpression() {
			return getRuleContext(AndExpressionContext.class,0);
		}
		public AndExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_andExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BooleanExpressionListener ) ((BooleanExpressionListener)listener).enterAndExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BooleanExpressionListener ) ((BooleanExpressionListener)listener).exitAndExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BooleanExpressionVisitor ) return ((BooleanExpressionVisitor<? extends T>)visitor).visitAndExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AndExpressionContext andExpression() throws RecognitionException {
		AndExpressionContext _localctx = new AndExpressionContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_andExpression);
		try {
			setState(47);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(39);
				((AndExpressionContext)_localctx).expression = expression();
				setState(40);
				match(AND);
				setState(41);
				((AndExpressionContext)_localctx).andExpression = andExpression();
				ArrayList<Expression> list = new ArrayList<Expression>(((AndExpressionContext)_localctx).andExpression.exp.getExpressionList()); list.add(((AndExpressionContext)_localctx).expression.exp); ((AndExpressionContext)_localctx).exp =  new AndExpression(list); 
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(44);
				((AndExpressionContext)_localctx).expression = expression();
				ArrayList<Expression> list = new ArrayList<Expression>(); list.add(((AndExpressionContext)_localctx).expression.exp); ((AndExpressionContext)_localctx).exp =  new AndExpression(list); 
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CompletedAndExpressionContext extends ParserRuleContext {
		public AndExpression exp;
		public AndExpressionContext andExpression;
		public TerminalNode LPAREN() { return getToken(BooleanExpressionParser.LPAREN, 0); }
		public AndExpressionContext andExpression() {
			return getRuleContext(AndExpressionContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(BooleanExpressionParser.RPAREN, 0); }
		public CompletedAndExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_completedAndExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BooleanExpressionListener ) ((BooleanExpressionListener)listener).enterCompletedAndExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BooleanExpressionListener ) ((BooleanExpressionListener)listener).exitCompletedAndExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BooleanExpressionVisitor ) return ((BooleanExpressionVisitor<? extends T>)visitor).visitCompletedAndExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CompletedAndExpressionContext completedAndExpression() throws RecognitionException {
		CompletedAndExpressionContext _localctx = new CompletedAndExpressionContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_completedAndExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(49);
			match(LPAREN);
			setState(50);
			((CompletedAndExpressionContext)_localctx).andExpression = andExpression();
			setState(51);
			match(RPAREN);
			((CompletedAndExpressionContext)_localctx).exp =  ((CompletedAndExpressionContext)_localctx).andExpression.exp; 
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OrExpressionContext extends ParserRuleContext {
		public OrExpression exp;
		public ExpressionContext expression;
		public OrExpressionContext orExpression;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode OR() { return getToken(BooleanExpressionParser.OR, 0); }
		public OrExpressionContext orExpression() {
			return getRuleContext(OrExpressionContext.class,0);
		}
		public OrExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_orExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BooleanExpressionListener ) ((BooleanExpressionListener)listener).enterOrExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BooleanExpressionListener ) ((BooleanExpressionListener)listener).exitOrExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BooleanExpressionVisitor ) return ((BooleanExpressionVisitor<? extends T>)visitor).visitOrExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OrExpressionContext orExpression() throws RecognitionException {
		OrExpressionContext _localctx = new OrExpressionContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_orExpression);
		try {
			setState(62);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(54);
				((OrExpressionContext)_localctx).expression = expression();
				setState(55);
				match(OR);
				setState(56);
				((OrExpressionContext)_localctx).orExpression = orExpression();
				ArrayList<Expression> list = new ArrayList<Expression>(((OrExpressionContext)_localctx).orExpression.exp.getExpressionList()); list.add(((OrExpressionContext)_localctx).expression.exp); ((OrExpressionContext)_localctx).exp =  new OrExpression(list); 
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(59);
				((OrExpressionContext)_localctx).expression = expression();
				ArrayList<Expression> list = new ArrayList<Expression>(); list.add(((OrExpressionContext)_localctx).expression.exp); ((OrExpressionContext)_localctx).exp =  new OrExpression(list); 
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CompletedOrExpressionContext extends ParserRuleContext {
		public OrExpression exp;
		public OrExpressionContext orExpression;
		public TerminalNode LPAREN() { return getToken(BooleanExpressionParser.LPAREN, 0); }
		public OrExpressionContext orExpression() {
			return getRuleContext(OrExpressionContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(BooleanExpressionParser.RPAREN, 0); }
		public CompletedOrExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_completedOrExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BooleanExpressionListener ) ((BooleanExpressionListener)listener).enterCompletedOrExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BooleanExpressionListener ) ((BooleanExpressionListener)listener).exitCompletedOrExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BooleanExpressionVisitor ) return ((BooleanExpressionVisitor<? extends T>)visitor).visitCompletedOrExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CompletedOrExpressionContext completedOrExpression() throws RecognitionException {
		CompletedOrExpressionContext _localctx = new CompletedOrExpressionContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_completedOrExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(64);
			match(LPAREN);
			setState(65);
			((CompletedOrExpressionContext)_localctx).orExpression = orExpression();
			setState(66);
			match(RPAREN);
			((CompletedOrExpressionContext)_localctx).exp =  ((CompletedOrExpressionContext)_localctx).orExpression.exp; 
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SimpleExpressionContext extends ParserRuleContext {
		public SimpleExpression exp;
		public Token key;
		public ComparatorContext comparator;
		public Token value;
		public DateContext val;
		public ComparatorContext comparator() {
			return getRuleContext(ComparatorContext.class,0);
		}
		public List<TerminalNode> STRING() { return getTokens(BooleanExpressionParser.STRING); }
		public TerminalNode STRING(int i) {
			return getToken(BooleanExpressionParser.STRING, i);
		}
		public DateContext date() {
			return getRuleContext(DateContext.class,0);
		}
		public TerminalNode DECIMAL() { return getToken(BooleanExpressionParser.DECIMAL, 0); }
		public SimpleExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_simpleExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BooleanExpressionListener ) ((BooleanExpressionListener)listener).enterSimpleExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BooleanExpressionListener ) ((BooleanExpressionListener)listener).exitSimpleExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BooleanExpressionVisitor ) return ((BooleanExpressionVisitor<? extends T>)visitor).visitSimpleExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SimpleExpressionContext simpleExpression() throws RecognitionException {
		SimpleExpressionContext _localctx = new SimpleExpressionContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_simpleExpression);
		try {
			setState(84);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,3,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(69);
				((SimpleExpressionContext)_localctx).key = match(STRING);
				setState(70);
				((SimpleExpressionContext)_localctx).comparator = comparator();
				setState(71);
				((SimpleExpressionContext)_localctx).value = match(STRING);
				((SimpleExpressionContext)_localctx).exp =  new SimpleExpression((((SimpleExpressionContext)_localctx).key!=null?((SimpleExpressionContext)_localctx).key.getText():null).toUpperCase(),(((SimpleExpressionContext)_localctx).value!=null?((SimpleExpressionContext)_localctx).value.getText():null),((SimpleExpressionContext)_localctx).comparator.compOp);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(74);
				((SimpleExpressionContext)_localctx).key = match(STRING);
				setState(75);
				((SimpleExpressionContext)_localctx).comparator = comparator();
				setState(76);
				((SimpleExpressionContext)_localctx).val = date();
				((SimpleExpressionContext)_localctx).exp =  new SimpleExpression((((SimpleExpressionContext)_localctx).key!=null?((SimpleExpressionContext)_localctx).key.getText():null).toUpperCase(),LocalDateTime.parse(((SimpleExpressionContext)_localctx).val.str.replaceFirst("@","") +"T00:00:00Z", DateTimeFormatter.ofPattern("yyyy/MM/dd'T'HH:mm:ss'Z'")) ,((SimpleExpressionContext)_localctx).comparator.compOp); 
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(79);
				((SimpleExpressionContext)_localctx).key = match(STRING);
				setState(80);
				((SimpleExpressionContext)_localctx).comparator = comparator();
				setState(81);
				((SimpleExpressionContext)_localctx).value = match(DECIMAL);
				((SimpleExpressionContext)_localctx).exp =  new SimpleExpression((((SimpleExpressionContext)_localctx).key!=null?((SimpleExpressionContext)_localctx).key.getText():null).toUpperCase(), Double.parseDouble((((SimpleExpressionContext)_localctx).value!=null?((SimpleExpressionContext)_localctx).value.getText():null)),((SimpleExpressionContext)_localctx).comparator.compOp);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ComparatorContext extends ParserRuleContext {
		public ComparisonOperator compOp;
		public TerminalNode GT() { return getToken(BooleanExpressionParser.GT, 0); }
		public TerminalNode GE() { return getToken(BooleanExpressionParser.GE, 0); }
		public TerminalNode LT() { return getToken(BooleanExpressionParser.LT, 0); }
		public TerminalNode LE() { return getToken(BooleanExpressionParser.LE, 0); }
		public TerminalNode EQ() { return getToken(BooleanExpressionParser.EQ, 0); }
		public TerminalNode DF() { return getToken(BooleanExpressionParser.DF, 0); }
		public TerminalNode CONTAINS() { return getToken(BooleanExpressionParser.CONTAINS, 0); }
		public TerminalNode SUBSTRING_OF() { return getToken(BooleanExpressionParser.SUBSTRING_OF, 0); }
		public ComparatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_comparator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BooleanExpressionListener ) ((BooleanExpressionListener)listener).enterComparator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BooleanExpressionListener ) ((BooleanExpressionListener)listener).exitComparator(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BooleanExpressionVisitor ) return ((BooleanExpressionVisitor<? extends T>)visitor).visitComparator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ComparatorContext comparator() throws RecognitionException {
		ComparatorContext _localctx = new ComparatorContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_comparator);
		try {
			setState(102);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case GT:
				enterOuterAlt(_localctx, 1);
				{
				setState(86);
				match(GT);
				((ComparatorContext)_localctx).compOp =  ComparisonOperator.valueOf("GRATER_THEN"); 
				}
				break;
			case GE:
				enterOuterAlt(_localctx, 2);
				{
				setState(88);
				match(GE);
				((ComparatorContext)_localctx).compOp =  ComparisonOperator.valueOf("GRATER_EQUALS"); 
				}
				break;
			case LT:
				enterOuterAlt(_localctx, 3);
				{
				setState(90);
				match(LT);
				((ComparatorContext)_localctx).compOp =  ComparisonOperator.valueOf("LESS_THEN"); 
				}
				break;
			case LE:
				enterOuterAlt(_localctx, 4);
				{
				setState(92);
				match(LE);
				((ComparatorContext)_localctx).compOp =  ComparisonOperator.valueOf("LESS_EQUALS"); 
				}
				break;
			case EQ:
				enterOuterAlt(_localctx, 5);
				{
				setState(94);
				match(EQ);
				((ComparatorContext)_localctx).compOp =  ComparisonOperator.valueOf("EQUALS"); 
				}
				break;
			case DF:
				enterOuterAlt(_localctx, 6);
				{
				setState(96);
				match(DF);
				((ComparatorContext)_localctx).compOp =  ComparisonOperator.valueOf("DIFFERENT_FROM"); 
				}
				break;
			case CONTAINS:
				enterOuterAlt(_localctx, 7);
				{
				setState(98);
				match(CONTAINS);
				((ComparatorContext)_localctx).compOp =  ComparisonOperator.valueOf("CONTAINS"); 
				}
				break;
			case SUBSTRING_OF:
				enterOuterAlt(_localctx, 8);
				{
				setState(100);
				match(SUBSTRING_OF);
				((ComparatorContext)_localctx).compOp =  ComparisonOperator.valueOf("SUBSTRING_OF"); 
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BinaryContext extends ParserRuleContext {
		public TerminalNode AND() { return getToken(BooleanExpressionParser.AND, 0); }
		public TerminalNode OR() { return getToken(BooleanExpressionParser.OR, 0); }
		public BinaryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_binary; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BooleanExpressionListener ) ((BooleanExpressionListener)listener).enterBinary(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BooleanExpressionListener ) ((BooleanExpressionListener)listener).exitBinary(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BooleanExpressionVisitor ) return ((BooleanExpressionVisitor<? extends T>)visitor).visitBinary(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BinaryContext binary() throws RecognitionException {
		BinaryContext _localctx = new BinaryContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_binary);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(104);
			_la = _input.LA(1);
			if ( !(_la==AND || _la==OR) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DateContext extends ParserRuleContext {
		public String str;
		public Token at;
		public Token year;
		public Token sl1;
		public Token month;
		public Token sl2;
		public Token day;
		public TerminalNode AT() { return getToken(BooleanExpressionParser.AT, 0); }
		public TerminalNode YEAR() { return getToken(BooleanExpressionParser.YEAR, 0); }
		public List<TerminalNode> SLASH() { return getTokens(BooleanExpressionParser.SLASH); }
		public TerminalNode SLASH(int i) {
			return getToken(BooleanExpressionParser.SLASH, i);
		}
		public TerminalNode MONTH() { return getToken(BooleanExpressionParser.MONTH, 0); }
		public TerminalNode DAY() { return getToken(BooleanExpressionParser.DAY, 0); }
		public DateContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_date; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BooleanExpressionListener ) ((BooleanExpressionListener)listener).enterDate(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BooleanExpressionListener ) ((BooleanExpressionListener)listener).exitDate(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BooleanExpressionVisitor ) return ((BooleanExpressionVisitor<? extends T>)visitor).visitDate(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DateContext date() throws RecognitionException {
		DateContext _localctx = new DateContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_date);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(106);
			((DateContext)_localctx).at = match(AT);
			setState(107);
			((DateContext)_localctx).year = match(YEAR);
			setState(108);
			((DateContext)_localctx).sl1 = match(SLASH);
			setState(109);
			((DateContext)_localctx).month = match(MONTH);
			setState(110);
			((DateContext)_localctx).sl2 = match(SLASH);
			setState(111);
			((DateContext)_localctx).day = match(DAY);
			((DateContext)_localctx).str =  (((DateContext)_localctx).at!=null?((DateContext)_localctx).at.getText():null) + (((DateContext)_localctx).year!=null?((DateContext)_localctx).year.getText():null) + (((DateContext)_localctx).sl1!=null?((DateContext)_localctx).sl1.getText():null) + (((DateContext)_localctx).month!=null?((DateContext)_localctx).month.getText():null) + (((DateContext)_localctx).sl2!=null?((DateContext)_localctx).sl2.getText():null) + (((DateContext)_localctx).day!=null?((DateContext)_localctx).day.getText():null); System.out.println("Date: "+_input.getText(_localctx.start, _input.LT(-1)));
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\27u\4\2\t\2\4\3\t"+
		"\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\3"+
		"\2\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3"+
		"(\n\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\5\4\62\n\4\3\5\3\5\3\5\3\5\3\5\3"+
		"\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\5\6A\n\6\3\7\3\7\3\7\3\7\3\7\3\b\3\b\3"+
		"\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\5\bW\n\b\3\t\3\t\3"+
		"\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\5\ti\n\t\3\n\3"+
		"\n\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\2\2\f\2\4\6\b\n\f\16\20"+
		"\22\24\2\3\3\2\3\4x\2\26\3\2\2\2\4\'\3\2\2\2\6\61\3\2\2\2\b\63\3\2\2\2"+
		"\n@\3\2\2\2\fB\3\2\2\2\16V\3\2\2\2\20h\3\2\2\2\22j\3\2\2\2\24l\3\2\2\2"+
		"\26\27\5\4\3\2\27\30\7\2\2\3\30\31\b\2\1\2\31\3\3\2\2\2\32\33\5\b\5\2"+
		"\33\34\b\3\1\2\34(\3\2\2\2\35\36\5\f\7\2\36\37\b\3\1\2\37(\3\2\2\2 !\7"+
		"\13\2\2!\"\5\4\3\2\"#\b\3\1\2#(\3\2\2\2$%\5\16\b\2%&\b\3\1\2&(\3\2\2\2"+
		"\'\32\3\2\2\2\'\35\3\2\2\2\' \3\2\2\2\'$\3\2\2\2(\5\3\2\2\2)*\5\4\3\2"+
		"*+\7\3\2\2+,\5\6\4\2,-\b\4\1\2-\62\3\2\2\2./\5\4\3\2/\60\b\4\1\2\60\62"+
		"\3\2\2\2\61)\3\2\2\2\61.\3\2\2\2\62\7\3\2\2\2\63\64\7\16\2\2\64\65\5\6"+
		"\4\2\65\66\7\17\2\2\66\67\b\5\1\2\67\t\3\2\2\289\5\4\3\29:\7\4\2\2:;\5"+
		"\n\6\2;<\b\6\1\2<A\3\2\2\2=>\5\4\3\2>?\b\6\1\2?A\3\2\2\2@8\3\2\2\2@=\3"+
		"\2\2\2A\13\3\2\2\2BC\7\16\2\2CD\5\n\6\2DE\7\17\2\2EF\b\7\1\2F\r\3\2\2"+
		"\2GH\7\26\2\2HI\5\20\t\2IJ\7\26\2\2JK\b\b\1\2KW\3\2\2\2LM\7\26\2\2MN\5"+
		"\20\t\2NO\5\24\13\2OP\b\b\1\2PW\3\2\2\2QR\7\26\2\2RS\5\20\t\2ST\7\21\2"+
		"\2TU\b\b\1\2UW\3\2\2\2VG\3\2\2\2VL\3\2\2\2VQ\3\2\2\2W\17\3\2\2\2XY\7\6"+
		"\2\2Yi\b\t\1\2Z[\7\7\2\2[i\b\t\1\2\\]\7\b\2\2]i\b\t\1\2^_\7\t\2\2_i\b"+
		"\t\1\2`a\7\n\2\2ai\b\t\1\2bc\7\5\2\2ci\b\t\1\2de\7\f\2\2ei\b\t\1\2fg\7"+
		"\r\2\2gi\b\t\1\2hX\3\2\2\2hZ\3\2\2\2h\\\3\2\2\2h^\3\2\2\2h`\3\2\2\2hb"+
		"\3\2\2\2hd\3\2\2\2hf\3\2\2\2i\21\3\2\2\2jk\t\2\2\2k\23\3\2\2\2lm\7\25"+
		"\2\2mn\7\22\2\2no\7\20\2\2op\7\23\2\2pq\7\20\2\2qr\7\24\2\2rs\b\13\1\2"+
		"s\25\3\2\2\2\7\'\61@Vh";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}