package it.uniroma2.art.exodus.selector.expression;

public enum ComparisonOperator {
	GRATER_THEN, LESS_THEN, EQUALS, DIFFERENT_FROM, GRATER_EQUALS, LESS_EQUALS, CONTAINS, SUBSTRING_OF
}
