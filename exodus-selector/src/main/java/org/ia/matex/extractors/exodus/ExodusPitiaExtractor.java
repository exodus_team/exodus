package org.ia.matex.extractors.exodus;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageNode;
import org.apache.pdfbox.util.PDFImageWriter;
import org.apache.tika.config.TikaConfig;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.DigestingParser;
import org.apache.tika.parser.utils.CommonsDigester;
import org.ia.matex.extractors.Extractor;
import org.ia.matex.extractors.PitiaExtractor;
import org.ia.matex.extractors.TikaExtractor;
import org.ia.matex.metadata.PitiaMetadata;
import org.ia.matex.metadata.PitiaMetadataInterface;
import org.ia.matex.utils.FileUtils;
import org.ia.matex.utils.XMLOutput;

//creata per ovviare al fatto che Matex mette "output\" davanti al path dei file XML creati senza possibilità di cambiarlo
public class ExodusPitiaExtractor extends PitiaExtractor{

	 /**
     * Path to save XML output
     */
    private String pathToSave;

    /**
     * Custom Metadata
     */
    private PitiaMetadata customMetadata;

    /**
     * Extractor that uses Apache Tika
     */
    private TikaExtractor tikaExtractor;
    /**
     * Name of resource in input
     */
    private String inputName;

    /**
     * State of force use of OCR
     */
    private boolean forceOCR = false;

    public boolean isForceOCR() {
        return forceOCR;
    }

    public void setForceOCR(boolean forceOCR) {
        this.forceOCR = forceOCR;
    }

    /**
     * Constructor without parameters
     */
    public ExodusPitiaExtractor(String pathToSave, String tesseractPath) {

    	super(pathToSave, tesseractPath);
    	
        this.setExtractedInfo(new String(""));
        this.metadata = new PitiaMetadata();
        this.customMetadata = new PitiaMetadata();
        this.pathToSave = pathToSave;
        final TikaConfig finalConfig = TikaConfig.getDefaultConfig();
        this.tikaExtractor = new TikaExtractor(new DigestingParser(
                new AutoDetectParser(finalConfig),
                new CommonsDigester(20*1024*1024,
                        CommonsDigester.DigestAlgorithm.MD5,
                        CommonsDigester.DigestAlgorithm.SHA256)), Extractor.OUT_TEXT, tesseractPath);
        if(tesseractPath.compareToIgnoreCase("nopath")!=0)
            this.forceOCR = true;
    }

    /**
     * This method in this extractor is empty because the input may be file or URL
     * @param input stream in input
     */
    @Override
    protected void parse(InputStream input) {

    }

    /**
     * Parse an input file.
     * @see Extractor#parse(InputStream)
     * @param input file in input
     */
    public void parseFile(File input) {

        long startTime = System.currentTimeMillis();

        //call TikaExtractor
        this.tikaExtractor.openFile(input);

        //merge Tika's metadata with Pitia's metadata, it even delete the duplicates.
        this.metadata.mergeMetadata(tikaExtractor.getMetadata());

        this.inputName = this.metadata.get(PitiaMetadataInterface.RESOURCE_NAME);

        //if it is a pdf file and don't extract content and it isn't empty, turn into images and recall parse,
        //Tika will use Tesseract OCR if it on the machine
        if(isForceOCR()){
            if(this.metadata.get("type").compareTo("application/pdf")==0) {
                if (this.tikaExtractor.getExtractedInfo().trim().compareTo("") == 0) {
                    String text = "";
                    TikaExtractor tikaEx = new TikaExtractor(Extractor.OUT_TEXT,"");
                    System.out.println("devi farlo!");
                    File[] fList = this.pdf2Images(input).listFiles();
                    for (File file : fList) {
                        tikaEx.openFile(file);
                        text += tikaEx.getExtractedInfo()+"\n";
                    }
                    tikaEx = null;
                    this.tikaExtractor.setExtractedInfo(text);
                }
            }
        }

        this.extractedInfo = this.tikaExtractor.getExtractedInfo();
        long endTime   = System.currentTimeMillis();
        long totalTime = endTime - startTime;
        System.out.println("Parse time: " + totalTime + " ms");
    }

    /**
     * Parse a file represented from URL
     * @see Extractor#parse(InputStream)
     * @param input URL in input
     */
    public void parseURL(URL input) {

        long startTime = System.currentTimeMillis();

        //call TikaExtractor
        this.tikaExtractor.openURL(input);

        //merge Tika's metadata with Pitia's metadata, it even delete the duplicates.
        this.metadata.mergeMetadata(tikaExtractor.getMetadata());

        this.inputName = this.metadata.get(PitiaMetadataInterface.RESOURCE_NAME);

        //if it is a pdf file and don't extract content and it isn't empty, turn into images and recall parse,
        //Tika will use Tesseract OCR if it on the machine
        if(isForceOCR()){
            if(this.metadata.get("type").compareTo("application/pdf")==0) {
                if (this.tikaExtractor.getExtractedInfo().trim().compareTo("") == 0) {
                    String text = "";
                    TikaExtractor tikaEx = new TikaExtractor(Extractor.OUT_TEXT,"");
                    System.out.println("devi farlo!");
                    File[] fList = this.pdf2Images(input).listFiles();
                    for (File file : fList) {
                        tikaEx.openFile(file);
                        text += tikaEx.getExtractedInfo()+"\n";
                    }
                    tikaEx = null;
                    this.tikaExtractor.setExtractedInfo(text);
                }
            }
        }

        this.extractedInfo = this.tikaExtractor.getExtractedInfo();
        long endTime   = System.currentTimeMillis();
        long totalTime = endTime - startTime;
        System.out.println("Parse time: " + totalTime + " ms");
    }

    /**
     * Write XML output in pathToSave
     * @return saved file
     */
    public File writeXMLResponse() {
        //write the xml file in output
        XMLOutput xmlOutput = new XMLOutput();
        xmlOutput.writeXML(this.metadata, tikaExtractor.getMetadata(), this.customMetadata,
                this.extractedInfo, this.pathToSave, this.inputName);
        System.out.println("XML file created: output/" + this.pathToSave + "/" + this.inputName + ".xml");
        if(pathToSave.isEmpty()){
            return new File(this.inputName+".xml");
        }
        else
            return new File(this.pathToSave+"/"+this.inputName+".xml");
    }

    /**
     * Transform the pdf file in input in a set of images every represent a page of a pdf.
     * @param inputFile pdf file in input
     * @return the last image file
     */
    public File pdf2Images(File inputFile) {
        PDFImageWriter pdfImageWriter = new PDFImageWriter();
        PDDocument doc = null;
        File imageFile = null;
        File dir = null;
        try {
            doc = PDDocument.load(inputFile);

            //more slowly
            /*System.out.println(pdfImageWriter.writeImage(doc, "png", null, 1, Integer.MAX_VALUE, "files/pic",
                BufferedImage.TYPE_INT_RGB, Toolkit.getDefaultToolkit().getScreenResolution()));*/

            //more fast
            PDPageNode node = doc.getDocumentCatalog().getPages(); ///// get pages
            List<PDPage> kids = node.getKids();
            int count = 0;
            for (PDPage page : kids) {   ///// iterate
                BufferedImage img = null;
                img = page.convertToImage(BufferedImage.TYPE_INT_RGB, 128);
                FileUtils.createDirIfNotExists(new File("files/"));
                dir = new File("files/"+inputFile.getName()+"_imgs/");
                FileUtils.createDirIfNotExists(dir);
                imageFile = new File("files/"+inputFile.getName()+"_imgs/"+ (count++) + ".jpg");
                ImageIO.write(img, "jpg", imageFile);
                img.flush();
            }
            doc.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return dir;
    }

    /**
     * Transform the pdf file represented by URL in input in a set of images every represent a page of a pdf.
     * @param inputFile pdf file in input
     * @return the last image file
     */
    public File pdf2Images(URL inputFile) {
        PDFImageWriter pdfImageWriter = new PDFImageWriter();
        PDDocument doc = null;
        File imageFile = null;
        File dir = null;
        try {
            doc = PDDocument.load(inputFile);

            //more slowly
            /*System.out.println(pdfImageWriter.writeImage(doc, "png", null, 1, Integer.MAX_VALUE, "files/pic",
                BufferedImage.TYPE_INT_RGB, Toolkit.getDefaultToolkit().getScreenResolution()));*/

            //more fast
            PDPageNode node = doc.getDocumentCatalog().getPages(); ///// get pages
            List<PDPage> kids = node.getKids();
            int count = 0;
            for (PDPage page : kids) {   ///// iterate
                BufferedImage img = null;
                img = page.convertToImage(BufferedImage.TYPE_INT_RGB, 128);
                FileUtils.createDirIfNotExists(new File("files/"));
                dir = new File("files/"+this.inputName+"_imgs/");
                FileUtils.createDirIfNotExists(dir);
                imageFile = new File("files/"+this.inputName+"_imgs/"+ (count++) + ".jpg");
                ImageIO.write(img, "jpg", imageFile);
                img.flush();
            }
            doc.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return dir;

    }

    //Getter and Setter
    public String getPathToSave() {
        return pathToSave;
    }

    public void setPathToSave(String pathToSave) {
        this.pathToSave = pathToSave;
    }

    public TikaExtractor getTikaExtractor() {
        return tikaExtractor;
    }

    public void setTikaExtractor(TikaExtractor tikaExtractor) {
        this.tikaExtractor = tikaExtractor;
    }

    public PitiaMetadata getCustomMetadata() {
        return customMetadata;
    }

    public void setCustomMetadata(PitiaMetadata customMetadata) {
        this.customMetadata = customMetadata;
    }

}
