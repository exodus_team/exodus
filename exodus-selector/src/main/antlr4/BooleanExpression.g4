grammar BooleanExpression;

@header {
  package it.uniroma2.art.exodus.selector.parser;
  import it.uniroma2.art.exodus.selector.expression.*;
  import java.time.LocalDateTime;
  import java.time.format.DateTimeFormatter;
}

parse returns [Expression exp]
 : expression EOF {$exp = $expression.exp; }
 ;

expression returns [Expression exp]
 : completedAndExpression                       {$exp = $completedAndExpression.exp; }
 | completedOrExpression                       {$exp = $completedOrExpression.exp; }
 | NOT expression                                 {$exp = new NotExpression($expression.exp); }
 | simpleExpression                               {$exp = $simpleExpression.exp; }
 ;

andExpression returns [AndExpression exp]
 : expression AND andExpression                    {ArrayList<Expression> list = new ArrayList<Expression>($andExpression.exp.getExpressionList()); list.add($expression.exp); $exp = new AndExpression(list); }
 | expression                                      {ArrayList<Expression> list = new ArrayList<Expression>(); list.add($expression.exp); $exp = new AndExpression(list); }
 ;
 
completedAndExpression returns [AndExpression exp]
 : LPAREN andExpression RPAREN                       {$exp = $andExpression.exp; }
 ;
 
orExpression returns [OrExpression exp]
 : expression OR orExpression                    {ArrayList<Expression> list = new ArrayList<Expression>($orExpression.exp.getExpressionList()); list.add($expression.exp); $exp = new OrExpression(list); }
 | expression                                    {ArrayList<Expression> list = new ArrayList<Expression>(); list.add($expression.exp); $exp = new OrExpression(list); }
 ;
 
completedOrExpression returns [OrExpression exp]
 : LPAREN orExpression RPAREN                       {$exp = $orExpression.exp; }
 ;
 
simpleExpression returns [SimpleExpression exp]
 : key=STRING comparator value=STRING {$exp = new SimpleExpression($key.text.toUpperCase(),$value.text,$comparator.compOp);}
 | key=STRING comparator val=date {$exp = new SimpleExpression($key.text.toUpperCase(),LocalDateTime.parse($val.str.replaceFirst("@","") +"T00:00:00Z", DateTimeFormatter.ofPattern("yyyy/MM/dd'T'HH:mm:ss'Z'")) ,$comparator.compOp); }
 | key=STRING comparator value=DECIMAL {$exp = new SimpleExpression($key.text.toUpperCase(), Double.parseDouble($value.text),$comparator.compOp);}
 ;
 
comparator returns [ComparisonOperator compOp]
 : GT {$compOp = ComparisonOperator.valueOf("GRATER_THEN"); }
 | GE {$compOp = ComparisonOperator.valueOf("GRATER_EQUALS"); }
 | LT {$compOp = ComparisonOperator.valueOf("LESS_THEN"); }
 | LE {$compOp = ComparisonOperator.valueOf("LESS_EQUALS"); }
 | EQ {$compOp = ComparisonOperator.valueOf("EQUALS"); }
 | DF {$compOp = ComparisonOperator.valueOf("DIFFERENT_FROM"); }
 | CONTAINS {$compOp = ComparisonOperator.valueOf("CONTAINS"); }
 | SUBSTRING_OF {$compOp = ComparisonOperator.valueOf("SUBSTRING_OF"); }
 ;

binary
 : AND | OR
 ;
 
date returns [String str]
 : at=AT year=YEAR sl1=SLASH month=MONTH sl2=SLASH day=DAY {$str = $at.text + $year.text + $sl1.text + $month.text + $sl2.text + $day.text; System.out.println("Date: "+$text);}
 ;




AND        : ('AND' | 'and' | '&&' );
OR         : ('OR' | 'or' | '||' );
DF         : ('!=' | '<>' );
GT         : '>' ;
GE         : '>=' ;
LT         : '<' ;
LE         : '<=' ;
EQ         : '=' ;
NOT        : ('NOT' | 'not' | '!');
CONTAINS   : ('CONTAINS' | 'contains') ;
SUBSTRING_OF  : ('SUBSTRING'|'substring') (' ')+ ('OF'|'of') ;
LPAREN     : '(' ;
RPAREN     : ')' ;
SLASH      : '/' ;
DECIMAL    : ('+' |'-')? [0-9]+ ( '.' [0-9]+ )? ;
YEAR	   : [0-9]+ ;
MONTH	   : ('0'[1-9] | '1'[0-2]) ;
DAY        : ( '0'[1-9]|[1-2][0-9]|'3'[0-1] ) ;
AT         : '@' ;
STRING     : [a-zA-Z_0-9./#$%^&*{};':"]+ ;
WS         : [ \r\t\u000C\n]+ -> skip ;