package it.uniroma2.art.exodus.exodus_flow;

import java.io.File;

public interface Flow {
	
	public void produceOntology(File inputFile, File outputOntologyFile);

}
