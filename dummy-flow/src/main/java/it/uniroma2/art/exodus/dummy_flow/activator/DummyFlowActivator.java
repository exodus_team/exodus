package it.uniroma2.art.exodus.dummy_flow.activator;

import java.util.Hashtable;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import it.uniroma2.art.exodus.dummy_flow.DummyFlow;
import it.uniroma2.art.exodus.exodus_flow.Flow;


public class DummyFlowActivator implements BundleActivator
{
   
    public void start(BundleContext context)
    {
        Hashtable<String, String> props = new Hashtable<String, String>();
        props.put("Language", "English");
        context.registerService(
            Flow.class.getName(), new DummyFlow(), props);
    	
    }
 
 
    public void stop(BundleContext context)
    {
        // NOTE: The service is automatically unregistered.
    }

   
}
