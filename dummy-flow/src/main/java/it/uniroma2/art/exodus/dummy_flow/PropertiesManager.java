package it.uniroma2.art.exodus.dummy_flow;


import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;


public class PropertiesManager {
	
	
	
	public static String PROPERTIES_FILE = "dummy-flow.properties";
	
	private static PropertiesManager instance = null;
	
	//private static String ontologyFilepath;
	
	
	
	

	private PropertiesManager(){
		init();
	}
	
	
	public static PropertiesManager getInstance(){
		if (instance == null)
			instance = new PropertiesManager();
		return instance;
	}
	
	
	
	private static void init(){
		
		
		Properties prop = new Properties();
		InputStream input = null;

		try {

			Class<PropertiesManager> thisClass = PropertiesManager.class;
			URL propertiesURL = thisClass.getClassLoader().getResource(PROPERTIES_FILE);
			input = propertiesURL.openStream();
			
			//input = new FileInputStream(PROPERTIES_FILE);

			// load a properties file
			prop.load(input);

			// get the property value and print it out
			//ontologyFilepath = prop.getProperty("OntologyFilepath");

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	  
		
	}


	/*public String getOntologyFilepath() {
		return ontologyFilepath;
	}*/
	

	
	


}
