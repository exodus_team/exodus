package it.uniroma2.art.exodus.dummy_flow;

import java.io.File;
import java.io.FileInputStream;

import java.io.IOException;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;



import org.eclipse.rdf4j.model.Model;

import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;



import it.uniroma2.art.exodus.exodus_flow.Flow;

public class DummyFlow implements Flow {
	
	public DummyFlow(){
		
	}
	

	public void produceOntology(File inputFile, File outputOntologyFile) {
		
		try{
			//Class<DummyFlow> thisClass = DummyFlow.class;
			//URL propertiesURL = thisClass.getClassLoader().getResource(file.getAbsolutePath());
			//InputStream input = propertiesURL.openStream();
			
			FileInputStream input = new FileInputStream(inputFile);
			FileInputStream input1 = new FileInputStream(inputFile);
			
			
			Model model = Rio.parse(input, "", RDFFormat.RDFXML);
			
			
			//Files.copy(input1, new File(PropertiesManager.getInstance().getOntologyFilepath()).toPath(), StandardCopyOption.REPLACE_EXISTING);
			Files.copy(input1, outputOntologyFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
		}
		
		catch(Exception e){
			throw new IllegalArgumentException(e);
		}
		
		
		
		
	}
	
	
	
	
	
	private static String readFile(File file) throws IOException {
		byte[] encoded = Files.readAllBytes(Paths.get(file.getAbsolutePath()));
		return new String(encoded, StandardCharsets.UTF_8);
	}
	
	
	
	
	public static void main(String[] args){
		/*String text = "Mario volava alto mentre Gianni era più concreto.\n";
		text = text + "Ciao caro Goffredo, come stai?\n";
		text = text + "Andiamo incontro per strada a Lorenzo Carlo.\n";
		text = text + "Erano anni che non vedevo Lorenzo bere.\n";
		text = text + "Salutate con ardore Tito Valerio Tauro, tribuno romano.\n";
		text = text + "Bill and Billa 03.2014\n";*/
		
		/*String text = "Matteo Renzi fu capo del governo.\n";
		text = text + "a v Paolo ha superato l'esame.\n";
		text = text + "Batman non ha poteri, al contrario di Superman.\n";*/
		
		Flow flow = new DummyFlow();
		flow.produceOntology(new File ("resources\\food.rdf"), new File("output\\filledOntology.n3"));
	}

}
