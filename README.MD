README
======
Exodus Selector - Getting Started
---------------
1. Download [Apache Karaf](http://karaf.apache.org/) (tested on version 4.0.4).
   
   It is possible to download the [binary archive](https://search.maven.org/remotecontent?filepath=org/apache/karaf/apache-karaf/4.0.4/apache-karaf-4.0.4.zip) from Maven Central.

2. Unpack the archive.

3. Put the file `exodus-0.0.1-SNAPSHOT-features.xml` (`Downloads` section) inside the folder `deploy` of the unpacked archive

4. Put the files `exodus-selector/{selector,selector-flows}.properties in the root of the unpacked archive

5. Start the Karaf container, by executing the command `bin/karaf.bat` (on Windows) or `bin/karaf` (on Linux)

6. Inside the interactive console of Karaf, install the `exodus` feature, by executing the command `feature:install exodus`

7. After the installation completes, a Swing dialog should appear on screen allowing to start the actual processing

8. To shutdown the container, execute the command `shutdown` inside the Karaf console


On subsequent runs, it is not necessary to install the feature `exodus`. To launch Karaf in a clean start, append the parameter `clean`





Exodus Validator - Getting Started
---------------
1. Unpack the archive `exodus-validator-pack.zip` (`Downloads` section)

2. Start Exodus Validator by executing `exodus-validator.jar`

